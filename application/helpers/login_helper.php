<?php
function is_logged_in_admin()
{
	$ci =& get_instance();

	// var_dump($ci->session->all_userdata());
	// exit;
	if (!$ci->session->all_userdata()) {
		redirect('admin/login','refresh');
	} else {
		$hak_akses = $ci->session->userdata('hak_akses');
		$role      = $ci->session->userdata('role');

		if ($hak_akses) {
			if ($role == 'pengguna') {
				redirect('','refresh');
			}else{
				header('admin/dashboard');
			}
		} else {
			redirect('admin/login','refresh');
		}
		
	}
	
}

function wates_su()
{
	
	$ci =& get_instance();

	$hak_akses = $ci->session->userdata('hak_akses');

	if (!$hak_akses) {
		redirect('admin/login','refresh');
	} else {
		if ($hak_akses != 'super_admin') {
			redirect('admin/404_override');
		}
		
	}
}

function wates_admin()
{
	
	$ci =& get_instance();

	$hak_akses = $ci->session->userdata('hak_akses');

	if (!$hak_akses) {
		redirect('admin/login','refresh');
	} else {
		if ($hak_akses == 'petugas' || $hak_akses == 'pengembang' || $hak_akses == 'pengguna') {
			redirect('admin/404_override');
		}
		
	}
}

function wates_petugas()
{
	
	$ci =& get_instance();

	$hak_akses = $ci->session->userdata('hak_akses');

	if (!$hak_akses) {
		redirect('admin/login','refresh');
	} else {
		if ($hak_akses == 'pengembang' || $hak_akses == 'pengguna') {
			redirect('admin/404_override');
		}
		
	}
}

/* End of file login_helper.php */
/* Location: ./application/helpers/login_helper.php */