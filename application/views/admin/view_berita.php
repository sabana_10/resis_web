<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Berita Property</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li>
                        <i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">
                        </i>
                    </li>
                    <li class="active">Berita Property</li>
                </ol>
            </div>
        </div>
        <!-- start Payment Details -->

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card  card-box">
                    <div class="card-head">
                        <header>Detail Data Berita Property</header>
                        <div class="tools">
                            <a href="javascript:;" class=" box-refresh btn btn-tbl-edit btn-warning btn-xs" data-toggle="tooltip" data-placement="right" title="Refresh">
                                <i class="fa fa-repeat"></i>
                            </a>
                            <a href="<?= site_url('admin/berita/add') ?>" id="btnAddBerita" class="btn btn-tbl-edit btn-success btn-xs" data-toggle="tooltip" data-placement="right" title="Tambah">
                                <i class="fa fa-plus"></i>
                            </a>
                            <!-- <a href="javascript:;" class="t-collapse btn btn-tbl-edit btn-danger btn-xs">
								<i class="fa fa-chevron-down"></i>
							</a> -->
                            <!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="table-scrollable mt-5">
                            <table id="example1" class="display full-width">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul</th>
                                        <th>Tanggal</th>
                                        <th>Menu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($getDataBerita as $gdb) :
                                    ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $gdb->berita ?></td>
                                            <td><?= $gdb->gambar ?></td>
                                            <td>
                                                <a href="<?= site_url('admin/berita/edit/' . md5($gdb->id)) ?>" class="btn btn-tbl-edit btn-xs" id="btnEditPetugas" data-toggle="tooltip" data-placement="right" title="Edit Data">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <button onclick="btnDeleteBerita('<?= md5($gdb->id) ?>')" class="btn btn-tbl-delete btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus">
                                                    <i class="fa fa-trash-o "></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // var url;
    // var url_1 = window.location.href;
    // var url_2 = url_1.substring(0, url_1.lastIndexOf('/edit/'));
    // var save_method;
    // function btnAddPetugas() {
    // 	save_method = 'tambahPetugas';
    //   $('#form')[0].reset();
    //   $('#modalPetugas').modal('show');
    //   $('.modal-title').text('Tambah Petugas');
    // }
    // function btnEditPetugas(id) {
    // 	save_method = 'editPetugas';
    // 	$('#form')[0].reset();
    // 	$.ajax({
    // 		url : '<?php echo site_url("Admin/Petugas/getDataPetugasById/") ?>'+id,
    // 		type : 'GET',
    // 		dataType : 'JSON',
    // 		success : function (data) {
    //     		$('[name = "id"]').val(data.id);
    //     		$('[name = "username"]').val(data.username);
    //     		// $('[name = "hak_akses"]').val(data.hak_akses);
    //   	    $('#modalPetugas').modal('show');
    //     	$('.modal-title').text('Edit Petugas');

    // 		},
    //  		error: function (jqXHR, textStatus, errorThrown){
    //      		failNotif('ErrorGetData : ' + errorThrown);
    //   	}
    //   });
    // }
    // $(document).ready(function(){
    //   $('#form').on('submit', function(e) {
    //     e.preventDefault();
    //     $('#btnSimpanPetugas').text('Menyimpan...');
    //     $('#btnSimpanPetugas').attr('disabled', true);
    //     	url = '<?php echo site_url('Admin/Petugas/saveWithAjax') ?>';
    //     	$.ajax({
    // 	      url : url,
    // 	      method : 'POST',
    // 	      data : new FormData(this),
    // 	      contentType : false,
    // 	      cache : false,
    // 	      processData : false,
    // 	      dataType : 'JSON',
    // 	      success : function (data) {
    // 	      	$('#modalPetugas').modal('hide');
    // 	        successNotif(data.message);
    // 	      },
    // 	      error : err=>{
    // 	        failNotif('saveData', 'Gagal menambah data!');
    // 	      }
    // 	    });
    // 	});
    // });
    function btnDeleteBerita(id) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
            title: 'Apakah kamu yakin?',
            text: "Kamu akan kehilangan data ini setelah dihapus!",
            icon: false,
            showCancelButton: true,
            confirmButtonText: 'Yes, Hapus!',
            cancelButtonText: 'Tidak!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "<?php echo site_url('Admin/Berita/deleteWithAjax') ?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data) {
                        successNotif(data.message);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        failNotif('Data gagal dihapus!');
                    }
                });
            }
        })
    }

    function successNotif(data) {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: data,
            showConfirmButton: false,
            timer: 2000,
        }).then(function() {
            location.reload();
        });
    }

    function failNotif(method = null, titles) {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: titles,
            showConfirmButton: false,
            timer: 2500,
        }).then(function() {
            if (method == 'saveData') {
                $('#btnSimpanPetugas').text('Simpan');
                $('#btnSimpanPetugas').attr('disabled', false);
            }
        });
    }
</script>

<!-- modal -->
<!-- <div class="modal fade" id="modalPetugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title text-gray-100" id="exampleModalLabel">Tajuk</label>
        <button class="close text-gray-100" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-gray-100" style="margin-right: 30px;margin-left: 30px;">
        <form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
        	<input type="hidden" value="" name="id"/>
          <div class="form-group">
            <label id="label-objek">
            	Username 
            	<i class="fa fa-info-circle ml-1 color-gray" data-toggle="tooltip" data-placement="right" title="Username wajib menggunakan email" ></i>
            </label>
            <input type="email" class="form-control" placeholder="contoh username : admin@resis.id" name="username" required="">
          </div>
          <div class="form-group">
            <label id="label-objek">Password</label>
            <input type="password" class="form-control" placeholder="contoh password : 4Dm1nR3515" name="password" required="">
          </div>
          <div class="form-group">
            <label id="label-objek">Hak Akses</label>
            <select name="hak_akses" id="hak_akses" class="form-control">
              <option value="super_admin">Super Admin</option>
              <option value="admin">Admin</option>
              <option value="petugas">Petugas</option>
            </select>
          </div>
          <br>
          <div class="form-group">
            <div style="float: right">
          	<button class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</button>
            <button id="btnSimpanPetugas" type="submit" class="btn btn-info">
            	<em class="fa fa-save"></em>
                 Simpan
            </button>
            </div>
          </div>
        </form>
      </div>
  </div>
</div> -->