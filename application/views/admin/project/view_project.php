<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Project</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li class="active">Data Project</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Detail Data Project</header>
						<div class="tools">
							<a href="javascript:;" class=" box-refresh btn btn-tbl-edit btn-warning btn-xs" data-toggle="tooltip" data-placement="right" title="Refresh">
								<i class="fa fa-repeat"></i>
							</a>
							<a href="<?= site_url('admin/data/project/add') ?>" class="btn btn-tbl-edit btn-success btn-xs" data-toggle="tooltip" data-placement="right" title="Tambah">
								<i class="fa fa-plus"></i>
							</a>
							<!-- <a href="javascript:;" class="t-collapse btn btn-tbl-edit btn-danger btn-xs">
								<i class="fa fa-chevron-down"></i>
							</a> -->
							<!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
						</div>
					</div>
					<div class="card-body ">
						<div class="table-scrollable mt-5">
            <table id="example1" class="display full-width">
                <thead>
									<tr>
										<th>No</th>
										<th>Project</th>
                    <th>Nama Pengembang</th>
										<th>Fasilitas Umum</th>
                    <th width="50">Tipe</th>
                    <th>Tahun dimulai</th>
                    <th>Estimasi Proyek Selesai</th>
										<th>Menu</th>
									</tr>
								</thead>
								<tbody>
									<?php
                    $no = 1;
										foreach ($getDataProject as $gdp) :
									?>
									<tr>
                    <td><?= $no++ ?></td>
                    <td><?= $gdp->nama ?></td>
                    <td><?= $gdp->nama_pengembang ?></td>
                    <td><?= $gdp->fasilitas_umum ?></td>
                    <td><?= $gdp->type ?></td>
                    <td><?= $gdp->tahun_proyek_dimuali ?></td>
                    <td><?= $gdp->estimasi_proyek_selesai ?></td>
                    <td>
                      <a href="<?= site_url('admin/data/project/edit/'.md5($gdp->id)) ?>" class="btn btn-tbl-edit btn-xs" id="btnEditPetugas" data-toggle="tooltip" data-placement="right" title="Edit Data">
                        <i class="fa fa-pencil"></i>
                      </a>
                      <button type="button" onclick="btnDeleteProject('<?= md5($gdp->id) ?>')" class="btn btn-tbl-delete btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus">
                        <i class="fa fa-trash-o "></i>
                      </button>
                    </td>
                  </tr>
									<?php
  										endforeach;
									?>
								</tbody>
              </table>
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
// var url;
// var save_method;
// function btnAddProject() {
// 	save_method = 'tambahProject';
//     $('#form')[0].reset();
//     $('#modalProject').modal('show');
//     $('.modal-title').text('Tambah Project');
//     $('#logo-preview').hide();
// }	
// function btnEditProject(id) {
// 	save_method = 'editProject';
//   	$('#form')[0].reset();
//   	$.ajax({
//   		url : '<?php //echo site_url('Admin/Project/getDataProjectById/') ?>'+id,
//   		type : 'GET',
//   		dataType : 'JSON',
//   		success : function (data) {
//     		$('[name = "id"]').val(data.id);
//         $('[name = "id_pengembang"]').val(data.id_pengembang);
//     		$('[name = "nama"]').val(data.nama);
//         $('[name = "deskripsi"]').val(data.deskripsi);
//         $('[name = "lokasi"]').val(data.lokasi);
//         $('[name = "fasilitas_umum"]').val(data.fasilitas_umum);
//         $('[name = "type"]').val(data.type);
//         $('[name = "tahun_proyek_dimuali"]').val(data.tahun_proyek_dimuali);
//         $('[name = "estimasi_proyek_selesai"]').val(data.estimasi_proyek_selesai);
//         $('[name = "tanggal_input"]').val(data.tanggal_input);
//         $('#modalProject').modal('show');
// 	    	$('.modal-title').text('Edit Project');
//   		},
//    		error: function (jqXHR, textStatus, errorThrown){
//        		failNotif('ErrorGetData : ' + errorThrown);
//     	}
//     });
// }
// $(document).ready(function(){
//   $('#form').on('submit', function(e) {
//     e.preventDefault();
//     $('#btnSimpanProject').text('Menyimpan...');
//     $('#btnSimpanProject').attr('disabled', true);
//     	url = '<?php echo site_url('Admin/Project/saveWithAjax') ?>';
//     	$.ajax({
// 	      url : url,
// 	      method : 'POST',
// 	      data : new FormData(this),
// 	      contentType : false,
// 	      cache : false,
// 	      processData : false,
// 	      dataType : 'JSON',
// 	      success : function (data) {
// 	      	$('#modalProject').modal('hide');
// 	        successNotif(data.message);
// 	      },
// 	      error : err=>{
// 	        failNotif('saveData', 'Gagal menambah data!');
// 	      }
// 	    });
// 	});
// });
function btnDeleteProject(id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })
  swalWithBootstrapButtons.fire({
    title: 'Apakah kamu yakin?',
    text: "Kamu akan kehilangan data ini setelah dihapus!",
    icon: false,
    showCancelButton: true,
    confirmButtonText: 'Yes, Hapus!',
    cancelButtonText: 'Tidak!',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url : "<?php echo site_url('Admin/Project/deleteWithAjax')?>/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          successNotif(data.message);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          failNotif('Data gagal dihapus!');
        }
      });
    }
  })
}
function successNotif(data) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
    location.reload();
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanProject').text('Simpan');
      $('#btnSimpanProject').attr('disabled', false);
    }
  });
}
</script>
