<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Project</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li >
						<a class="parent-item"href="<?= site_url('admin/data/project') ?>">Data Project</a></li>&nbsp;<i class="fa fa-angle-right">		
					</i>
					<li class="active">Edit</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Edit Data Project</header>
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
					      	<input type="hidden" value="<?= $getDataProject->id ?>" name="id"/>
					      	<!-- <input type="hidden" value="<?= $getDataProject->id_pengembang ?>" name="id_pengembang"/> -->
				          <div class="row">
				            <div class="col-md-8">
				              <div class="form-group">
				                <label id="label-objek">
				                  Nama Pengembang
				                </label>
				                <input type="text" class="form-control" placeholder="contoh : Resis Realestate" name="nama_pengembang" required="" value="<?= $getDataProject->nama_pengembang ?>" readonly>
				                <!-- <select name="id_pengembang" class="form-control">
				                  <?php foreach ($getDataPengembang as $gdpm) : ?>
				                  <option value="<?= $gdpm->id ?>"><?= $gdpm->nama ?></option>
				                  <?php endforeach; ?>
				                </select> -->
				              </div>
				            </div>
				            <div class="col-md-4">
				              <div class="form-group text-center">
				                <label for="" class="label-objek">Tanggal Sekarang</label><br>
				                <label for="" class="label-objek"><?= date("d / m / Y") ?></label>
				              </div>
				            </div>
				          </div>
				          <div class="row">
				            <div class="col-md-8">
				              <div class="form-group">
				                <label id="label-objek">
				                  Nama Project <small class="text-danger">wajib</small>
				                </label>
				                <input type="text" class="form-control" placeholder="contoh : Resis Realestate" name="nama" required="" value="<?= $getDataProject->nama ?>">
				              </div>
				            </div>
				            <div class="col-md-4">
				              <div class="form-group">
				                <label id="label-objek">
				                  Type <small class="text-danger">wajib</small>
				                </label>
				                <select name="type" class="form-control" required="">
				                	<?php
				                		if ($getDataProject->type == 'rumah') {
				                			echo '<option value="rumah">Rumah</option>
				                  <option value="apartemen">Apartement</option>
				                  <option value="vila">Villa</option>';
				                		} else if($getDataProject->type == 'apartemen'){
				                			echo '<option value="apartemen">Apartement</option>
				                			<option value="rumah">Rumah</option>
				                  <option value="vila">Villa</option>';
				                		}else{
				                			echo '<option value="vila">Villa</option>
				                  <option value="rumah">Rumah</option>
				                  <option value="apartemen">Apartement</option>';
				                		}	
				                	?>
				                </select>
				              </div>
				            </div>
				          </div>
				          <div class="form-group">
				          	<label id="label-objek">
				            	Deskripsi 
				            </label>
				            <textarea name="deskripsi" id="summernote" cols="20" rows="5" class="form-control"><?= $this->typography->auto_typography($getDataProject->deskripsi); ?>
				            </textarea>
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Fasilitas Umum 
				            </label>
				            <textarea name="fasilitas_umum" cols="20" rows="5" class="form-control"><?= $getDataProject->fasilitas_umum ?>
				            </textarea>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Tahun Dimulai <small class="text-danger">wajib</small>
				                </label>
				                <input type="number" class="form-control" placeholder="contoh : 1933" name="tahun_proyek_dimuali" value="<?= $getDataProject->tahun_proyek_dimuali ?>" required="">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Estimasi Project Selesai <small class="text-danger">wajib</small>
				                </label>
				                <input type="number" class="form-control" placeholder="contoh : 1933" name="estimasi_proyek_selesai" value="<?= $getDataProject->estimasi_proyek_selesai ?>" required="">
				              </div>
				            </div>
				          </div>
				          <hr>
				          <div class="row">
				            <div class="col-md-4">
				              <div class="form-group">
				                <label id="label-objek">
				                  Provinsi  <small class="text-danger">wajib</small>
				                </label>
				                <select name="provinsi" class="form-control" id="provinsi" required="">
                                    <option value="<?= ($getDataProject->id_provinsi != null)? $getDataProject->id_provinsi : '' ?>"><?= ($getDataProject->nama_provinsi != null)? $getDataProject->nama_provinsi : 'Pilih Provinsi' ?></option>
                                    <?php
                                	foreach ($getProvinsi as $gp) {
                                	?>
                                		<option value="<?= $gp->id ?>"><?= $gp->name ?></option>
                                	<?php
                                	}
                                    ?>
                                </select>
				              </div>
				            </div>
				            <div class="col-md-4">
				              <div class="form-group">
				                <label id="label-objek">
				                  Kabupaten / Kota <small class="text-danger">wajib</small>
				                </label>
				                <input type="text" name="input_kabupaten" id="input_kabupaten" class="form-control" value="<?= ($getDataProject->nama_kabupaten != null)? $getDataProject->nama_kabupaten : '' ?>" readonly>
				                <select name="kabupaten" class="form-control kabupaten" id="kabupaten"></select>
				              </div>
				            </div>
				            <div class="col-md-4">
				              <div class="form-group">
				                <label id="label-objek">
				                  Kecamatan <small class="text-danger">wajib</small>
				                </label>
				                <input type="text" name="input_kecamatan" id="input_kecamatan" class="form-control" value="<?= ($getDataProject->nama_kecamatan != null)? $getDataProject->nama_kecamatan : '' ?>" readonly>
				                <select name="kecamatan" class="form-control kecamatan" id="kecamatan"></select>
				              </div>
				            </div>
				            <!-- <div class="col-md-3">
				              <div class="form-group">
				                <label id="label-objek">
				                  Desa
				                </label>
				                <input type="text" name="input_desa" class="form-control" value="<?= $getDataProject->nama_desa ?>" readonly>
				                <select name="desa" class="form-control desa" id="desa"></select>
				              </div>
				            </div> -->
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Lokasi <small class="text-danger">wajib</small>
				            </label>
				            <input id="lokasi" type="text" class="form-control" name="lokasi" required="" value="<?= $getDataProject->lokasi ?>" readonly>
				            <br>
				            <div id="peta" style="width: 100%; height: 300px"></div>
				          </div>
				          <br>
				          <div class="form-group" id="wadahBtn">
				            <div style="float: right">
				          	<a href="<?= site_url('admin/data/project') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
				            <button id="btnSimpanProject" type="submit" class="btn btn-info">
				            	<em class="fa fa-save"></em>
				                 Simpan
				            </button>
				            </div>
				          </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
//get gmap
var url = '<?php echo site_url('Admin/Produk/getLokasi/'.md5($getDataProject->id)) ?>';
$.ajax({
  url : url,
  method : 'POST',
  dataType : 'JSON',
  success : function (response) {
    //set gmaps
	var mapOptions = {
	    center: [response.data.lat, response.data.lng],
	    zoom: 13
	} 
	var maapin = new L.map('peta', mapOptions);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?accessToken={accessToken}', {
	    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	    maxZoom: 18,
	    id: 'mapbox.streets',
	    accessToken: 'AIzaSyD4NfvktSJrrnmUz9hTTfEJcCmHC-PRBpc'
	}).addTo(maapin);

	var lokasiInput = document.querySelector("[name=lokasi]");

	var curlocation = [response.data.lat, response.data.lng];
	maapin.attributionControl.setPrefix(false);

	var marker = new L.marker(curlocation, {
		draggable: 'true',
	});
	marker.on('dragend', function(event) {
		var position = marker.getLatLng();
		marker.setLatLng(position, {
			draggable: 'true',
		}).bindPopup(position).update();
		$("#lokasi").val(position.lat+","+position.lng);
	});
	maapin.addLayer(marker);

	marker.on('click', function(e) {
		var lat = e.latlng.lat;
		var lng = e.latlng.lng;
		if (!marker) {
			marker = L.marker(e.latlng).addTo(maapin);
		} else {
			marker = L.marker(e.latlng);
		}
		lokasiInput.value = lat+","+lng;
	});	
  }
});

//save data
$(document).ready(function(){
  $('#kabupaten').hide();
  $('#kecamatan').hide();
  // $('#desa').hide();
  
  //fun get kabupaten
  $('#provinsi').on('input', function getKabupaten() {
	$('#kabupaten').empty();
	var e = document.getElementById("provinsi");
	var id = e.value;
	url = '<?php echo site_url("Admin/Project/getKabupaten/'+id+'") ?>';
	$.ajax({
      url : url,
      method : 'POST',
      data : {},
      contentType : false,
      cache : false,
      processData : false,
      dataType : 'JSON',
      success : function (data) {
      	var result = Object.assign({}, data.data);
      	for (var i = 0; i < data.data.length; i++) {
      		$('#input_kabupaten').hide();
	      	$('#kabupaten').show();
	      	$('#kabupaten').append('<option value="'+data.data[i].id+'">'+data.data[i].name+'</option>');
	      	$('#kabupaten').change();
      	}
      },
      error : err=>{}
    });
  });

  // fun get kecamatan
  $('#kabupaten').on('input', function() {
  	$('#kecamatan').empty();
	var e = document.getElementById("kabupaten");
	var id = e.value;
	url = '<?php echo site_url("Admin/Project/getKecamatan/'+id+'") ?>';
	$.ajax({
      url : url,
      method : 'POST',
      data : {},
      contentType : false,
      cache : false,
      processData : false,
      dataType : 'JSON',
      success : function (data) {
      	var result = Object.assign({}, data.data);
      	for (var i = 0; i < data.data.length; i++) {
      		$('#input_kecamatan').hide();
	      	$('#kecamatan').show();
	      	$('#kecamatan').append('<option value="'+data.data[i].id+'">'+data.data[i].name+'</option>');
	      	$('#kecamatan').change();
      	}
      },
      error : err=>{}
    });
  })

  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanProject').text('Menyimpan...');
    $('#btnSimpanProject').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Project/saveWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	        successNotif(data.message);
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});
});
function successNotif(data) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
    document.location.href='<?php echo site_url('admin/data/project') ?>';
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanProject').text('Simpan');
      $('#btnSimpanProject').attr('disabled', false);
    }
  });
}
</script>