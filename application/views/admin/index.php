<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Admin Resis" />
	<meta name="author" content="Resis" />
	<title>Resis - Admin</title>
	<!-- icons -->
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!--bootstrap -->
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/summernote/summernote.css" rel="stylesheet">
	<!-- data tables -->
    <link href="<?= site_url('assets/admin/') ?>assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet"
        type="text/css" />
	<!-- morris chart -->
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/summernote/summernote.css" rel="stylesheet">
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="<?= site_url('assets/admin/') ?>assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="<?= site_url('assets/admin/') ?>assets/css/material_style.css">
	<!-- animation -->
	<link href="<?= site_url('assets/admin/') ?>assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- sweetalert -->
	<script src="<?= site_url('assets/admin/sweetalert/dist/') ?>sweetalert2.all.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/jquery/jquery.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/popper/popper.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- Template Styles -->
	<link href="<?= site_url('assets/admin/') ?>assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= site_url('assets/admin/') ?>assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?= site_url('assets/admin/') ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="<?= site_url('assets/admin/') ?>assets/css/theme-color.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
	<link rel="shortcut icon" href="<?= site_url('assets/logo/') ?>resis_logo_transparent_1.png" />
	<!-- leaflet js -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    
    <!-- geo location -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.css"/>
    <script src="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.umd.js"></script>
    <script src="<?= site_url('assets/admin/') ?>assets/js/leaflet-search.js"></script>
</head>
<!-- END HEAD -->

<body
	class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
	<div class="page-wrapper">
		<!-- start header -->
		<?= $header ?>
		<!-- end header -->
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?= $sidebar ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<?= $content ?>
			<!-- end page content -->
			<!-- start chat sidebar -->
			<?php //echo $sidebar_setting ?>
			<!-- end chat sidebar -->
		</div>
		<!-- end page container -->
		<!-- start footer -->
		<?= $footer ?>
		<!-- end footer -->
	</div>
	<!-- start js include path -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- bootstrap -->
	
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/js/pages/sparkline/sparkline-data.js"></script>
	<!-- datatables -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= site_url('assets/admin/') ?>assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
    <script src="<?= site_url('assets/admin/') ?>assets/js/pages/table/table_data.js"></script>
	<!-- Common js-->
	<script src="<?= site_url('assets/admin/') ?>assets/js/app.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/js/layout.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/js/theme-color.js"></script>
	<!-- material -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/material/material.min.js"></script>
	<!-- animation -->
	<script src="<?= site_url('assets/admin/') ?>assets/js/pages/ui/animations.js"></script>
	
	<!-- morris chart -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/morris/morris.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/morris/raphael-min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/js/pages/chart/morris/morris_home_data.js"></script>
	<!-- summernote -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/summernote/summernote.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/js/pages/summernote/summernote-data.js"></script>
	<!-- end js include path -->
</body>

</html>