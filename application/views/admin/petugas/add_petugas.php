<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Petugas</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">
						</i>
					</li>
					<li>
						<a class="parent-item" href="<?= site_url('admin/user/petugas') ?>">Data Petugas</a>
					</li>&nbsp;<i class="fa fa-angle-right">
					</i>
					<li class="active">Tambah</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Tambah Data Petugas</header>
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
							<input type="hidden" value="" name="id" />
							<div class="form-group">
								<label id="label-objek">
									Username
									<i class="fa fa-info-circle ml-1 color-gray" data-toggle="tooltip" data-placement="right" title="Username wajib menggunakan email"></i>
								</label>
								<input type="email" class="form-control" placeholder="contoh username : admin@resis.id" name="username" required="">
							</div>
							<div class="form-group">
								<label id="label-objek">Password</label>
								<input type="password" class="form-control" placeholder="contoh password : 4Dm1nR3515" name="password" required="">
							</div>
							<div class="form-group">
								<label id="label-objek">Hak Akses</label>
								<input type="text" class="form-control" name="hak_akses" value="petugas" readonly>
							</div>
							<br>
							<div class="form-group">
								<div style="float: right">
									<a href="<?= site_url('admin/user/petugas') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
									<button id="btnSimpanPetugas" type="submit" class="btn btn-info">
										<em class="fa fa-save"></em>
										Simpan
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#form').on('submit', function(e) {
			e.preventDefault();
			$('#btnSimpanPetugas').text('Menyimpan...');
			$('#btnSimpanPetugas').attr('disabled', true);
			url = '<?php echo site_url('Admin/Petugas/saveWithAjax') ?>';
			$.ajax({
				url: url,
				method: 'POST',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'JSON',
				success: function(data) {
					$('#modalPetugas').modal('hide');
					successNotif(data.message);
				},
				error: err => {
					failNotif('saveData', 'Gagal menambah data!');
				}
			});
		});
	});

	function successNotif(data) {
		Swal.fire({
			position: 'center',
			icon: 'success',
			title: data,
			showConfirmButton: false,
			timer: 2000,
		}).then(function() {
			document.location.href = '<?php echo site_url('admin/user/petugas') ?>';
		});
	}

	function failNotif(method = null, titles) {
		Swal.fire({
			position: 'center',
			icon: 'error',
			title: titles,
			showConfirmButton: false,
			timer: 2500,
		}).then(function() {
			if (method == 'saveData') {
				$('#btnSimpanPetugas').text('Simpan');
				$('#btnSimpanPetugas').attr('disabled', false);
			}
		});
	}
</script>