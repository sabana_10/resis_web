<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Data Berita</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li>
                        <i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">
                        </i>
                    </li>
                    <li>
                        <a class="parent-item" href="<?= site_url('admin/berita') ?>">Data Berita</a>
                    </li>&nbsp;<i class="fa fa-angle-right">
                    </i>
                    <li class="active">Tambah</li>
                </ol>
            </div>
        </div>
        <!-- start Payment Details -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card  card-box">
                    <div class="card-head">
                        <header>Tambah Data Berita</header>
                    </div>
                    <div class="card-body ">
                        <form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" value="" name="id" />
                            <div class="form-group">
                                <label id="label-objek">
                                    Judul
                                    <small class="text-danger">wajib</small>
                                </label>
                                <input type="text" class="form-control" placeholder="contoh : Rumah Murah" name="judul" required="">
                            </div>
                            <div class="form-group">
                                <label id="label-objek">
                                    Deskripsi
                                    <small class="text-danger">wajib</small>
                                </label>
                                <textarea name="berita" id="summernote" cols="20" rows="2" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label id="label-objek">
                                    Thumbnail
                                    <small class="text-danger">wajib</small>
                                </label>
                                <input type="file" class="form-control" name="gambar">
                            </div>
                            <!-- <div class="form-group">
                                <label id="label-objek">
                                    Thumbnail
                                    <small class="text-danger">wajib</small>
                                </label>
                                <input type="file" class="form-control" name="gambar_thumb">
                            </div> -->
                            <div class="form-group">
                                <label id="label-objek">
                                    Tanggal
                                    <small class="text-danger">wajib</small>
                                </label>
                                <input type="date" class="form-control" name="create_date">
                            </div>

                            <!-- <div class="form-group">
                                <label id="label-objek">Hak Akses</label>
                                <input type="text" class="form-control" name="hak_akses" value="berita" readonly>
                            </div> -->
                            <br>
                            <div class="form-group">
                                <div style="float: right">
                                    <a href="<?= site_url('admin/berita') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
                                    <button id="btnSimpanBerita" type="submit" class="btn btn-info">
                                        <em class="fa fa-save"></em>
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#form').on('submit', function(e) {
            e.preventDefault();
            $('#btnSimpanBerita').text('Menyimpan...');
            $('#btnSimpanBerita').attr('disabled', true);
            url = '<?php echo site_url('Admin/Berita/saveWithAjax') ?>';
            $.ajax({
                url: url,
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    $('#modalBerita').modal('hide');
                    successNotif(data.message);
                },
                error: err => {
                    failNotif('saveData', 'Gagal menambah data!');
                }
            });
        });
    });

    function successNotif(data) {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: data,
            showConfirmButton: false,
            timer: 2000,
        }).then(function() {
            document.location.href = '<?php echo site_url('admin/berita') ?>';
        });
    }

    function failNotif(method = null, titles) {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: titles,
            showConfirmButton: false,
            timer: 2500,
        }).then(function() {
            if (method == 'saveData') {
                $('#btnSimpanBerita').text('Simpan');
                $('#btnSimpanBerita').attr('disabled', false);
            }
        });
    }
</script>