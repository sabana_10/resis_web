<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Promo</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li >
						<a class="parent-item"href="<?= site_url('admin/promo') ?>"> Data Promo</a></li>&nbsp;<i class="fa fa-angle-right">		
					</i>
					<li class="active">Edit</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Edit Data Promo</header>
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
			          	  <div class="form-group">
						  <input type="hidden" value="<?= $getDataPromo->id ?>" name="id"/>
					           <label id="label-objek">
					               Title
					           </label>
					           <input type="text" class="form-control" placeholder="Tulis title disini..." name="judul" value="<?= $getDataPromo->title ?>" required="">
					      </div>
				          <div class="form-group">
				          	<label id="label-objek">
				            	Deskripsi 
				            </label>
				            <textarea name="deskripsi" id="summernote" cols="20" rows="2" class="form-control"><?= $getDataPromo->deskripsi ?></textarea>
				          </div>
				          <div class="form-group">
				          	<label id="label-objek">
				            	Thumbnail sebelumnya
				            </label>
				            <br>
				            <img src="<?= site_url('assets/uploads/photo_promo/').$getDataPromo->thumbnail ?>" alt="" class="image-responsive" width="170px" height="100px">
				          </div>
				          <div class="form-group">
				          	<label id="label-objek">
				            	Upload Thumbnail
				            </label>
				            <input type="file" class="form-control" name="foto">
				          </div>
				          <div class="row">
				          	<div class="col-md-6">
				          		<div class="form-group">
						          	<label id="label-objek">
						            	Mulai Promo
						            </label>
						            <input type="date" class="form-control" name="tanggal_mulai" value="<?= $getDataPromo->tanggal_mulai ?>" required="">
						        </div>
				          	</div>
				          	<div class="col-md-6">
					            <div class="form-group">
					          		<label id="label-objek">
					            		Akhir Promo
					            	</label>
					            	<input type="date" class="form-control" name="tanggal_akhir" value="<?= $getDataPromo->tanggal_akhir ?>" required="">
					            </div>
				          	</div>
				          </div>
				          <br>
				          <div class="form-group">
				            <div style="float: right">
				          	<a href="<?= site_url('admin/promo') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
				            <button id="btnSimpanPromo" type="submit" class="btn btn-info">
				            	<em class="fa fa-save"></em>
				                 Simpan
				            </button>
				            </div>
				          </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanPromo').text('Menyimpan...');
    $('#btnSimpanPromo').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Promo/saveWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	      	if (data.status) {
		        successNotif(data.message, data.status, 'success', 2000);
	      	}else{
		        successNotif(data.errorintruth, data.status, 'error', 5000);
	      	}
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});
});
function successNotif(data, status, icon, time) {
  Swal.fire({
    position: 'center',
    icon: icon,
    title: data,
    showConfirmButton: false,
    timer: time,
  }).then(function() {
    if (status) {
	  document.location.href='<?php echo site_url('admin/promo') ?>';
    }else{
      $('#btnSimpanpromo').text('Simpan');
      $('#btnSimpanpromo').attr('disabled', false);
    }
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanpromo').text('Simpan');
      $('#btnSimpanpromo').attr('disabled', false);
    }
  });
}
</script>