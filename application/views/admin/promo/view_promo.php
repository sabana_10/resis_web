<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Promo</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li class="active">Data Promo</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Detail Data Promo</header>
						<div class="tools">
							<a href="javascript:;" class=" box-refresh btn btn-tbl-edit btn-warning btn-xs" data-toggle="tooltip" data-placement="right" title="Refresh">
								<i class="fa fa-repeat"></i>
							</a>
							<a href="<?= site_url('admin/promo/add') ?>" id="btnAddPromo" class="btn btn-tbl-edit btn-success btn-xs" data-toggle="tooltip" data-placement="right" title="Tambah">
                <i class="fa fa-plus"></i>
              </a>
							<!-- <a href="javascript:;" class="t-collapse btn btn-tbl-edit btn-danger btn-xs">
								<i class="fa fa-chevron-down"></i>
							</a> -->
							<!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
						</div>
					</div>
					<div class="card-body ">
						<div class="table-scrollable mt-5">
            <table id="example1" class="display full-width">
                <thead>
									<tr>
										<th>No</th>
										<th>Thumbnail</th>
										<th>Title</th>
										<th>Deskripsi</th>
										<th width="100px">Tanggal Dimulai</th>
										<th width="100px">Tanggal Diakhiri</th>
										<th width="100px">Menu</th>
									</tr>
								</thead>
								<tbody>
									<?php
                					    $no = 1;
										foreach ($getDataPromo as $gdp) :
									?>
									<tr>
										<td><?= $no++ ?></td>
										<td>
											<img src="<?= site_url('assets/uploads/photo_promo/').$gdp->thumbnail ?>" alt="" class="image-responsive" width="170px" height="100px">
										</td>
										<td><?= $gdp->title ?></td>
										<td>
					                    <?php
					                      if (strlen($gdp->deskripsi) <= 80) {
					                        echo $gdp->deskripsi;                         
					                      } else {
					                        echo substr($gdp->deskripsi, 0, 80)."..."; 
					                      }
					                      
					                    ?>
					                    </td>
										<td><?= $this->lee->getTanggalIndonesia($gdp->tanggal_mulai) ?></td>
										<td><?= $this->lee->getTanggalIndonesia($gdp->tanggal_akhir) ?></td>
										<td>
					                        <a href="<?= site_url('admin/promo/edit/'.md5($gdp->id)) ?>" class="btn btn-tbl-edit btn-xs" id="EditPromo" data-toggle="tooltip" data-placement="right" title="Edit Data">
												<i class="fa fa-pencil"></i>
											</a>
											<button onclick="btnDeletePromo('<?= md5($gdp->id) ?>')" class="btn btn-tbl-delete btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus">
												<i class="fa fa-trash-o "></i>
											</button>
										</td>
									</tr>
									<?php
										endforeach;
									?>
								</tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function btnDeletePromo(id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })
  swalWithBootstrapButtons.fire({
    title: 'Apakah kamu yakin?',
    text: "Kamu akan kehilangan data ini setelah dihapus!",
    icon: false,
    showCancelButton: true,
    confirmButtonText: 'Yes, Hapus!',
    cancelButtonText: 'Tidak!',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url : "<?php echo site_url('Admin/Promo/deleteWithAjax')?>/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          successNotif(data.message);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          failNotif('Data gagal dihapus!');
        }
      });
    }
  })
}
function successNotif(data) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
    location.reload();
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  });
}
</script>