<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">User Profile</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Pengaturan&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Profile</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PROFILE SIDEBAR -->
				<div class="profile-sidebar">
					<div class="card card-topline-aqua">
						<div class="card-body no-padding height-9">
							<div class="row">
								<div class="profile-userpic">
								<?php
									if ($this->session->userdata('hak_akses') != 'pengembang') {
								?>
									<img src="<?= site_url('assets/uploads/photo_petugas/default.png') ?>" class="img-responsive" alt=""> 
								<?php
									} else {
								?>
									<img src="<?= site_url('assets/uploads/logo_pengembang/').$gdp->logo ?>" class="img-responsive" alt=""> 
								<?php
									}	
								?>
								</div>
							</div>
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
								<?php
									if ($this->session->userdata('nama') != null) {
										echo $gdp->nama;
									} else{
										echo $gdp->username;
									}
								?>
								</div>
								<div class="profile-usertitle-job">
								<?php
									if ($this->session->userdata('hak_akses') == 'petugas') {
										echo "Petugas";
									} else if($this->session->userdata('hak_akses') == 'admin'){
										echo "Admin";
									}else if($this->session->userdata('hak_akses') == 'super_admin'){
										echo "Super Admin";
									}else if($this->session->userdata('hak_akses') == 'pengembang'){
										echo "Pengembang";
									}
								?>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-head card-topline-aqua">
							<header>Alamat</header>
						</div>
						<div class="card-body no-padding height-9">
							<div class="row text-center m-t-10">
								<div class="col-md-12">
									<p>Purwanegara
										<br> Purwokerto, Jawa Tengah.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END BEGIN PROFILE SIDEBAR -->
				<!-- BEGIN PROFILE CONTENT -->
				<div class="profile-content">
					<div class="row">
						<div class="profile-tab-box">
							<div class="p-l-20">
								<ul class="nav ">
									<li class="nav-item tab-all"><a class="nav-link active show"
											href="#tab1" data-toggle="tab">Tentang Saya</a></li>
									<li class="nav-item tab-all p-l-20"><a class="nav-link" href="#tab3"
											data-toggle="tab">Password</a></li>
								</ul>
							</div>
						</div>
						<div class="white-box">
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active fontawesome-demo" id="tab1">
									<div id="biography">
										<div class="row">
											<div class="col-md-4 col-6 b-r"> <strong>Nama Lengkap</strong>
												<br>
												<p class="text-muted">
												<?php
													if ($this->session->userdata('nama') != null) {
														echo $gdp->nama;
													} else{
														echo $gdp->username;
													}
												?>
												</p>
											</div>
											<div class="col-md-4 col-6 b-r"> <strong>No. Telpon</strong>
												<br>
												<p class="text-muted">-</p>
											</div>
											<div class="col-md-4 col-6 b-r"> <strong>Email</strong>
												<br>
												<p class="text-muted"><?= $gdp->username ?></p>
											</div>
											<!-- <div class="col-md-3 col-6"> <strong>Lokasi</strong>
												<br>
												<p class="text-muted">Indonesia</p>
											</div> -->
										</div>
										<hr>
										<h4 class="m-t-30">Tentang Saya</h4>
										<p>
										<?php
											if ($this->session->userdata('hak_akses') == 'pengembang') {
												echo $gdp->deskripsi;
											}else{
												echo "-";
											}
										?>
										</p>
									</div>
								</div>
								
								<div class="tab-pane" id="tab3">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="card-head">
												<header>Ubah Password</header>
											</div>
											<div class="card-body " id="bar-parent1">
												<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
											    	<input type="hidden" name="id" value="<?= $gdp->id ?>"/>
													<div class="form-group">
														<label for="passwordbaru">Password Baru</label>
														<input type="password" 
															class="form-control"
															id="passwordbaru"
															placeholder="contoh password : 4Dm1nR3515B4R0" 
															name="passwordbaru"
															required="">
													</div>
													<div class="form-group">
														<label for="ulangipasswordbaru">Ulangi Password Baru</label>
														<input type="password" 
															class="form-control"
															id="ulangipasswordbaru"
															placeholder="contoh password : 4Dm1nR3515" 
															name="ulangipasswordbaru"
															required="">
													</div>
													<div class="form-group">
														<label for="simpleFormPassword">Password Lama</label>
														<input type="password" 
															class="form-control"
															id="newpassword" 
															placeholder="contoh password : 4Dm1nR3515" name="passwordlama" required="">
													</div>
													<div class="form-group">
											            <div style="float: right">
												          	<a href="<?= site_url('admin/user/pengembang') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
												            <button id="btnSimpanPassword" type="submit" class="btn btn-info">
												            	<em class="fa fa-save"></em>
												                 Simpan Password
												            </button>
											            </div>
											        </div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
	<!-- end page content -->
	
</div>
<script>
$(document).ready(function(){
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanPassword').text('Menyimpan...');
    $('#btnSimpanPassword').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Profile/savePwWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	      	$('#modalPengembang').modal('hide');
	      	if (data.status == 'bedapwnya') {
		        successNotif(data.bedapw, data.status, 'error', '5000');
	      	}else if(data.status == 'pwlamasalah'){
		        successNotif(data.pwsalah, data.status, 'error', '5000');
	      	}else{
		        successNotif(data.message, data.status, 'success', '2000');
	      	}
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});
});
function successNotif(data, status, icon, time) {
  Swal.fire({
    position: 'center',
    icon: icon,
    title: data,
    showConfirmButton: false,
    timer: time,
  }).then(function() {
    if (status != 'bedapwnya' && status != 'pwlamasalah') {
    	location.reload();
	  // document.location.href='<?php echo site_url('admin/pengaturan/profile/') ?>';
    }else{
      $('#btnSimpanPassword').text('Simpan');
      $('#btnSimpanPassword').attr('disabled', false);
    }
  });
}function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanPassword').text('Simpan');
      $('#btnSimpanPassword').attr('disabled', false);
    }
  });
}
</script>