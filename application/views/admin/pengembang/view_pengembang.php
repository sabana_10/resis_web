<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Data Pengembang</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li>
            <i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">
            </i>
          </li>
          <li class="active">Data Pengembang</li>
        </ol>
      </div>
    </div>
    <!-- start Payment Details -->

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="card  card-box">
          <div class="card-head">
            <header>Detail Data Pengembang</header>
            <div class="tools">
              <a href="javascript:;" class=" box-refresh btn btn-tbl-edit btn-warning btn-xs" data-toggle="tooltip" data-placement="right" title="Refresh">
                <i class="fa fa-repeat"></i>
              </a>
              <a href="<?= site_url('admin/user/pengembang/add') ?>" id="btnAddPetugas" class="btn btn-tbl-edit btn-success btn-xs" data-toggle="tooltip" data-placement="right" title="Tambah">
                <i class="fa fa-plus"></i>
              </a>
              <!-- <a href="javascript:;" class="t-collapse btn btn-tbl-edit btn-danger btn-xs">
								<i class="fa fa-chevron-down"></i>
							</a> -->
              <!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
            </div>
          </div>
          <div class="card-body ">
            <div class="table-scrollable mt-5">
              <table id="example1" class="display full-width">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Logo</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Tahun Berdiri</th>
                    <th>Username</th>
                    <th width="120px">Menu</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($getDataPengembang as $gdp) :
                  ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td>
                        <img src="<?= site_url('assets/uploads/logo_pengembang/') . $gdp->logo ?>" alt="" class="image-responsive" width="100px" height="100px">
                      </td>
                      <td><?= $gdp->nama ?></td>
                      <td><?= $gdp->deskripsi ?></td>
                      <td><?= $gdp->tahun_berdiri ?></td>
                      <td><?= $gdp->username ?></td>
                      <td>
                        <a href="<?= site_url('admin/user/pengembang/editpw/' . md5($gdp->id)) ?>" class="btn btn-tbl-edit btn-primary btn-xs" id="EditPassword" data-toggle="tooltip" data-placement="right" title="Edit Password">
                          <i class="fa fa-key"></i>
                        </a>
                        <a href="<?= site_url('admin/user/pengembang/edit/' . md5($gdp->id)) ?>" class="btn btn-tbl-edit btn-xs" id="EditPengembang" data-toggle="tooltip" data-placement="right" title="Edit Data">
                          <i class="fa fa-pencil"></i>
                        </a>
                        <button onclick="btnDeletePengembang('<?= md5($gdp->id) ?>')" class="btn btn-tbl-delete btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus">
                          <i class="fa fa-trash-o "></i>
                        </button>
                      </td>
                    </tr>
                  <?php
                  endforeach;
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  // var url;
  // var save_method;
  // var link_img = '<?php //echo site_url('assets/admin/uploads/logo_pengembang/') 
                      ?>';
  // function btnAddPengembang() {
  // 	save_method = 'tambahPengembang';
  //     $('#form')[0].reset();
  //     $('#modalPengembang').modal('show');
  //     $('.modal-title').text('Tambah Pengembang');
  //     $('#logo-preview').hide();
  // }	
  // function btnEditPengembang(id) {
  // 	save_method = 'editPengembang';
  //   	$('#form')[0].reset();
  //   	$.ajax({
  //   		url : '<?php //echo site_url('Admin/Pengembang/getDataPengembangById/') 
                  ?>'+id,
  //   		type : 'GET',
  //   		dataType : 'JSON',
  //   		success : function (data) {
  //       		$('[name = "id"]').val(data.id);
  //       		$('[name = "nama"]').val(data.nama);
  //       		$('[name = "deskripsi"]').val(data.deskripsi);
  //       		$('[name = "username"]').val(data.username);
  //       		$('[name = "tahun_berdiri"]').val(data.tahun_berdiri);
  // 	  	    $('#modalPengembang').modal('show');
  // 	    	$('.modal-title').text('Edit Pengembang');
  // 		    $('#logo-preview').show();
  // 		    if (data.logo != '') {
  // 		      $('#logo-preview div').html('<img src="'+link_img+data.logo+'" class="img-responsive" width="100px" height="100px">');
  // 		    }
  //   		},
  //    		error: function (jqXHR, textStatus, errorThrown){
  //        		failNotif('ErrorGetData : ' + errorThrown);
  //     	}
  //     });
  // }
  // $(document).ready(function(){
  //   $('#form').on('submit', function(e) {
  //     e.preventDefault();
  //     $('#btnSimpanPengembang').text('Menyimpan...');
  //     $('#btnSimpanPengembang').attr('disabled', true);
  //     	url = '<?php //echo site_url('Admin/Pengembang/saveWithAjax') 
                  ?>';
  //     	$.ajax({
  // 	      url : url,
  // 	      method : 'POST',
  // 	      data : new FormData(this),
  // 	      contentType : false,
  // 	      cache : false,
  // 	      processData : false,
  // 	      dataType : 'JSON',
  // 	      success : function (data) {
  // 	      	$('#modalPengembang').modal('hide');
  // 	        successNotif(data.message);
  // 	      },
  // 	      error : err=>{
  // 	        failNotif('saveData', 'Gagal menambah data!');
  // 	      }
  // 	    });
  // 	});
  // });
  function btnDeletePengembang(id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })
    swalWithBootstrapButtons.fire({
      title: 'Apakah kamu yakin?',
      text: "Kamu akan kehilangan data ini setelah dihapus!",
      icon: false,
      showCancelButton: true,
      confirmButtonText: 'Yes, Hapus!',
      cancelButtonText: 'Tidak!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: "<?php echo site_url('Admin/Pengembang/deleteWithAjax') ?>/" + id,
          type: "POST",
          dataType: "JSON",
          success: function(data) {
            successNotif(data.message);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            failNotif('Data gagal dihapus!');
          }
        });
      }
    })
  }

  function successNotif(data) {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 2000,
    }).then(function() {
      location.reload();
    });
  }

  function failNotif(method = null, titles) {
    Swal.fire({
      position: 'center',
      icon: 'error',
      title: titles,
      showConfirmButton: false,
      timer: 2500,
    }).then(function() {
      if (method == 'saveData') {
        $('#btnSimpanPengembang').text('Simpan');
        $('#btnSimpanPengembang').attr('disabled', false);
      }
    });
  }
</script>
<!-- modal -->
<!-- <div class="modal fade" id="modalPengembang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title text-gray-100" id="exampleModalLabel">Tajuk</label>
        <button class="close text-gray-100" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-gray-100" style="margin-right: 30px;margin-left: 30px;">
        <form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
    	<input type="hidden" value="" name="id"/>
          <div class="form-group">
          	<label id="label-objek">
            	Nama Pengembang
            </label>
            <input type="text" class="form-control" placeholder="contoh : Resis Realestate" name="nama" required="">
          </div>
          <div class="form-group">
          	<label id="label-objek">
            	Deskripsi 
            </label>
            <textarea name="deskripsi" id="deskripsi" cols="20" rows="2" class="form-control"></textarea>
          </div>
          <div class="form-group">
          	<label id="label-objek">
            	Tahun Berdiri
            </label>
            <input type="number" class="form-control" placeholder="contoh : 1933" name="tahun_berdiri" required="">
          </div>
          <div class="form-group" id="logo-preview">
              <label class="label-objek" id="label-logo">Logo Sebelumnya</label><br>
              <div class="">
                  (No logo)
              </div>
          </div>
          <div class="form-group">
          	<label id="label-objek">
            	Upload Logo
            </label>
            <input type="file" class="form-control" name="logo">
          </div> -->
<!-- <hr> -->
<!--  <div class="form-group">
            <label id="label-objek">
            	Username 
            	<i class="fa fa-info-circle ml-1 color-gray" data-toggle="tooltip" data-placement="right" title="Username wajib menggunakan email" ></i>
            </label>
            <input type="email" class="form-control" placeholder="contoh username : admin@resis.id" name="username" required="">
          </div>
          <div class="form-group">
            <label id="label-objek">Password</label>
            <input type="password" class="form-control" placeholder="contoh password : 4Dm1nR3515" name="password" required="">
          </div>
          <br>
          <div class="form-group">
            <div style="float: right">
          	<button class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</button>
            <button id="btnSimpanPengembang" type="submit" class="btn btn-info">
            	<em class="fa fa-save"></em>
                 Simpan
            </button>
            </div>
          </div>
        </form>
      </div>
  </div>
</div> -->