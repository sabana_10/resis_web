<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Pengembang</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li >
						<a class="parent-item"href="<?= site_url('admin/user/pengembang') ?>"> Data Pengembang</a></li>&nbsp;<i class="fa fa-angle-right">		
					</i>
					<li class="active">Edit Password</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header class="mr-4">Edit Password Pengembang</header>
					<!-- 	<button id="btnLihatPw" type="button" class="btn btn-info">
			            	<em class="fa fa-eye"></em>
			                 Lihat Semua Password
			            </button> -->
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
				    	<input type="hidden" name="id" value="<?= $getDataPengembang->id ?>"/>
				          <div class="form-group">
				            <label id="label-objek">Nama Akun</label>
				            <input type="text" class="form-control" value="<?= $getDataPengembang->nama ?>" readonly>
				          </div>
				          <div class="form-group">
				            <label id="label-objek">Password Baru</label>
				            <input type="password" class="form-control" placeholder="contoh password : 4Dm1nR3515B4R0" name="passwordbaru" required="">
				          </div>
				          <div class="form-group">
				            <label id="label-objek">Ulangi Password Baru</label>
				            <input type="password" class="form-control" placeholder="contoh password : 4Dm1nR3515B4R00L4N5" name="ulangipasswordbaru" required="">
				          </div>
				          <br>
				          <div class="form-group">
				            <div style="float: right">
				          	<a href="<?= site_url('admin/user/pengembang') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
				            <button id="btnSimpanPengembang" type="submit" class="btn btn-info">
				            	<em class="fa fa-save"></em>
				                 Simpan
				            </button>
				            </div>
				          </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanPengembang').text('Menyimpan...');
    $('#btnSimpanPengembang').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Pengembang/savePwWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	      	$('#modalPengembang').modal('hide');
	      	if (data.status == 'bedapwnya') {
		        successNotif(data.bedapw, data.status, 'error', '5000');
	      	}else{
		        successNotif(data.message, data.status, 'success', '2000');
	      	}
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});
});
function successNotif(data, status, icon, time) {
  Swal.fire({
    position: 'center',
    icon: icon,
    title: data,
    showConfirmButton: false,
    timer: time,
  }).then(function() {
    if (status != 'bedapwnya') {
	  document.location.href='<?php echo site_url('admin/user/pengembang') ?>';
    }else{
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
</script>