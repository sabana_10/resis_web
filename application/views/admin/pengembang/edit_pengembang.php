<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Pengembang</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li >
						<a class="parent-item"href="<?= site_url('admin/user/pengembang') ?>"> Data Pengembang</a></li>&nbsp;<i class="fa fa-angle-right">		
					</i>
					<li class="active">Edit</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Edit Data Pengembang</header>
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
				    	<input type="hidden" name="id" value="<?= $getDataPengembang->id ?>"/>
				          <div class="form-group">
				            <label id="label-objek">
				            	Username 
				            </label>
				            <input type="email" class="form-control" placeholder="contoh username : admin@resis.id" name="username" required="" value="<?= $getDataPengembang->username ?>" readonly>
				          </div>
				          <div class="row">
				          	<div class="col-md-8">
				          		<div class="form-group">
						          	<label id="label-objek">
						            	Nama Pengembang
						            </label>
						            <input type="text" class="form-control" placeholder="contoh : Resis Realestate" name="nama" required="" value="<?= $getDataPengembang->nama ?>">
						        </div>
				          	</div>
				          	<div class="col-md-4">
					            <div class="form-group">
					          		<label id="label-objek">
					            		Tahun Berdiri
					            	</label>
					            	<input type="number" class="form-control" placeholder="contoh : 1933" name="tahun_berdiri" required="" value="<?= $getDataPengembang->tahun_berdiri ?>">
					            </div>
				          	</div>
				          </div>
				          <div class="form-group">
				          	<label id="label-objek">
				            	Deskripsi 
				            </label>
				            <textarea name="deskripsi" id="summernote" cols="20" rows="2" class="form-control"><?= $getDataPengembang->deskripsi ?></textarea>
				          </div>
				          <div class="form-group" id="logo-preview">
				              <label class="label-objek" id="label-logo">Logo Sebelumnya</label><br>
				              <div class="">
				                  <img src="<?= site_url('assets/uploads/logo_pengembang/').$getDataPengembang->logo ?>" class="img-responsive" width="100px" height="100px">
				              </div>
				          </div>
				          <div class="form-group">
				          	<label id="label-objek">
				            	Upload Logo
				            </label>
				            <input type="file" class="form-control" name="logo">
				          </div>
				        <!-- <hr> -->
				          <br>
				          <div class="form-group">
				            <div style="float: right">
				          	<a href="<?= site_url('admin/user/pengembang') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
				            <button id="btnSimpanPengembang" type="submit" class="btn btn-info">
				            	<em class="fa fa-save"></em>
				                 Simpan
				            </button>
				            </div>
				          </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanPengembang').text('Menyimpan...');
    $('#btnSimpanPengembang').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Pengembang/saveWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	      	$('#modalPengembang').modal('hide');
	        successNotif(data.message);
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});
});
function successNotif(data) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
    document.location.href='<?php echo site_url('admin/user/pengembang') ?>';
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
</script>