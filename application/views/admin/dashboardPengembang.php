<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Dashboard</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url() ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Dashboard</li>
				</ol>
			</div>
		</div>
		<div class="state-overview">
			<div class="row">
				<!-- /.col -->
				<div class="col-xl-12 col-md-12 col-12">
					<div class="info-box bg-info">
						<div class="info-box-content">
							<span class="info-box-text">Welcome back sir!</span>
							<span class="info-box-number">Real Estate Sales Information System</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
			</div>
		</div>
		<div class="state-overview">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div class="overview-panel green">
						<div class="symbol">
							<i class="fa fa-institution"></i>
						</div>
						<div class="value white">
							<p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?= $getProyek ?></p>
							<p>Proyek</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-sm-6">
					<div class="overview-panel purple">
						<div class="symbol">
							<i class="fa fa-institution"></i>
						</div>
						<div class="value white">
							<p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?= $getProduk ?></p>
							<p>Produk/Properti</p>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="overview-panel blue-bgcolor">
						<div class="symbol">
							<i class="fa fa-money"></i>
						</div>
						<div class="value white">
							<p class="sbold addr-font-h1" data-counter="counterup" data-value="3421">Rp 1.550.000.000</p>
							<p>Total Pendapatan</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- chart start -->
		<div class="row">
			<div class="col-md-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Chart Survey</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body no-padding height-9">
						<div class="row text-center">
							<div class="col-sm-3 col-6">
								<h4 class="margin-0">$ 209 </h4>
								<p class="text-muted"> Today's Income</p>
							</div>
							<div class="col-sm-3 col-6">
								<h4 class="margin-0">$ 837 </h4>
								<p class="text-muted">This Week's Income</p>
							</div>
							<div class="col-sm-3 col-6">
								<h4 class="margin-0">$ 3410 </h4>
								<p class="text-muted">This Month's Income</p>
							</div>
							<div class="col-sm-3 col-6">
								<h4 class="margin-0">$ 78,000 </h4>
								<p class="text-muted">This Year's Income</p>
							</div>
						</div>
						<div class="row">
							<div id="area_line_chart" class="width-100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>