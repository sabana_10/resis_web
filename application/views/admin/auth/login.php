<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="SmartUniversity" />
	<title>Resis - Login</title>
	<!-- icons -->
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?= site_url('assets/admin/') ?>assets/plugins/iconic/css/material-design-iconic-font.min.css">
	<!-- bootstrap -->
	<link href="<?= site_url('assets/admin/') ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- style -->
	<link rel="stylesheet" href="<?= site_url('assets/admin/') ?>assets/css/pages/extra_pages.css">
	<!-- favicon -->
	<link rel="shortcut icon" href="<?= site_url('assets/logo/resis_logo_transparent_1.png') ?>" />
	<!-- sweet alert -->
	<script src="<?= site_url('assets/admin/sweetalert/dist/') ?>sweetalert2.all.min.js"></script>
	

	<style>
		.container-login100-2 {
		    width: 100%;
		    min-height: 100vh;
		    display: -webkit-box;
		    display: -webkit-flex;
		    display: -moz-box;
		    display: -ms-flexbox;
		    display: flex;
		    flex-wrap: wrap;
		    justify-content: center;
		    align-items: center;
		    padding: 15px;
		    background-repeat: no-repeat;
		    background-position: center;
		    background-size: cover;
		    position: relative;
		    z-index: 1;
		}
		.container-login100-2::before {
		    content: "";
		    display: block;
		    position: absolute;
		    z-index: -1;
		    width: 100%;
		    height: 100%;
		    top: 0;
		    left: 0;
		    background-color: rgba(10,10,10,0.8);
		    display: block;

		}
	</style>
</head>

<body>
	<div class="limiter">
		<div class="container-login100-2 ">
		<!-- <div class="container-login100 page-background"> -->
			<div class="wrap-login100">
				<form method="POST" id="form" class="login100-form validate-form" enctype="multipart/form-data">
					<span class="login100-form-logo">
						<!-- <i class="zmdi zmdi-flower"></i> -->
						<img src="<?= site_url('assets/logo/resis_logo_transparent_1.png') ?>" alt="" class='img img-responsive img-thumbnail'>
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>
					<div class="wrap-input100" >
						<input class="input100" type="email" name="username" placeholder="Email">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100" >
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					<!-- <div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div> -->
					<div class="container-login100-form-btn">
						<button id="btnLogin" class="login100-form-btn btn-block">
							Login
						</button>
					</div>
					<div class="text-center" style="margin-top: 50px">
						<a class="txt1" href="#">
							Forgot Password?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- start js include path -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/jquery/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="<?= site_url('assets/admin/') ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= site_url('assets/admin/') ?>assets/js/pages/extra_pages/login.js"></script>
	<!-- end js include path -->
<script>
$(document).ready(function(){
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnLogin').text('Authentikasi...');
    $('#btnLogin').attr('disabled', true);
    if ($('[name = "username"]').val().length != 0) {
    	if ($('[name = "password"]').val().length != 0) {
	    	url = '<?php echo site_url('Admin/Auth/login') ?>';
	    	$.ajax({
		      url : url,
		      method : 'POST',
		      data : new FormData(this),
		      contentType : false,
		      cache : false,
		      processData : false,
		      dataType : 'JSON',
		      success : function (data) {
		        if (data.status) {
		        	successNotif(data.message, data.page);
		        } else {
			        failNotif('saveData', data.message);
		        }
		      },
		      error : err=>{
		        failNotif('saveData', 'Authentikasi gagal!');
		      }
		    });
    	} else {
    		failNotif('saveData', 'Password tidak boleh kosong!');
    	}
    } else {
    	failNotif('saveData', 'Username tidak boleh kosong!');
    }
  });
});
function successNotif(data, page) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
  	if (page != 'pengembang') {document.location.href='<?php echo site_url('admin/dashboard') ?>';}
  	else{document.location.href='<?php echo site_url('pengembang/dashboard') ?>';}
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnLogin').text('Login');
      $('#btnLogin').attr('disabled', false);
    }
  });
}
</script>
</body>

</html>