<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Produk</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li >
						<a class="parent-item"href="<?= site_url('admin/data/produk') ?>">Data Produk</a></li>&nbsp;<i class="fa fa-angle-right">		
					</i>
					<li class="active">Edit</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Edit Data Produk</header>
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
				      	<input type="hidden" value="<?= $getDataProduk->id ?>" name="id"/>
				          <div class="form-group">
				            <label id="label-objek">
				              Proyek
				            </label>
				            <input type="text" class="form-control" name="id_proyek" value="<?= $getDataProyek->nama_proyek.' - '.$getDataProyek->nama_pengembang ?>" readonly>
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Nama Produk
				            </label>
				            <input type="text" class="form-control" placeholder="contoh : Rumah gajah mada" name="nama" required="" value="<?= $getDataProduk->nama ?>">
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Deskripsi 
				            </label>
				            <textarea name="deskripsi" id="summernote" cols="20" rows="10" class="form-control"><?= $this->typography->auto_typography($getDataProduk->deskripsi) ?></textarea>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Luas Tanah / m
				                </label>
				                <input type="number" class="form-control" placeholder="00" name="luas_tanah" required="" value="<?= $getDataProduk->luas_tanah ?>">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Luas Bangunan / m
				                </label>
				                <input type="number" class="form-control" placeholder="00" name="luas_bangunan" required=""value="<?= $getDataProduk->luas_bangunan ?>">
				              </div>
				            </div>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Kamar Tidur
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="kamar_tidur"value="<?= $getDataProduk->kamar_tidur ?>">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Kamar Mandi
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="kamar_mandi"value="<?= $getDataProduk->kamar_mandi ?>">
				              </div>
				            </div>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Ruang Tamu
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="ruang_tamu" value="<?= $this->lee->getHarga($getDataProduk->ruang_tamu) ?>">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Dapur
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="dapur" value="<?= $this->lee->getHarga($getDataProduk->dapur) ?>">
				              </div>
				            </div>
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Harga Produk / Rp.
				            </label>
				            <input id="harga" type="text" class="form-control" oninput="rupiahh()" placeholder="contoh : 15.000.000" value="<?= $this->lee->getHarga($getDataProduk->harga) ?>" name="harga"  required="">
				          </div><hr>
				          <div class="form-group">
				          	<div class="checkbox checkbox-icon-black">
				          		<?php
				          			if ($getDataProduk->subsidi == 1) {
				          		?>
						          	<input type="hidden" name="subsidi" value="0">
						          	<input type="checkbox" name="subsidi" value="1" checked="checked">
				          		<?php
				          			} else {
				          		?>
				          			<input type="hidden" name="subsidi" value="0">
						          	<input type="checkbox" name="subsidi" value="1">
				          		<?php
				          			}
				          			
				          		?>
					          	<label for="label-objek">Produk bersubsidi</label>
				          	</div>
				          </div>
				          <br>
				          <div class="form-group mb-5" id="wadahBtn">
				            <div style="float: right">
				          	<a href="<?= site_url('admin/data/produk') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
				            <button id="btnSimpanProduk" type="submit" class="btn btn-info">
				            	<em class="fa fa-save"></em>
				                 Simpan
				            </button>
				            </div>
				          </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Preview Produk</header>
					</div>
					<div class="card-body">
						<form method="POST" id="formGambar" class="form-horizontal" enctype="multipart/form-data">
				      	<input type="hidden" value="<?= $getDataProduk->id ?>" name="id"/>
			            <input type="hidden" class="form-control" name="nama" required="" value="<?= $getDataProduk->nama ?>">
						<div class="row">
							<div class="col-md-12">
								<label for="">Upload Photo Produk Disini</label>
							</div>
							<div class="col-md-9">
					            <input id="gambar" type="file" class="form-control" name="gambar[]" multiple="multiple">
							</div>
							<div class="col-md-3">
					            <button id="btnUpload" type="submit" class="btn btn-info btn-block">
					                <i class="fa fa-cog"></i> Upload Photo Produk
					            </button>
							</div>
						</div>
						</form>
					</div>
					<hr>
					<div class="card-body ">
						
						<div class="table-scrollable mt-5">
				            <table id="example1" class="display full-width">
				            	<thead>
									<tr>
										<th width="20px">No</th>
										<th>Photo Produk</th>
										<th width="100px">Menu</th>
									</tr>
								</thead>
								<tbody>
								<?php $no = 1; foreach ($getPhotoProduk as $gpp) : ?>
									<tr>
										<td><?= $no++ ?></td>
										<td>
											<a href="<?= site_url('assets/uploads/photo_produk/').$gpp->gambar ?>">
												<img src="<?= site_url('assets/uploads/thumbnails_produk/medium/').$gpp->gambar ?>" alt="" class="img img-responsive" style="width: 150px; height: 95px">	
											</a>
										</td>
										<td>
											<button type="button" onclick="btnDeleteProduk('<?= md5($gpp->id) ?>')" class="btn btn-danger btn-block">
												<i class="fa fa-times "></i>
												 Delete
											</button>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
				            </table>
				        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$(document).ready(function(){
  	// console.log($('#subsidi').val());
  //ubah data
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanProduk').text('Menyimpan...');
    $('#btnSimpanProduk').attr('disabled', true);
	url = '<?php echo site_url('Admin/Produk/saveWithAjax') ?>';
	$.ajax({
      url : url,
      method : 'POST',
      data : new FormData(this),
      contentType : false,
      cache : false,
      processData : false,
      dataType : 'JSON',
      success : function (data) {
      	if (data.status == true) {
	        successNotif(data.message, data.status, 'success', 2000, 'data');
      	}else{
	        successNotif(data.errorintruth, data.status, 'error', 5000);
      	}
      },
      error : err=>{
        failNotif('saveData', 'Gagal menambah data!');
      }
    });
  });
  //Add photo
  $('#formGambar').on('submit', function(e) {
    e.preventDefault();
    $('#btnUpload').text('Menyimpan...');
    $('#btnUpload').attr('disabled', true);
	url = '<?php echo site_url('Admin/Produk/saveGambar') ?>';
	$.ajax({
      url : url,
      method : 'POST',
      data : new FormData(this),
      contentType : false,
      cache : false,
      processData : false,
      dataType : 'JSON',
      success : function (data) {
      	if (data.status == true) {
	        successNotif(data.message, data.status, 'success', 2000, 'data');
      	}else{
	        successNotif(data.errorintruth, data.status, 'error', 5000);
      	}
      },
      error : err=>{
        failNotif('saveData', 'Gagal menambah data!');
      }
    });
  });
});
function rupiahh() {
	const hrg = $('#harga').val();
  	var number_string = hrg.replace(/[^,\d]/g, '').toString(),
		split   		= number_string.split(','),
		sisa     		= split[0].length % 3,
		rupiah     		= split[0].substr(0, sisa),
		ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
  	if (ribuan) {
  		separator = sisa? '.' : '';
  		rupiah += separator + ribuan.join('.');
  	}

	$('#harga').val(rupiah);
}
function btnDeleteProduk(id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })
  swalWithBootstrapButtons.fire({
    title: 'Apakah kamu yakin?',
    text: "Kamu akan kehilangan data ini setelah dihapus!",
    icon: false,
    showCancelButton: true,
    confirmButtonText: 'Yes, Hapus!',
    cancelButtonText: 'Tidak!',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url : "<?php echo site_url('Admin/Produk/deleteWithAjax2')?>/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          successNotif(data.message, data.status, 'success', 2000, id);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          failNotif('saveData', 'Data gagal dihapus!');
        }
      });
    }
  })
}
function successNotif(data, status, icon, time, id = null) {
  Swal.fire({
    position: 'center',
    icon: icon,
    title: data,
    showConfirmButton: false,
    timer: time,
  }).then(function() {
    if (status) {
    	if (id) {
		  location.reload();
    	}else{
		  document.location.href="<?php echo site_url('admin/data/produk') ?>";
    	}
    }else{
      $('#btnSimpanProduk').text('Simpan');
      $('#btnSimpanProduk').attr('disabled', false);
    }
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanProduk').text('Simpan');
      $('#btnSimpanProduk').attr('disabled', false);
    }
  });
}
</script>