<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Produk</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li>
						<a class="parent-item" href="<?= site_url('admin/data/produk') ?>">Data Produk</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Detail</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head text-center">
						<header class=" mt-3 mb-3">
							<?= $getDataProduk->nama ?>
						</header>
						<a href="<?= site_url('admin/data/produk') ?>" class="btn btn-warning text-gray-100 mr-3 mt-3 mb-3" type="button" data-dismiss="modal" style="float: right">
							Kembali
						</a>	
					</div>
					<div class="card-body mb-5">
						
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-bank"></span>
							</div>
							<div class="col-md-2">
								Proyek
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->nama_proyek ?></div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-user"></span>
							</div>
							<div class="col-md-2">
								Pengembang
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->nama_pengembang ?></div>
						</div><hr>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-area-chart"></span>
							</div>
							<div class="col-md-2">
								Luas Tanah
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->luas_tanah ?> m²</div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-area-chart"></span>
							</div>
							<div class="col-md-2">
								Luas Bangunan
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->luas_bangunan ?> m²</div>
						</div><hr>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-television"></span>
							</div>
							<div class="col-md-2">
								Ruang Tamu
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->ruang_tamu ?></div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-bed"></span>
							</div>
							<div class="col-md-2">
								Kamar Tidur
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->kamar_tidur ?></div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-bathtub"></span>
							</div>
							<div class="col-md-2">
								Kamar Mandi
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->kamar_mandi ?></div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-cutlery"></span>
							</div>
							<div class="col-md-2">
								Dapur
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $getDataProduk->dapur ?></div>
						</div><hr>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-money"></span>
							</div>
							<div class="col-md-2">
								Harga Produk
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $this->lee->getRupiah($getDataProduk->harga) ?></div>
						</div><hr>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-file-image-o"></span>
							</div>
							<div class="col-md-2">
								Deskripsi
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7"><?= $this->typography->auto_typography($getDataProduk->deskripsi); ?></div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<span class="fa fa-area-chart"></span>
							</div>
							<div class="col-md-2">
								Preview Produk
							</div>
							<div class="col-md-1"> : </div>
							<div class="col-md-7">
								<?php 
								foreach ($getPhotoProduk as $gpp) {
									echo '
									<a href="'.site_url('assets/uploads/photo_produk/').$gpp->gambar_thumb.'" title="preview">
										<img src="'.site_url('assets/uploads/thumbnails_produk/medium/').$gpp->gambar_thumb.'" alt="" width="200px" height="200px" class="img img-responsive" style="margin: 10px; float:left;">
									</a>
									';	
								} 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Table Detail Produk</header>
					</div>
					<div class="card-body ">
						<div class="table-scrollable mt-5">
			            <table id="example1" class="display full-width">
			                <thead>
								<tr>
									<th width="50px">No</th>
				                    <th>Nama</th>
									<th width="100px">Status</th>
									<th width="100px">Menu</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>hardcode</td>
									<td>
										<button type="button"  class="btn btn-block btn-success">
											<i class="fa fa-check "></i> Tersedia
										</button>
										<button type="button"  class="btn btn-block btn-danger">
											<i class="fa fa-check "></i> Sold Out
										</button>
									</td>
									<td>-</td>
								</tr>	
							</tbody>
						</table>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
  $('#form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSimpanPengembang').text('Menyimpan...');
    $('#btnSimpanPengembang').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Produk/saveWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	      	if (data.status == true) {
		        successNotif(data.message, data.status, 'success', 2000);
	      	}else{
		        successNotif(data.errorintruth, data.status, 'error', 5000);
	      	}
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});
});
function successNotif(data, status, icon, time) {
  Swal.fire({
    position: 'center',
    icon: icon,
    title: data,
    showConfirmButton: false,
    timer: time,
  }).then(function() {
    if (status) {
	  document.location.href='<?php echo site_url('admin/data/produk') ?>';
    }else{
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
</script>