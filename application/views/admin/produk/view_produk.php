<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Produk</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li class="active">Data Produk</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Detail Data Produk</header>
						<div class="tools">
							<a href="javascript:;" class=" box-refresh btn btn-tbl-edit btn-warning btn-xs">
								<i class="fa fa-repeat"></i>
							</a>
							<a href="<?= site_url('admin/data/produk/add') ?>" id="btnAddProduk" class="btn btn-tbl-edit btn-success btn-xs" data-toggle="tooltip" data-placement="right" title="Tambah">
                <i class="fa fa-plus"></i>
              </a>
							<!-- <a href="javascript:;" class="t-collapse btn btn-tbl-edit btn-danger btn-xs">
								<i class="fa fa-chevron-down"></i>
							</a> -->
							<!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
						</div>
					</div>
					<div class="card-body ">
						<div class="table-scrollable mt-5">
            <table id="example1" class="display full-width">
                <thead>
									<tr>
										<th>ID</th>
                    <th>Proyek & Pengembang</th>
										<th>Nama Produk</th>
                    <!-- <th>Deskripsi</th> -->
                    <th>Luas Tanah</th>
                    <th>Luas Bangunan</th>
                    <th>Harga</th>
										<th width="100px">Menu</th>
									</tr>
								</thead>
								<tbody>
									<?php
                    $no = 1;
										foreach ($getDataProduk as $gdp) :
									?>
									<tr>
                    <td><?= $no++ ?></td>
                    <td><?= $gdp->nama_proyek." - ".$gdp->nama_pengembang ?></td>
                    <td><?= $gdp->nama ?></td>
                    <!-- <td><p><?= substr($this->typography->auto_typography($gdp->deskripsi), 0, 150).'...' ?></p></td> -->
                    <td><?= $gdp->luas_tanah ?> m²</td>
                    <td><?= $gdp->luas_bangunan ?> m²</td>
                    <td><?= $this->lee->getRupiah($gdp->harga) ?></td>
										<td>
											<a href="<?= site_url('admin/data/produk/detailproduk/'.md5($gdp->id)) ?>" class="btn btn-tbl-edit btn-primary btn-xs" id="DetailProduk" data-toggle="tooltip" data-placement="right" title="Detail Produk">
                        <i class="fa fa-eye"></i>
                      </a>
                      <a href="<?= site_url('admin/data/produk/edit/'.md5($gdp->id)) ?>" class="btn btn-tbl-edit btn-xs" id="EditProduk" data-toggle="tooltip" data-placement="right" title="Edit Data">
                        <i class="fa fa-pencil"></i>
                      </a>
											<button type="button" onclick="btnDeleteProduk('<?= md5($gdp->id) ?>')" class="btn btn-tbl-delete btn-xs">
												<i class="fa fa-trash-o "></i>
											</button>
										</td>
									</tr>
									<?php
  										endforeach;
									?>
								</tbody>
              </table>
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
var url;
var save_method;
function btnAddProduk() {
	save_method = 'tambahProduk';
    $('#form')[0].reset();
    $('#modalProduk').modal('show');
    $('.modal-title').text('Tambah Produk');
    $('#logo-preview').hide();
}	
function btnEditProduk(id) {
	save_method = 'editProduk';
  	$('#form')[0].reset();
  	$.ajax({
  		url : '<?php echo site_url('Admin/Produk/getDataProdukById/') ?>'+id,
  		type : 'GET',
  		dataType : 'JSON',
  		success : function (data) {
    		$('[name = "id"]').val(data.id);
        // $('[name = "id_proyek"]').val(data.id_pengembang);
        $('[name = "nama"]').val(data.nama);
        $('[name = "deskripsi"]').val(data.deskripsi);
        $('[name = "luas_tanah"]').val(data.luas_tanah);
        $('[name = "luas_bangunan"]').val(data.luas_bangunan);
        $('[name = "kamar_tidur"]').val(data.kamar_tidur);
        $('[name = "kamar_mandi"]').val(data.kamar_mandi);
        $('[name = "harga"]').val(data.harga);
        $('#modalProduk').modal('show');
	    	$('.modal-title').text('Edit Produk');
  		},
   		error: function (jqXHR, textStatus, errorThrown){
       		failNotif('ErrorGetData : ' + errorThrown);
    	}
    });
}
// $(document).ready(function(){
//   $('#form').on('submit', function(e) {
//     e.preventDefault();
//     $('#btnSimpanProduk').text('Menyimpan...');
//     $('#btnSimpanProduk').attr('disabled', true);
//     	url = '<?php echo site_url('Admin/Produk/saveWithAjax') ?>';
//     	$.ajax({
// 	      url : url,
// 	      method : 'POST',
// 	      data : new FormData(this),
// 	      contentType : false,
// 	      cache : false,
// 	      processData : false,
// 	      dataType : 'JSON',
// 	      success : function (data) {
// 	      	$('#modalProduk').modal('hide');
// 	        successNotif(data.message);
// 	      },
// 	      error : err=>{
// 	        failNotif('saveData', 'Gagal menambah data!');
// 	      }
// 	    });
// 	});
// });
function btnDeleteProduk(id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })
  swalWithBootstrapButtons.fire({
    title: 'Apakah kamu yakin?',
    text: "Kamu akan kehilangan data ini setelah dihapus!",
    icon: false,
    showCancelButton: true,
    confirmButtonText: 'Yes, Hapus!',
    cancelButtonText: 'Tidak!',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url : "<?php echo site_url('Admin/Produk/deleteWithAjax')?>/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          successNotif(data.message);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          failNotif('Data gagal dihapus!');
        }
      });
    }
  })
}
function successNotif(data) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
    location.reload();
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanProduk').text('Simpan');
      $('#btnSimpanProduk').attr('disabled', false);
    }
  });
}
</script>

<!-- modal -->
<!-- <div class="modal fade" id="modalProduk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title text-gray-100" id="exampleModalLabel">Tajuk</label>
        <button class="close text-gray-100" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-gray-100" style="margin-right: 30px;margin-left: 30px;">
        <form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
      	<input type="hidden" value="" name="id"/>
          <div class="form-group">
            <label id="label-objek">
              Proyek
            </label>
            <select name="id_proyek" class="form-control">
            <?php foreach ($getDataProyek as $gdpr) : ?>
              <option value="<?= $gdpr->id ?>"><?= $gdpr->nama_proyek.' - '.$gdpr->nama_pengembang ?></option>
            <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label id="label-objek">
              Nama Produk
            </label>
            <input type="text" class="form-control" placeholder="contoh : Rumah gajah mada" name="nama" required="">
          </div>
          <div class="form-group">
            <label id="label-objek">
              Deskripsi 
            </label>
            <textarea name="deskripsi" cols="20" rows="2" class="form-control"></textarea>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label id="label-objek">
                  Luas Tanah / m
                </label>
                <input type="number" class="form-control" placeholder="00" name="luas_tanah" required="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label id="label-objek">
                  Luas Bangunan / m
                </label>
                <input type="number" class="form-control" placeholder="00" name="luas_bangunan" required="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label id="label-objek">
                  Kamar Tidur
                </label>
                <input type="number" class="form-control" placeholder="0" name="kamar_tidur" required="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label id="label-objek">
                  Kamar Mandi
                </label>
                <input type="number" class="form-control" placeholder="0" name="kamar_mandi" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label id="label-objek">
              Harga Produk / Rp.
            </label>
            <input type="text" class="form-control" placeholder="contoh : 15000000" name="nama" required="">
          </div>
          <br>
          <div class="form-group mb-5" id="wadahBtn">
            <div style="float: right">
          	<button class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</button>
            <button id="btnSimpanProduk" type="submit" class="btn btn-info">
            	<em class="fa fa-save"></em>
                 Simpan
            </button>
            </div>
          </div>
        </form>
      </div>
  </div>
</div> -->