<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Data Produk</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li>
						<i class="fa fa-home"></i>&nbsp;<a class="parent-item"
							href="<?= site_url('admin/dashboard') ?>">Home</a>&nbsp;<i class="fa fa-angle-right">		
						</i>
					</li>
					<li >
						<a class="parent-item"href="<?= site_url('admin/data/produk') ?>">Data Produk</a></li>&nbsp;<i class="fa fa-angle-right">		
					</i>
					<li class="active">Tambah</li>
				</ol>
			</div>
		</div>
		<!-- start Payment Details -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-head">
						<header>Tambah Data Produk</header>
					</div>
					<div class="card-body ">
						<form method="POST" id="form" class="form-horizontal" enctype="multipart/form-data">
				      	<input type="hidden" value="" name="id"/>
				          <div class="form-group">
				            <label id="label-objek">
				              Proyek <small class="text-danger">wajib</small>
				            </label>
				            <select name="id_proyek" class="form-control">
				            <?php
				            	if ($this->session->userdata('hak_akses')=='pengembang') {
				            		foreach ($getDataProyek as $gdp) :				            			
				            ?>
				              <option value="<?= $gdp->id ?>"><?= $gdp->nama_proyek.' - '.$gdp->nama_pengembang ?></option>
				            <?php
				            		endforeach;
				            	} else {
					            	foreach ($getDataProyek as $gdpr) :
				            ?>
				              <option value="<?= $gdpr->id ?>"><?= $gdpr->nama_proyek.' - '.$gdpr->nama_pengembang ?></option>
				            <?php 
				        			endforeach;
				        		}
				        	?>
				            </select>
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Nama Produk <small class="text-danger">wajib</small>
				            </label>
				            <input type="text" class="form-control" placeholder="contoh : Rumah gajah mada" name="nama" required="">
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Deskripsi 
				            </label>
				            <textarea name="deskripsi" id="summernote" cols="20" rows="5" class="form-control"></textarea>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Luas Tanah / m <small class="text-danger">wajib</small>
				                </label>
				                <input type="number" class="form-control" placeholder="00" name="luas_tanah" required="" value="0">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Luas Bangunan / m <small class="text-danger">wajib</small>
				                </label>
				                <input type="number" class="form-control" placeholder="00" name="luas_bangunan" required="" value="0">
				              </div>
				            </div>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Kamar Tidur
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="kamar_tidur" value="0">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Kamar Mandi
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="kamar_mandi" value="0">
				              </div>
				            </div>
				          </div>
				          <div class="row">
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Ruang Tamu
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="ruang_tamu" value="0">
				              </div>
				            </div>
				            <div class="col-md-6">
				              <div class="form-group">
				                <label id="label-objek">
				                  Dapur
				                </label>
				                <input type="number" class="form-control" placeholder="0" name="dapur" value="0">
				              </div>
				            </div>
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Harga Produk / Rp. <small class="text-danger">wajib</small>
				            </label>
				            <input id="harga" type="text" class="form-control" placeholder="contoh : 15.000.000" name="harga" oninput="rupiahh()" required="">
				          </div>
				          <div class="form-group">
				            <label id="label-objek">
				              Photo Produk  <small class="text-danger">wajib</small>
				            </label><br>
				            <label>*Sebelum upload Photo silahkan baca ketentuan photo 
				            </label>
			            	<button class="btn btn-success" type="button" data-toggle="modal" data-target="#ketentuanPhotoProduk">klik disini.</button>
				            <br><br>
				            <input id="gambar" type="file" class="form-control" name="gambar[]" multiple="multiple" required="">
				          </div><hr>
				          <div class="form-group">
				          	<div class="checkbox checkbox-icon-black">
					          	<input type="hidden" name="subsidi" value="0">
					          	<input type="checkbox" name="subsidi" value="1">
					          	<label for="label-objek">Produk bersubsidi</label>
				          	</div>
				          </div>
				          <br>
				          <div class="form-group mb-5" id="wadahBtn">
				            <div style="float: right">
				          	<a href="<?= site_url('admin/data/produk') ?>" class="btn btn-transparent text-gray-100 mr-2" type="button" data-dismiss="modal">Kembali</a>
				            <button id="btnSimpanProduk" type="submit" class="btn btn-info">
				            	<em class="fa fa-save"></em>
				                 Simpan
				            </button>
				            </div>
				          </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$(document).ready(function(){

    $('#form').on('submit', function(e) {
	    e.preventDefault();
	    $('#btnSimpanPengembang').text('Menyimpan...');
	    $('#btnSimpanPengembang').attr('disabled', true);
    	url = '<?php echo site_url('Admin/Produk/saveWithAjax') ?>';
    	$.ajax({
	      url : url,
	      method : 'POST',
	      data : new FormData(this),
	      contentType : false,
	      cache : false,
	      processData : false,
	      dataType : 'JSON',
	      success : function (data) {
	      	if (data.status == true) {
		        successNotif(data.message, data.status, 'success', 2000);
	      	}else{
		        successNotif(data.errorintruth, data.status, 'error', 5000);
	      	}
	      },
	      error : err=>{
	        failNotif('saveData', 'Gagal menambah data!');
	      }
	    });
	});

    //checkbox

});
function rupiahh() {
	const hrg = $('#harga').val();
  	var number_string = hrg.replace(/[^,\d]/g, '').toString(),
		split   		= number_string.split(','),
		sisa     		= split[0].length % 3,
		rupiah     		= split[0].substr(0, sisa),
		ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
  	if (ribuan) {
  		separator = sisa? '.' : '';
  		rupiah += separator + ribuan.join('.');
  	}

	$('#harga').val(rupiah);
}
function successNotif(data, status, icon, time) {
  Swal.fire({
    position: 'center',
    icon: icon,
    title: data,
    showConfirmButton: false,
    timer: time,
  }).then(function() {
    if (status) {
	  document.location.href='<?php echo site_url('admin/data/produk') ?>';
    }else{
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
</script>

<!-- Modal -->
<div class="modal fade" id="ketentuanPhotoProduk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ketentuan Photo Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ol>
        	<li>Photo harus Orisinil milik pengembang</li>
        	<li>Photo harus <i>high resolution</i></li>
        	<li><i>Size</i> maksimal : 10mb</li>
        	<li><i>Height</i> minimum : 400px</li>
        	<li><i>Width</i> minimum : 700px</li>
        	<li>Pengembang dapat upload photo lebih dari 1</li>
        	<li>Support format : jpg | jpeg | png | JPG | JPEG | PNG</li>
        	<li>
        		Jika pengembang upload photo lebih dari 1 photo, maka format photo tidak boleh berbeda. contoh : 
        		<ul>
        			<li>file-1.<i class="text-success">jpg</i> dan file-2.<i class="text-success">jpg</i>   <span class="fa fa-check text-success"></span></li>
        			<li>file-beda-format.<i class="text-danger">png</i> dan file-beda-format-2.<i class="text-danger">jpg</i>   <span class="fa fa-times text-danger"></span></li>
        		</ul>
        	</li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Mengerti</button>
      </div>
    </div>
  </div>
</div>