<div class="page-header navbar navbar-fixed-top">
	<div class="page-header-inner ">
		<!-- logo start -->
		<div class="page-logo">
			<a href="<?= site_url('admin/dashboard') ?>">
				<img alt="" src="<?= site_url('assets/logo/') ?>resis_logo_transparent_1.png" width="32" style="margin-top: -5px">
				<span class="logo-default">Resis</span> </a>
		</div>
		<!-- logo end -->
		<ul class="nav navbar-nav navbar-left in">
			<li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
		</ul>
		<!-- <form class="search-form-opened" action="#" method="GET">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Search..." name="query">
				<span class="input-group-btn search-btn">
					<a href="javascript:;" class="btn submit">
						<i class="icon-magnifier"></i>
					</a>
				</span>
			</div>
		</form> -->
		<!-- start mobile menu -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span></span>
		</a>
		<!-- end mobile menu -->
		<!-- start header menu -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- start manage user dropdown -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
						data-close-others="true">
						<img alt="" class="img-circle " src="<?= site_url('assets/uploads/photo_petugas/default.png') ?>" />
						<span class="username username-hide-on-mobile"> 
							<?php
								if ($this->session->userdata('nama') != null) {
									echo $this->session->userdata('nama');
								} else{
									echo $this->session->userdata('username');
								}
							?>
						</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default animated jello">
						<li>
							<a href="<?= site_url('admin/pengaturan/profile/').md5($this->session->userdata('id')); ?>">
								<i class="icon-user"></i> Profile </a>
						</li>
						<!-- <li>
							<a href="#">
								<i class="icon-settings"></i> Settings
							</a>
						</li>
						<li>
							<a href="#">
								<i class="icon-directions"></i> Help
							</a>
						</li> -->
						<!-- <li class="divider"> </li>
						<li>
							<a href="lock_screen.html">
								<i class="icon-lock"></i> Lock
							</a>
						</li> -->
						<li>
							<a onclick="btnLogout()">
								<i class="icon-logout"></i> Log Out 
							</a>
						</li>
					</ul>
				</li>
				<!-- end manage user dropdown -->
			</ul>
		</div>
	</div>
</div>

<script>
function btnLogout() {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })
  swalWithBootstrapButtons.fire({
    title: 'Apakah kamu yakin akan keluar?',
    //text: "Kamu akan kehilangan data ini setelah dihapus!",
    icon: false,
    showCancelButton: true,
    confirmButtonText: 'keluar!',
    cancelButtonText: 'Kembali',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url : "<?php echo site_url('Admin/Auth/logout')?>",
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          successNotif(data.message);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          failNotif('Data gagal dihapus!');
        }
      });
    }
  })
}
function successNotif(data) {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 2000,
  }).then(function() {
    document.location.href='<?php echo site_url('admin/login') ?>';
  });
}
function failNotif(method = null, titles) {
  Swal.fire({
    position: 'center',
    icon: 'error',
    title: titles,
    showConfirmButton: false,
    timer: 2500,
  }).then(function() {
    if (method == 'saveData') {
      $('#btnSimpanPengembang').text('Simpan');
      $('#btnSimpanPengembang').attr('disabled', false);
    }
  });
}
</script>