<div class="sidebar-container">
	<div class="sidemenu-container navbar-collapse collapse fixed-menu">
		<div id="remove-scroll">
			<ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<li class="sidebar-toggler-wrapper hide">
					<div class="sidebar-toggler">
						<span></span>
					</div>
				</li>
				<li class="sidebar-user-panel">
					<div class="user-panel">
						<div class="row">
							<div class="sidebar-userpic">
								<img src="<?= site_url('assets/uploads/photo_petugas/default.png') ?>" class="img-responsive" alt="">
							</div>
						</div>
						<div class="profile-usertitle">
							<div class="sidebar-userpic-name">
								<?php
								if ($this->session->userdata('nama') != null) {
									echo $this->session->userdata('nama');
								} else {
									echo $this->session->userdata('username');
								}
								?>
							</div>
							<div class="profile-usertitle-job">
								<?php
								if ($this->session->userdata('hak_akses') == 'petugas') {
									echo "Petugas";
								} else if ($this->session->userdata('hak_akses') == 'admin') {
									echo "Admin";
								} else if ($this->session->userdata('hak_akses') == 'super_admin') {
									echo "Super Admin";
								} else if ($this->session->userdata('hak_akses') == 'pengembang') {
									echo "Pengembang";
								}
								?>
							</div>
						</div>
						<div class="sidebar-userpic-btn">
							<a class="tooltips" href="<?= site_url('admin/pengaturan/profile/') . md5($this->session->userdata('id')); ?>" data-placement="top" data-original-title="Profile">
								<i class="material-icons">person_outline</i>
							</a>
							<!-- <a class="tooltips" href="email_inbox.html" data-placement="top"
								data-original-title="Mail">
								<i class="material-icons">mail_outline</i>
							</a>
							<a class="tooltips" href="chat.html" data-placement="top"
								data-original-title="Chat">
								<i class="material-icons">chat</i>
							</a> -->
							<a class="tooltips" data-placement="top" data-original-title="Logout" onclick="btnLogout()">
								<i class="material-icons">input</i>
							</a>
						</div>
					</div>
				</li>
				<?php if ($this->session->userdata('hak_akses') == 'super_admin') {
				?>
					<li class="nav-item <?= ($this->uri->segment('2') == 'dashboard') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/dashboard') ?>" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
							<span class="title">Dashboard</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'data') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">business</i>
							<span class="title">Management Project</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'data') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'project') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/data/project') ?>" class="nav-link ">
									<span class="title">Data Project</span>
									<span class="selected"></span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'produk') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/data/produk') ?>" class="nav-link ">
									<span class="title">Data Produk</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'user') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">account_box</i>
							<span class="title">Management User</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'user') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'pengembang') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/user/pengembang') ?>" class="nav-link ">
									<span class="title">Data Pengembang</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'petugas') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/user/petugas') ?>" class="nav-link ">
									<span class="title">Data Petugas</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'admin') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/user/admin') ?>" class="nav-link ">
									<span class="title">Data Admin</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'promo') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/promo') ?>" class="nav-link nav-toggle"> <i class="fa fa-folder"></i>
							<span class="title">Management Promo</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'berita') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/berita') ?>" class="nav-link nav-toggle"> <i class="fa fa-folder-open"></i>
							<span class="title">Berita Property</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'pengaturan') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title">Pengaturan</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == '') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'profile') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/profile/') . md5($this->session->userdata('id')); ?>" class="nav-link ">
									<span class="title">Profile</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'faqs') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/faqs') ?>" class="nav-link ">
									<span class="title">Faqs</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link" onclick="btnLogout()">
									<span class="title">Logout</span>
								</a>
							</li>
						</ul>
					</li>
				<?php
				} else if ($this->session->userdata('hak_akses') == 'admin') {
				?>
					<li class="nav-item <?= ($this->uri->segment('2') == 'dashboard') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/dashboard') ?>" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
							<span class="title">Dashboard</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'data') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">business</i>
							<span class="title">Management Project</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'data') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'project') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/data/project') ?>" class="nav-link ">
									<span class="title">Data Project</span>
									<span class="selected"></span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'produk') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/data/produk') ?>" class="nav-link ">
									<span class="title">Data Produk</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'user') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">account_box</i>
							<span class="title">Management User</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'user') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'pengembang') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/user/pengembang') ?>" class="nav-link ">
									<span class="title">Data Pengembang</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'petugas') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/user/petugas') ?>" class="nav-link ">
									<span class="title">Data Petugas</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'promo') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/promo') ?>" class="nav-link nav-toggle"> <i class="fa fa-folder"></i>
							<span class="title">Management Promo</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'berita') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/berita') ?>" class="nav-link nav-toggle"> <i class="fa fa-folder-open"></i>
							<span class="title">Berita Property</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'pengaturan') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title">Pengaturan</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == '') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'profile') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/profile/') . md5($this->session->userdata('id')); ?>" class="nav-link ">
									<span class="title">Profile</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'faqs') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/faqs') ?>" class="nav-link ">
									<span class="title">Faqs</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link" onclick="btnLogout()">
									<span class="title">Logout</span>
								</a>
							</li>
						</ul>
					</li>
				<?php
				} else if ($this->session->userdata('hak_akses') == 'petugas') {
				?>
					<li class="nav-item <?= ($this->uri->segment('2') == 'dashboard') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/dashboard') ?>" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
							<span class="title">Dashboard</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'data') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">business</i>
							<span class="title">Management Project</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'data') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'project') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/data/project') ?>" class="nav-link ">
									<span class="title">Data Project</span>
									<span class="selected"></span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'produk') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/data/produk') ?>" class="nav-link ">
									<span class="title">Data Produk</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'user') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">account_box</i>
							<span class="title">Management User</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'user') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'pengembang') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/user/pengembang') ?>" class="nav-link ">
									<span class="title">Data Pengembang</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'promo') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/promo') ?>" class="nav-link nav-toggle"> <i class="fa fa-folder"></i>
							<span class="title">Management Promo</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'berita') ? 'active' : '' ?>">
						<a href="<?= site_url('admin/berita') ?>" class="nav-link nav-toggle"> <i class="fa fa-folder-open"></i>
							<span class="title">Berita Property</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'pengaturan') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title">Pengaturan</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == '') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'profile') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/profile/') . md5($this->session->userdata('id')); ?>" class="nav-link ">
									<span class="title">Profile</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'faqs') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/faqs') ?>" class="nav-link ">
									<span class="title">Faqs</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link" onclick="btnLogout()">
									<span class="title">Logout</span>
								</a>
							</li>
						</ul>
					</li>
				<?php
				} else if ($this->session->userdata('hak_akses') == 'pengembang') {
				?>
					<li class="nav-item <?= ($this->uri->segment('2') == 'dashboard') ? 'active' : '' ?>">
						<a href="<?= site_url('pengembang/dashboard') ?>" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
							<span class="title">Dashboard</span>
						</a>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'data') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="material-icons">business</i>
							<span class="title">Management Project</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == 'data') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'project') ? 'active' : '' ?>">
								<a href="<?= site_url('pengembang/data/project') ?>" class="nav-link ">
									<span class="title">Data Project</span>
									<span class="selected"></span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'produk') ? 'active' : '' ?>">
								<a href="<?= site_url('pengembang/data/produk') ?>" class="nav-link ">
									<span class="title">Data Produk</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item <?= ($this->uri->segment('2') == 'pengaturan') ? 'start active' : '' ?>">
						<a href="#" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title">Pengaturan</span>
							<span class="selected"></span>
							<span class="arrow <?= ($this->uri->segment('2') == '') ? 'open' : '' ?>"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item <?= ($this->uri->segment('3') == 'profile') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/profile/') . md5($this->session->userdata('id')); ?>" class="nav-link ">
									<span class="title">Profile</span>
								</a>
							</li>
							<li class="nav-item <?= ($this->uri->segment('3') == 'faqs') ? 'active' : '' ?>">
								<a href="<?= site_url('admin/pengaturan/faqs') ?>" class="nav-link ">
									<span class="title">Faqs</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link" onclick="btnLogout()">
									<span class="title">Logout</span>
								</a>
							</li>
						</ul>
					</li>
				<?php
				}
				?>
			</ul>
		</div>
	</div>
</div>
<script>
	function btnLogout() {
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: true
		})
		swalWithBootstrapButtons.fire({
			title: 'Apakah kamu yakin akan keluar?',
			//text: "Kamu akan kehilangan data ini setelah dihapus!",
			icon: false,
			showCancelButton: true,
			confirmButtonText: 'keluar!',
			cancelButtonText: 'Kembali',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: "<?php echo site_url('Admin/Auth/logout') ?>",
					type: "POST",
					dataType: "JSON",
					success: function(data) {
						successNotif(data.message);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						failNotif('Data gagal dihapus!');
					}
				});
			}
		})
	}

	function successNotif(data) {
		Swal.fire({
			position: 'center',
			icon: 'success',
			title: data,
			showConfirmButton: false,
			timer: 2000,
		}).then(function() {
			document.location.href = '<?php echo site_url('admin/login') ?>';
		});
	}

	function failNotif(method = null, titles) {
		Swal.fire({
			position: 'center',
			icon: 'error',
			title: titles,
			showConfirmButton: false,
			timer: 2500,
		}).then(function() {
			if (method == 'saveData') {
				$('#btnSimpanPengembang').text('Simpan');
				$('#btnSimpanPengembang').attr('disabled', false);
			}
		});
	}
</script>