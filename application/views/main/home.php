 <div class="rev_slider_wrapper fullscreen-container">
    <div id="fullscreen_hero_video" class="rev_slider fullscreenbanner gradient_slider" style="display:none">
        <ul>

            <li data-transition="fade">
                <!-- MAIN IMAGE -->
                <img src="<?= site_url('assets/main/') ?>images/slider/video_fullscreen.jpg" 
                     alt="Image" 
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat" 
                     data-bgparallax="3" 
                     class="rev-slidebg" 
                     data-no-retina>
                <!-- VIDEO -->
              <!--   <div id="bg-video" 
                    class="rs-background-video-layer" 
                    data-ready="true"
                    data-forcerewind="on" 
                    data-volume="mute" 
                    data-videomp4="<?= site_url('assets/main/') ?>videos/hero_video.mp4"
                    data-videoattributes="title=0&amp;byline=0&amp;portrait=0&amp;api=1" 
                    data-videowidth="100%" 
                    data-videoheight="100%" 
                    data-videocontrols="none" 
                    data-videostartat="00:00" 
                    data-videoendat="" 
                    data-videoloop="loop" 
                    data-forceCover="1" 
                    data-aspectratio="4:3" 
                    data-autoplay="true" 
                    loop="true"
                    data-autoplayonlyfirsttime="false" 
                    data-nextslideatend="true">
                </div> -->

                <video 
                  class="rs-background-video-layer video-gold" 
                  autoplay 
                  loop 
                  playsinline 
                  muted 
                  name="media-bg">
                  <source src="<?= site_url('assets/main/') ?>videos/hero_video.mp4" 
                    type="video/mp4"
                    min-height="100%"
                    min-width="100%"
                    style="background-size: cover;">
                </video>
                <!-- LAYER NR. 1 -->
                <div class="tp-caption gradient_gold" 
                     data-x="['center','center','center','center']" 
                     data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" 
                     data-voffset="['0','0','0','0']" 
                     data-width="full" 
                     data-height="full" 
                     data-whitespace="nowrap" 
                     data-transform_idle="o:1;" 
                     data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                     data-transform_out="opacity:0;s:500;s:500;" 
                     data-start="0" 
                     data-basealign="slide"
                     data-responsive_offset="off" 
                     data-responsive="off" 
                     style="z-index: 7;border-color:rgba(0, 0, 0, 0);">
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme" 
                     data-x="center" 
                     data-hoffset="" 
                     data-y="middle" 
                     data-voffset="" 
                     data-fontsize="['100','90','70','60']" 
                     data-lineheight="['100','90','70','60']"
                     data-whitespace="nowrap"
                     data-responsive_offset="on"
                     data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                     style="z-index: 99; color: #fff; font-weight: 900;">RESIS
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme" 
                     data-x="['center','center','center','center']" 
                     data-hoffset="['-98','-270','-200','-160']" 
                     data-y="middle" 
                     data-voffset="['-12','-15','-24','-28']"
                     data-fontsize="['11','10','7','6']" 
                     data-lineheight="['11','10','7','6']"
                     data-whitespace="nowrap"
                     data-width="100"
                     data-height="100"
                     data-responsive_offset="on" 
                     data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"x:[40%];z:0;rX:0deg;rY:0;rZ:-90;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;rZ:-90","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                     style="z-index: 11; color: #fff; font-weight: 900; ">WELCOME TO
                </div>
                <!-- LAYER NR. 4 -->
                <a class="tp-caption button btn_yellow" 
                  href="<?= site_url('property') ?>"
                  data-x="center" 
                  data-hoffset=""
                  data-y="middle"
                  data-voffset="75"
                  data-fontsize="14"
                  data-responsive_offset="on" 
                  data-whitespace="nowrap"
                  data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                  style="z-index: 11; padding-top: : 1rem; padding-bottom: 1rem; padding-right: 5rem; padding-left: 5rem;">
                  <i class="fa fa-search"></i>CARI PERUMAHAN
                </a>  

            </li>
        </ul>
    </div>
</div>
<!-- ========== BOOKING FORM ========== -->
<!-- <div class="hbf_3">
    <div class="container">
        <div class="inner">
            <form id="booking-form">
                <div class="row">
                    <div class="col-md-10 md_pr5">
                        <div class="form-group">
                            <input class="form-control" name="cari" type="text"  placeholder="Cari properti anda disini!">
                        </div>
                    </div>
                    <div class="col-md-2 md_pl5">
                        <button type="submit" class="button btn_blue btn_full"><i class="fa fa-search"></i> CARI</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> -->
<!-- ========== GALLERY ========== -->
<section id="gallery_style_2" class="white_bg">
    <div class="container">
        <div class="main_title mt_wave mt_yellow a_left">
            <h2 class="">PROMO TERKINI</h2>
            <a class="button btn_sm btn_yellow f_right" href="<?= site_url('promo') ?>">PROMO LAINNYA</a>
        </div>
        <div class="row">
          <?php //foreach ($getDataPromo as $gdp) : ?>
            <!-- ITEM -->
            <div class="col-md-3">
                <article class="room">
                    <figure>
                        <!-- <div class="price"><?php //$this->lee->getRupiah($gdp->harga); ?></div> -->
                        <a class="hover_effect h_yellow h_link" href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>">
                            <img src="<?= site_url('assets/uploads/photo_promo/gratis_promo.png') ?>" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h5><a href="room.html">GRATIS KONSULTASI DENGAN PROFESIONAL</a></h5>
                        <br>
                        <br>
                        <span style="margin-top:20px"><a href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                        </figcaption>
                    </figure>
                </article>
            </div>
            <div class="col-md-3">
                <article class="room">
                    <figure>
                        <!-- <div class="price"><?php //$this->lee->getRupiah($gdp->harga); ?></div> -->
                        <a class="hover_effect h_yellow h_link" href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>">
                            <img src="<?= site_url('assets/uploads/photo_promo/promo_rumah_pertama.png') ?>" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h5><a href="room.html">PROMO PEMBELIAN RUMAH PERTAMA!</a></h5>
                            <br>
                        <br>
                        <span style="margin-top:20px"><a href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                        </figcaption>
                    </figure>
                </article>
            </div>
            <div class="col-md-3">
                <article class="room">
                    <figure>
                        <!-- <div class="price"><?php //$this->lee->getRupiah($gdp->harga); ?></div> -->
                        <a class="hover_effect h_yellow h_link" href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>">
                            <img src="<?= site_url('assets/uploads/photo_promo/gratis_interior.png') ?>" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h5><a href="room.html">GRATIS INTERIOR</a></h5>
                        <br><br><br>
                        <span style="margin-top:20px"><a href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                        </figcaption>
                    </figure>
                </article>
            </div>
            <div class="col-md-3">
                <article class="room">
                    <figure>
                        <!-- <div class="price"><?php //$this->lee->getRupiah($gdp->harga); ?></div> -->
                        <a class="hover_effect h_yellow h_link" href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>">
                            <img src="<?= site_url('assets/uploads/photo_promo/gratis_biaya_bphtb.png') ?>" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h5><a href="room.html">BELI RUMAH GRATIS BIAYA BPHTB</a></h5>
                            <br>
                        <br>
                        <span style="margin-top:20px"><a href="<?php //echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                        </figcaption>
                    </figure>
                </article>
            </div>
          <?php //endforeach; ?>
        </div>
    </div>
</section>

<!-- ========== Iklan ========== -->
<section id="white_bg" class="">
    <div class="slack-carousel">
      <div class="item">
        <img src="<?= site_url('assets/uploads/photo_iklan/keuntungan_rumah.png') ?>" alt="Chicago" style="height: 100%" width="100%" class="img-responsive">
      </div>
      <div class="item">
        <img src="<?= site_url('assets/uploads/photo_iklan/invest_rumah.png') ?>" alt="Chicago" style="height: 100%" width="100%" class="img-responsive">
      </div>
    </div>
</section>

<!-- ========== ROOMS ========== -->
<section class="white_bg" id="rooms_grid">
    <div class="container">
        <div class="main_title mt_wave mt_yellow a_left">
            <h2>DAFTAR PROPERTY</h2>
            <a class="button btn_sm btn_yellow f_right" href="<?= site_url('property') ?>">PROPERTY LAINNYA</a>
        </div>
        <div class="row">
            <!-- ITEM -->
          <?php foreach ($getDataProduk as $gdp) : ?>
            <div class="col-md-3">
              <div class="room_grid_item_2" style="
                background: #f5f5f5;
                margin-bottom: 50px;
                border: 1px solid #eeeeee;
                border-radius: 1px;">
               <article class="room">
                 <figure>
                  <div class="price">
                      <?php if ($gdp->tipe_bangunan == 'rumah') {
                      ?>
                          <i class="fa fa-home"></i> Rumah
                      <?php
                      } else if($gdp->tipe_bangunan == 'vila'){
                      ?>
                          <i class="fa fa-building"></i> Villa
                      <?php
                      }else if($gdp->tipe_bangunan == 'apartemen'){
                      ?>
                          <i class="fa fa-building"></i> Apartement
                      <?php
                      }
                      ?>
                  </div>
                  <a class="hover_effect h_yellow h_link" href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>">
                  <img src="<?= site_url('assets/uploads/thumbnails_produk/medium/').$gdp->gambar_thumb ?>" class="img-responsive" alt="Image" style="width: 764px; height: 180px">
                  </a>
                  <div style="
                      position: absolute;
                      bottom: 0;
                      text-align: center;
                      z-index: 9;
                  " class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                      <button class="button btn_dark btn_sm btn-block " data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Luas Bangunan" data-content="Luas : <?= $gdp->luas_bangunan ?> m²" style="background-color: #444444; border-color: #444444; border: 0" >
                        LuasBangunan :
                        <?= $gdp->luas_bangunan ?> m²
                      </button>
                    </div>
                      
                    <div class="btn-group" role="group">
                      <button class="button btn_dark btn_sm btn-block" data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Luas Tanah" data-content="Luas : <?= $gdp->luas_tanah ?> m²" style="background-color: #444444; border-color: #444444; border: 0">
                        LuasTanah :
                        <?= $gdp->luas_tanah ?> m²   
                      </button>
                    </div>
                      
                  </div>
               </figure>
               </article>
               <div class="room_info" style=" padding: 10px;">
                  <h4 class="mt5" style="font-weight: 700">
                    <a href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>" class="text-info">
                      <?php
                      $sumchar = strlen($gdp->nama_produk );
                      if ($sumchar <= 20) {
                         echo $gdp->nama_produk;
                         echo "<br>";
                         echo "<br>";
                       }else{
                         echo substr($gdp->nama_produk, 0, 36).'...';
                       }
                      ?>  
                    </a>
                  </h4>
                  <h4 class="text-warning" style="font-family: sans-serif; margin-bottom: -0.5rem">
                    <?php
                      if ($this->session->userdata('is_login')) {
                         $qp = $this->mod_sb->mengambil('pengguna', ['username'=>$this->session->userdata('uname')])->row();
                         if ($qp->no_tlp != null && $qp->KTP != null && $qp->Penghasilan != null) {
                            echo $this->lee->getRupiah2($gdp->harga); 
                         } else {
                            echo $this->lee->getRupiahWithSensor($gdp->harga); 
                         }                             
                      }else{
                        echo $this->lee->getRupiahWithSensor($gdp->harga); 
                      }
                      
                    ?>
                    <!-- diskon -->
                    <!-- <label style="text-decoration: line-through;">Rp 1.500.000</label> -->
                  </h4>
                  <p>
                    <a href="<?= site_url('profile_pengembang/').md5($gdp->id_pengembang) ?>" style="color: #444">
                      <?php 
                        $sumchar = strlen($gdp->nama);
                        if ($sumchar <= 28) {
                        ?>
                          <?= $gdp->nama ?>
                        <?php
                        }else{
                        ?>
                          <?= substr($gdp->nama, 0, 24).'...' ?>
                        <?php
                        }
                      ?>
                    </a>
                    <br>
                    <?php 
                    $sumchar = strlen($gdp->nama_proyek);
                    if ($sumchar <= 28) {
                    ?>
                      <?= $gdp->nama_proyek ?>
                    <?php
                    }else{
                    ?>
                      <?= substr($gdp->nama_proyek, 0, 24).'...' ?>
                    <?php
                    }
                    ?>
                  </p>
                  <p>
                  <!-- <?= substr($this->typography->auto_typography($gdp->deskripsi), 0, 29).'...'; ?> -->
                  </p>
                  <div class="text-center">
                    <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Ruang tamu" data-content="Jumlah : <?= $gdp->ruang_tamu ?>">
                      <i class="fa fa-television"></i>
                      Ruang Tamu :
                      <?= $gdp->ruang_tamu ?>
                    </button>
                    <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Kamar Tidur" data-content="Jumlah : <?= $gdp->kamar_tidur ?>">
                      <i class="fa fa-bed"></i> 
                      Ruang Tidur :
                      <?= $gdp->kamar_tidur ?>
                    </button>
                    <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Kamar Mandi" data-content="Jumlah : <?= $gdp->kamar_mandi ?>">
                      <i class="fa fa-bathtub"></i> 
                      Ruang Mandi :
                      <?= $gdp->kamar_mandi ?>
                    </button>
                    <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Dapur" data-content="Jumlah : <?= $gdp->dapur ?>">
                      <i class="fa fa-cutlery"></i>
                      Ruang Dapur :
                      <?= $gdp->dapur ?>
                    </button>
                  </div>
                  <a href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button  btn_blue btn_full upper mt20">Lebih Detail</a>
               </div>
             </div>
            </div>
          <?php endforeach; ?>
      </main>
      
    </div>
</section>

<!-- ========== GALLERY ========== -->
<section id="gallery_style_2" class="white_bg">
    <div class="container">
        <div class="main_title mt_wave mt_yellow a_center">
            <h2>VITUAL TOUR</h2>
        </div>
        <div class="row">
            <div class="image-gallery">
              <?php foreach ($getVirtualTour as $gv) : ?>
                <!-- ITEM -->
                <div class="col-md-3 col-sm-4">
                    <figure class="gs2_item"  style="width: 270px; height: 171px">
                        <a href="<?= site_url('assets/uploads/thumbnails_produk/large/').$gdp->gambar_thumb ?>" class="hover_effect h_lightbox h_yellow" style="width: 270px; height: 171px">
                            <img src="<?= site_url('assets/uploads/thumbnails_produk/medium/').$gdp->gambar_thumb ?>" class="img-responsive" alt="Image"  style="width: 270px; height: 171px">
                        </a>
                    </figure>
                </div>
              <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<!-- ========== CONTACT ========== -->
<section class="white_bg" id="contact">
    <div class="container">
        <div class="main_title mt_wave mt_yellow a_center">
            <h2 class="c_title">HUBUNGI KAMI</h2>
        </div> 
        <div class="row">
            <div class="col-md-6">
                <div id="" style="border: 1px solid #cfd7da;border-radius: 2px;width: 100%;height: 380px;">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.4317729566887!2d109.25041001459591!3d-7.4173732946484465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655e933c7ebafb%3A0x405e82ed924c8e98!2sMarketing%20Gallery%20Banyumas%20Property%20Center!5e0!3m2!1sid!2sid!4v1623483212166!5m2!1sid!2sid" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="contact-items">
                        <div class="col-md-4 col-sm-4">
                            <div class="contact-item">
                                <i class="glyphicon glyphicon-user"></i>
                                <h6>Admin resis</h6>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="contact-item">
                                <i class="glyphicon glyphicon-phone-alt"></i>
                                <h6>+62 857-4799-9476</h6>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="contact-item">
                                <i class="fa fa-envelope"></i>
                                <h6>cs@resis.id</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <form id="contact-form" name="contact-form">
                    <div id="contact-result"></div>
                    <div class="form-group">
                        <input class="form-control" name="name" placeholder="Nama Anda" type="text">
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="email" type="email" placeholder="Alamat Email Anda">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Ketik Pesan Disini"></textarea>
                    </div>
                    <button class="button btn_lg btn_blue btn_full upper" type="submit" onclick="alert('Sorry this features under the maintenance! please chat admin at bottom right for customer services')"><i class="fa fa-location-arrow"></i>KIRIM PESAN</button>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
  $('.slack-carousel').slick({
    // dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 2000
  });
})
</script>