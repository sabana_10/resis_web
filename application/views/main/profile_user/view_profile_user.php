<!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay-2">
    <div class="container">
        <div class="inner">
            <h1>Profile</h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url('') ?>">Home</a></li>
                <li>Profile</li>
            </ol>
        </div>
    </div>
</div>

<!-- =========== MAIN ========== -->
<main id="our_staff_page">
    <div class="container">
        <div class="row">
            <!-- ITEM -->
            <div class="col-md-3 col-sm-6">
                <div class="item">
                    <img src="<?= site_url('assets/logo/resis_logo_transparent_1.png') ?>" class="img-responsive" alt="Image">
                    <h3 class="text-center text-warning" style="font-weight: 900">Selamat Datang <?= ($getDataUser->nama != null)? $getDataUser->nama : $getDataUser->username ?></h3>
                    <!-- <p>Purwanegara, Banyumas, Jawa Tengah</p> -->
                    <p class="text-danger"></p>
                   <!--  <div class="social-media">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </div> -->
                </div>
                <div class="item">
                    <button id="btnAkun" class="button btn-md btn-block">Akun</button>
                    <button id="btnAlamat" class="button btn-md btn-block">Alamat</button>
                    <button id="btnGantiPassword" class="button btn-md btn-block">Ganti Password</button>
                    <!-- <a href="" class="button btn-md btn-block <?= ($this->uri->segment(2) == 'keamanan')? 'btn_yellow' : '' ?>">Keamanan</a> -->
                </div>
                <div class="item">
                    <button id="btnLogout" class="btn btn-danger btn-block">Logout</button>
                </div>
            </div>
            <!-- ITEM -->
            <div class="col-md-9 col-sm-6">
                <div class="item" id="akun">
                    <form id="formAkun" enctype="multipart/form-data">
                        <input name="id" type="hidden" class="form-control" value="<?= $getDataUser->id ?>" style="color: #444444">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="item">
                                    <h4 class="text-warning text-center mb10" style="font-weight: 700;">Photo Profile</h4>
                                    <?php if ($getDataUser->foto != null) { ?>
                                    <a href="<?= site_url('assets/uploads/photo_pengguna/').$getDataUser->foto ?>" class="hover_effect h_yellow h_link mb10">
                                        <img src="<?= site_url('assets/uploads/photo_pengguna/').$getDataUser->foto ?>" class="img-responsive" alt="Image">
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?= site_url('assets/uploads/photo_pengguna/default.png') ?>" class="hover_effect h_yellow h_link mb10">
                                        <img src="<?= site_url('assets/uploads/photo_pengguna/default.png') ?>" class="img-responsive" alt="Image">
                                    </a>
                                    <?php } ?>
                                    <div class="form-group">
                                        <input type="file" class="form-control" name="foto">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group" style="margin-bottom: 1rem">
                                    <label for="">Email</label>
                                    <small style="float: right;"><span id="batasEmail">0</span> /100</small>
                                    <input name="email" type="email" class="form-control" value="<?= $getDataUser->username ?>" style="color: #444444" readonly>
                                </div>
                                <div class="form-group" style="margin-bottom: 1rem">
                                    <label for="">Nama Lengkap <small for="" class="text-danger">wajib</small></label>
                                    <small style="float: right;"><span id="batasNama">0</span> /50</small>
                                    <input name="nama" type="text" class="form-control" placeholder="isi nama lengkap disini" style="color: #444444" maxlength="50" value="<?= ($getDataUser->nama != null)? $getDataUser->nama : '' ?>" required>
                                </div>
                                <div class="form-group" style="margin-bottom: 1rem">
                                    <label for="">No Telpon/Whatsapp <small for="" class="text-danger">wajib</small></label>
                                    <small style="float: right;"><span id="batasTelp">0</span> /13</small>
                                    <input name="no_tlp" type="text" class="form-control" placeholder="isi nomor telpon / wa disini" style="color: #444444" maxlength="13" value="<?= ($getDataUser->no_tlp != null)? $getDataUser->no_tlp : '' ?>" required>
                                </div>
                                <div class="form-group" style="margin-bottom: 1rem">
                                    <label for="">No. KTP <small for="" class="text-danger">wajib</small></label>
                                    <small style="float: right;"><span id="batasKtp">0</span> /16</small>
                                    <input name="KTP" type="text" class="form-control"  placeholder="isi no ktp disini" style="color: #444444" maxlength="16" value="<?= ($getDataUser->KTP != null)? $getDataUser->KTP : '' ?>" required>
                                </div>
                                <div class="form-group" style="margin-bottom: 1rem">
                                    <label for="">Penghasilan Rp. <small for="" class="text-danger">wajib</small></label>
                                    <small style="float: right;"><span id="batasPenghasilan">0</span> /12</small>
                                    <input name="Penghasilan" type="text" class="form-control" placeholder="isi penghasilan disini" style="color: #444444" maxlength="12" value="<?= ($getDataUser->Penghasilan != null)? $getDataUser->Penghasilan : '' ?>" required>
                                </div><br>
                                <div>
                                    <button id="btnSimpanAkun" class="btn btn-info btn-block">
                                        <em class="fa fa-save"></em>
                                         Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="item" id="alamat">
                    <form  method="POST" id="formAlamat" enctype="multipart/form-data">
                        <input name="id" type="hidden" class="form-control" value="<?= $getDataUser->id ?>" style="color: #444444">
                        <input name="email" type="hidden" class="form-control" value="<?= $getDataUser->username ?>" style="color: #444444">
                        <div class="row" style="margin-bottom: 1rem">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label id="label-objek">
                                  Provinsi <small for="" class="text-danger">wajib</small>
                                </label>
                                <select name="provinsi" class="form-control" id="provinsi" style="color: #444444">
                                    <option value="<?= ($getDataUser->id_provinsi != null)? $getDataUser->id_provinsi : 'Pilih Provinsi' ?>"><?= ($getDataUser->provinsi != null)? $getDataUser->provinsi : 'Pilih Provinsi' ?></option>
                                    <?php
                                    foreach ($getProvinsi as $gp) {
                                    ?>
                                        <option value="<?= $gp->id ?>"><?= $gp->name ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label id="label-objek">
                                  Kabupaten / Kota <small for="" class="text-danger">wajib</small>
                                </label>
                                <input type="text" name="input_kabupaten" id="input_kabupaten" class="form-control" placeholder="Kabupaten/Kota" readonly="" style="color: #444444" value="<?= ($getDataUser->kabupaten != null)? $getDataUser->kabupaten : '' ?>">
                                <select name="kabupaten" class="form-control kabupaten" id="kabupaten" style="color: #444444"></select>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label id="label-objek">
                                  Kecamatan <small for="" class="text-danger">wajib</small>
                                </label>
                                <input type="text" name="input_kecamatan"  id="input_kecamatan" class="form-control" placeholder="Kecamatan" readonly="" style="color: #444444" value="<?= ($getDataUser->kecamatan != null)? $getDataUser->kecamatan : '' ?>">
                                <select name="kecamatan" class="form-control kecamatan" id="kecamatan" style="color: #444444"></select>
                              </div>
                            </div>
                          </div>
                        <div class="form-group" style="margin-bottom: 1rem">
                            <label for="">Alamat lengkap <small for="" class="text-danger">wajib</small></label>
                            <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control" style="color: #444444" required><?= ($getDataUser->alamat != null)? $getDataUser->alamat : '' ?></textarea>
                        </div><br>
                        <div>
                            <button id="btnSimpanAlamat" type="submit" class="btn btn-info btn-block">
                                <em class="fa fa-save"></em>
                                 Simpan
                            </button>
                        </div>
                    </form>
                </div>

                <div class="item" id="gantiPassword">
                    <form  method="POST" id="formPassword" enctype="multipart/form-data">
                        <input name="id" type="hidden" class="form-control" value="<?= $getDataUser->id ?>" style="color: #444444">
                        <input name="email" type="hidden" class="form-control" value="<?= $getDataUser->username ?>" style="color: #444444">
                        <div class="form-group" style="margin-bottom: 1rem">
                            <label for="">Password Baru <small for="" class="text-danger">wajib</small></label>
                            <input type="password" name="passwordBaru" class="form-control" id="passwordBaru">
                        </div>
                        <div class="form-group" style="margin-bottom: 1rem">
                            <label for="">Ulangi Password Baru <small for="" class="text-danger">wajib</small></label>
                            <input type="password" name="passwordBaru" class="form-control" id="passwordBaru">
                        </div>
                        <hr>
                        <div class="form-group" style="margin-bottom: 1rem">
                            <label for="">Konfirmasi Password Lama <small for="" class="text-danger">wajib</small></label>
                            <input type="password" name="passwordBaru" class="form-control" id="passwordBaru">
                        </div>
                        <br>
                        <div>
                            <button id="btnSimpanAlamat" type="submit" class="btn btn-info btn-block">
                                <em class="fa fa-save"></em>
                                 Simpan
                            </button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
        
    </div>
</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
$(document).ready(function(){
    $('#btnAkun').addClass('btn_yellow');
    $('#alamat').hide();
    $('#gantiPassword').hide();
    $('#kabupaten').hide();
    $('#kecamatan').hide();
    $('[name="Penghasilan"]').val(konversiRupiah($('[name="Penghasilan"]').val()));
    // set jumlah huruf maksimal
    $('#batasEmail').text($('[name="email"]').val().length);
    if ($('[name="nama"]').val()) { $('#batasNama').text($('[name="nama"]').val().length); }
    if ($('[name="no_tlp"]').val()) { $('#batasTelp').text($('[name="no_tlp"]').val().length); }
    if ($('[name="KTP"]').val()) { $('#batasKtp').text($('[name="KTP"]').val().length); }
    if ($('[name="Penghasilan"]').val()) { $('#batasPenghasilan').text($('[name="Penghasilan"]').val().length); }
    // batas text
    $('[name="nama"]').on('input', function () { $('#batasNama').text($('[name="nama"]').val().length); });
    $('[name="no_tlp"]').on('input', function () { 
        $('#batasTelp').text($('[name="no_tlp"]').val().length); 
    });
    $('[name="KTP"]').on('input', function () { $('#batasKtp').text($('[name="KTP"]').val().length); });
    $('[name="Penghasilan"]').on('input', function () { $('#batasPenghasilan').text($('[name="Penghasilan"]').val().length); });
    // on clik alamat
    $('#btnAlamat').on('click', function() {
        $('#alamat').show();
        $('#akun').hide();
        $('#gantiPassword').hide();
        $('#btnAlamat').addClass('btn_yellow');
        $('#btnAkun').removeClass('btn_yellow');
        $('#btnGantiPassword').removeClass('btn_yellow');
    });
    // on click akun
    $('#btnAkun').on('click', function() {
        $('#akun').show();
        $('#alamat').hide();
        $('#gantiPassword').hide();
        $('#btnAkun').addClass('btn_yellow');
        $('#btnAlamat').removeClass('btn_yellow');
        $('#btnGantiPassword').removeClass('btn_yellow');
    });
    $('#btnGantiPassword').on('click', function() {
        $('#gantiPassword').show();
        $('#alamat').hide();
        $('#akun').hide();
        $('#btnGantiPassword').addClass('btn_yellow');
        $('#btnAlamat').removeClass('btn_yellow');
        $('#btnAkun').removeClass('btn_yellow');
    });
    $('[name="Penghasilan"]').on('keyup', function(e) {
        $('[name="Penghasilan"]').val(konversiRupiah($('[name="Penghasilan"]').val()));
    });
    function konversiRupiah(angka) {
        var ns = angka.replace(/[^,\d]/g, '').toString(),
            split = ns.split(','),
            sisa  = split[0].length % 3,
            rupiah  = split[0].substr(0, sisa),
            ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return rupiah;
    }
    //fun get kabupaten
    $('#provinsi').on('input', function getKabupaten() {
        $('#kabupaten').empty();
        var e = document.getElementById("provinsi");
        var id = e.value;
        url = '<?php echo site_url("Konsumen/ProfileUser/getKabupaten/'+id+'") ?>';
        $.ajax({
          url : url,
          method : 'POST',
          data : {},
          contentType : false,
          cache : false,
          processData : false,
          dataType : 'JSON',
          success : function (data) {
            var result = Object.assign({}, data.data);
            for (var i = 0; i < data.data.length; i++) {
                $('#input_kabupaten').hide();
                $('#kabupaten').show();
                $('#kabupaten').append('<option value="'+data.data[i].id+'">'+data.data[i].name+'</option>');
                $('#kabupaten').change();
            }
          }, error : err=>{}
        });
    });
    // fun get kecamatan
    $('#kabupaten').on('input', function() {
        $('#kecamatan').empty();
        var e = document.getElementById("kabupaten");
        var id = e.value;
        url = '<?php echo site_url("Konsumen/ProfileUser/getKecamatan/'+id+'") ?>';
        $.ajax({
          url : url,
          method : 'POST',
          data : {},
          contentType : false,
          cache : false,
          processData : false,
          dataType : 'JSON',
          success : function (data) {
            var result = Object.assign({}, data.data);
            for (var i = 0; i < data.data.length; i++) {
                $('#input_kecamatan').hide();
                $('#kecamatan').show();
                $('#kecamatan').append('<option value="'+data.data[i].id+'">'+data.data[i].name+'</option>');
                $('#kecamatan').change();
            }
          }, error : err=>{}
        });
    })
    $('#formAkun').on('submit', function(e) {
        e.preventDefault();
        $('#btnSimpanAkun').text('Menyimpan...');
        $('#btnSimpanAkun').attr('disabled', true);
        url = '<?php echo site_url('Konsumen/ProfileUser/saveAkun') ?>';
        $.ajax({
          url : url,
          method : 'POST',
          data : new FormData(this),
          contentType : false,
          cache : false,
          processData : false,
          dataType : 'JSON',
          success : function (data) {
            if (data.status == true) {
                successNotif(data.message, 'success', 2000, 'data');
            }else{
                successNotif(data.errorintruth, data.status, 'error', 5000);
            }
          },
          error : err=>{
            failNotif('saveAkun', 'Gagal menambah data!');
          }
        });
    });
    $('#formAlamat').on('submit', function(e) {
        e.preventDefault();
        $('#btnSimpanAlamat').text('Menyimpan...');
        $('#btnSimpanAlamat').attr('disabled', true);
        url = '<?php echo site_url('Konsumen/ProfileUser/saveAlamat') ?>';
        $.ajax({
          url : url,
          method : 'POST',
          data : new FormData(this),
          contentType : false,
          cache : false,
          processData : false,
          dataType : 'JSON',
          success : function (data) {
            successNotif(data.message, 'success', 2000, 'data');
          },
          error : err=>{
            failNotif('saveAlamat', 'Gagal menambah data!');
          }
        });
    });
    function successNotif(data, icon, time, id = null) {
      Swal.fire({
        position: 'center',
        icon: icon,
        title: data,
        showConfirmButton: false,
        timer: time,
      }).then(function() {
          document.location.href="<?php echo site_url('profile') ?>";
      });
    }
    function failNotif(method = null, titles) {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: titles,
        showConfirmButton: false,
        timer: 5000,
      }).then(function() {
        if (method == 'saveAkun') {
          $('#btnSimpanAkun').text('Simpan');
          $('#btnSimpanAkun').attr('disabled', false);
        }else{
          $('#btnSimpanAlamat').text('Simpan');
          $('#btnSimpanAlamat').attr('disabled', false);
        }
      });
    }
    
});
</script>