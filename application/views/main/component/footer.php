<footer>
    <div class="inner gold-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 widget">
                    <div class="about">
                        <div class="col-md-12 text-center">
                            <a href="<?= site_url('') ?>"><img class="logo" src="<?= site_url('assets/logo/') ?>resis_logo_transparent.png" height="180px" alt="Logo"></a>
                        </div>
                        <div class="col-md-12 text-center">Power by : </div>
                        <div class="col-md-6 text-center">
                            <a href="<?= site_url('') ?>"><img class="logo" src="<?= site_url('assets/logo/') ?>rei.png" height="80px" alt="Logo"></a>
                        </div>
                        <div class="col-md-6 text-center">
                            <a href="<?= site_url('') ?>"><img class="logo" src="<?= site_url('assets/logo/') ?>satria_tech.png" height="80px" alt="Logo"></a>
                        </div>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p> -->
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 widget">
                    <h5>Berita Terbaru</h5>
                    <ul class="blog_posts">
                        <li><a href="blog-post.html">10 Cara memilih property yang pas untuk Anda</a> <small>28/06/2021</small></li>
                        <li><a href="blog-post.html">Tip & Trik memilih rumah impian</a> <small>06/06/2021</small></li>
                        <li><a href="blog-post.html">Cek rumah tanpa ribet? virtual tour aja di Resis.id</a> <small>06/06/2021</small></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 widget">
                    <h5>Link Kami</h5>
                    <ul class="useful_links">
                        <li><a href="<?= site_url('about_us') ?>">Tentang Kami</a></li>
                        <li><a href="<?= site_url('kontak') ?>">Kontak kami</a></li>
                        <li><a href="<?= site_url('blog') ?>">Blog</a></li>
                        <li><a href="<?= site_url('galeri') ?>">Galeri</a></li>
                        <li><a href="<?= site_url('kontak') ?>">Lokasi</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 widget">
                    <h5>Kontak Kami</h5>
                    <address>
						<ul class="address_details">
							<li><i class="glyphicon glyphicon-map-marker"></i> Jl. Prof. Dr. Suharso, Arcawinangun, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah 53113</li>
							<li><i class="glyphicon glyphicon-phone-alt"></i> +62 857-4799-9476 </li>
							<!-- <li><i class="fa fa-fax"></i> Fax: 800 123 3456</li> -->
							<li><i class="fa fa-envelope"></i> <a href="mailto:cs@resis.id">cs@resis.id</a></li>
						</ul>
					</address>
                </div>
            </div>
        </div>
    </div>
    <div class="subfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="copyrights">
                         Copyright <?= date('Y') ?> <a href="<?= site_url('') ?>">Resis.id</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="social_media">
                        <a class="facebook" data-original-title="Facebook" data-toggle="tooltip" href="https://www.facebook.com/resis.indonesia"><i class="fa fa-facebook"></i></a>
                        <a class="twitter" data-original-title="Twitter" data-toggle="tooltip" href="https://twitter.com/Resisid"><i class="fa fa-twitter"></i></a>
                        <a class="linkedin" data-original-title="Linkedin" data-toggle="tooltip" href="https://www.linkedin.com/company/resisid/about/?viewAsMember=true"><i class="fa fa-linkedin"></i></a>
                        <a class="instagram" data-original-title="Instagram" data-toggle="tooltip" href="https://www.instagram.com/resis_id/"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>