<header id="header" class="fixed transparent mt-0">
    <div class="container">
        <div class="navbar-header" style="height: 80px;">
            <button type="button" class="navbar-toggle mobile_menu_btn" data-toggle="collapse" data-target=".mobile_menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand light" href="<?= site_url() ?>">
                <img src="<?= site_url('assets/logo/') ?>resis_logo_transparent.png" height="62" alt="Logo" style="margin-top: -10px">
            </a>
            <a class="navbar-brand dark nodisplay" href="<?= site_url() ?>">
                <img src="<?= site_url('assets/logo/') ?>resis_logo_transparent.png" height="62" alt="Logo" style="margin-top: -10px">
            </a>
        </div>
        <nav id="main_menu" class="mobile_menu navbar-collapse" style="margin-top: 10px">
            <ul class="nav navbar-nav">
                <li class="mobile_menu_title" style="display:none;">MENU</li>
                <li class="<?= ($this->uri->segment(1) == 'property')? 'active' : '' ?>">
                    <a href="<?= site_url('property') ?>">Property</a>
                </li>
                <li class="dropdown mega_menu mega_menu_fullwidth"><a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true">Kategori <b class="caret"></b></a>
                    <ul class="dropdown-menu"> 
                        <li>
                            <div class="mega_menu_inner">
                                <div class="row">
                                    <ul class="col-md-6">
                                        <li class="list_title">Property</li>
                                        <li><a href="<?= site_url('property') ?>">Rumah</a></li>
                                        <li><a href="<?= site_url('property') ?>">Apartement</a></li>
                                        <li><a href="<?= site_url('property') ?>">Villa</a></li>
                                    </ul>
                                    <ul class="col-md-6">
                                        <li class="list_title">Kota</li>
                                        <li><a href="<?= site_url('property') ?>">Banyumas</a></li>
                                        <li><a href="<?= site_url('property') ?>">Cilacap</a></li>
                                        <li><a href="<?= site_url('property') ?>">Purbalingga</a></li>
                                        <li><a href="<?= site_url('property') ?>">Banjarnegara</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="<?= ($this->uri->segment(1) == 'promo')? 'active' : '' ?>">
                    <!-- <a href="<?= site_url('promo') ?>">Promo</a> -->
                    <a href="#" onclick="alert('Sorry this features under the maintenance! please chat admin at bottom right for customer services')">Promo</a>
                </li>
                <!-- <li class="simple_menu <?= ($this->uri->segment(1) == 'galeri')? 'active' : '' ?>">
                    <a href="<?= site_url('galeri') ?>">Galeri</a>
                </li> -->
                <li class="simple_menu <?= ($this->uri->segment(1) == 'cari_kpr')? 'active' : '' ?>">
                    <a href="<?= site_url('cari_kpr') ?>">KPR</a>
                </li>
                <li class="simple_menu <?= ($this->uri->segment(1) == 'berita')? 'active' : '' ?>">
                    <a href="<?= site_url('berita') ?>">Berita Property</a>
                </li>
<!--                 <li class="dropdown simple_menu 
                <?php 
                    if($this->uri->segment(1) == 'kontak'){
                        echo 'active';
                    }else if($this->uri->segment(1) == 'about_us'){
                        echo 'active';
                    }else if($this->uri->segment(1) == 'blog'){
                        echo 'active';
                    }else if($this->uri->segment(1) == 'panduan_referensi'){
                        echo 'active';
                    }else if($this->uri->segment(1) == 'panduan_produk'){
                        echo 'active';
                    }else{
                        echo '';
                    }
                ?>
                ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">LAINNYA <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= site_url('panduan_referensi') ?>">Panduan dan Referensi</a></li>
                        <li><a href="<?= site_url('panduan_produk') ?>">Panduan Produk</a></li> -->
                 <!--        <li><a href="<?= site_url('blog') ?>">Blog</a></li>
                        <li><a href="<?= site_url('about_us') ?>">Tentang Kami</a></li>
                        <li><a href="<?= site_url('kontak') ?>">Kontak Kami</a></li>
                    </ul>
                </li> -->
                <?php
                    if ($this->session->userdata('is_login')) {
                ?>
                    <li class="menu_button" style="margin-right: 10px">
                        <a href="<?= site_url('profile') ?>" class="button btn_dark">Profile</a>
                        <!-- <button id="btnLogout" class="button btn_dark"><i class="fa fa-user"></i>Profile</button> -->
                    </li>
                <?php
                    } else {
                ?>
                    <li class="menu_button" style="margin-right: 10px">
                        <!-- <button class="button btn_yellow" data-toggle="modal" data-target="#loginModal" onclick="loginshow()"><i class="fa fa-user"></i>LOGIN</button> -->
                        <button class="button btn_yellow" onclick="loginshow()"><i class="fa fa-user"></i>LOGIN</button>
                        <!-- <a href="<?= site_url('login') ?>" class="button  btn_yellow"><i class="fa fa-user"></i>LOGIN</a> -->
                    </li>
                <?php
                    }
                ?>
                    
            </ul>
        </nav>
    </div>
</header>