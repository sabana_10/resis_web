 <!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay" style="background: url<?= site_url('assets/main/') ?>(images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <h1>Proyek</h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url('main') ?>">Home</a></li>
                <li >Proyek</li>
            </ol>
        </div>
    </div>
</div>
<!-- ========== GALLERY ========== -->
<section id="gallery_style_2" class="white_bg">
    <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="sidebar">
              <aside class="widget">
                  <div class="search">
                      <form method="get" class="widget_search">
                          <input type="search" placeholder="Start Searching...">
                          <button class="search_btn" id="searchsubmit" type="submit">
                              <i class="fa fa-search"></i>
                          </button>
                      </form>
                  </div>
              </aside>
              <aside class="widget">
                  <h4>CATEGORIES</h4>
                  <ul class="categories">
                      <li><a href="#">Rumah <span class="num_posts">51</span></a></li>
                      <li><a href="#">Apartement <span class="num_posts">51</span></a></li>
                      <li><a href="#">Villa <span class="num_posts">51</span></a></li>
                  </ul>
              </aside>
              <aside class="widget">
                  <h4>Latest Posts</h4>
                  <div class="latest_posts">
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb1.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Live your myth in Greece</a></h6>
                              <span><i class="fa fa-calendar"></i>23/11/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Hotel Zante in pictures</a></h6>
                              <span><i class="fa fa-calendar"></i>18/10/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb3.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Hotel Zante family party</a></h6>
                              <span><i class="fa fa-calendar"></i>13/08/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb4.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Hotel Zante weddings</a></h6>
                              <span><i class="fa fa-calendar"></i>13/08/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb5.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">10 things you should know</a></h6>
                              <span><i class="fa fa-calendar"></i>13/08/2017</span>
                          </div>
                      </article>
                  </div>
              </aside>
              <!-- <aside class="widget">
                  <h4>Tags</h4>
                  <div class="tagcloud clearfix">
                      <a href="#"><span class="tag">Hotel Zante</span><span class="num">12</span></a>
                      <a href="#"><span class="tag">HOLIDAYS</span><span class="num">24</span></a>
                      <a href="#"><span class="tag">PARTY</span><span class="num">8</span></a>
                      <a href="#"><span class="tag">GREECE</span><span class="num">4</span></a>
                      <a href="#"><span class="tag">PARTY</span><span class="num">56</span></a>

                      <a href="#"><span class="tag">ZAKYNTHOS</span><span class="num">12</span></a>
                  </div>
              </aside>
              <aside class="widget">
                  <h4>ARCHIVE</h4>
                  <ul class="archive">
                      <li><a href="#">May 2017<span class="num_posts">21</span></a></li>
                      <li><a href="#">June 2017<span class="num_posts">24</span></a></li>
                      <li><a href="#">July 2017<span class="num_posts">38</span></a></li>
                      <li><a href="#">August 2017<span class="num_posts">11</span></a></li>
                  </ul>
              </aside> -->
            </div>
          </div>
          <div class="col-md-9">
            <div class="main_title mt_wave mt_yellow a_left">
              <h2 class="">Promo Terkini</h2>
            </div>
            <div class="row">
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.35.000.000,00 </div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
            </div>
            <!-- ========== Proyek terbaru ========== -->
            <section id="gallery_style_2" class="">
                <!-- <div class="banner">
                  <img src="<?= site_url('assets/main/') ?>images/slider/slider-1.jpg" alt="Image" class="img-responsive">
                </div> -->
                <div id="myCarousel" class="carousel slide banner" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner banner">
                    <div class="item active">
                      <img src="<?= site_url('assets/main/') ?>images/slider/slider-1.jpg" alt="Los Angeles">
                    </div>
                    <div class="item">
                      <img src="<?= site_url('assets/main/') ?>images/slider/slider-1.jpg" alt="Chicago">
                    </div>
                    <div class="item">
                      <img src="<?= site_url('assets/main/') ?>images/slider/slider-1.jpg" alt="New York">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </section>
            <!-- terbaru -->
            <div class="main_title mt_wave mt_yellow a_left">
              <h2 class="">Proyek Terbaru</h2>
            </div>
            <div class="row">
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.35.000.000,00 </div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
            </div>
            <!-- all proyek -->
            <div class="main_title mt_wave mt_yellow a_left">
              <h2 class="">Semua Proyek</h2>
            </div>
            <div class="row">
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.35.000.000,00 </div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-3">
                    <article class="room">
                        <figure>
                            <div class="price">Rp.2jt</div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h5><a href="room.html">Perum Purwokerto</a></h5>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
                            <br>
                            <span style="margin-top:20px"><a href="rooms-list.html" class="button btn_sm btn-block btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
            </div>
            
          </div>
        </div>
    </div>
</section>



<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
</script>