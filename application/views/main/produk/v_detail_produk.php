<!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay-2" style="background: url(<?= site_url('assets/main/') ?>images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1>Detail Property</h1>
                    <ol class="breadcrumb">
                        <li><a href="<?= site_url() ?>">Home</a></li>
                        <li><a href="<?= site_url('property') ?>">Property</a></li>
                        <li>Detail Property</li>
                    </ol>
                </div>
                <div class="col-md-3 col-sm-3"></div>
                <div class="col-md-3 col-sm-3">
                    <button class="button btn_lg btn_yellow btn_full" type="button" style="float: right;margin-top: 1.5rem">
                      <h3 style="color: white; font-family: sans-serif;">
                        <?php
                          if ($this->session->userdata('is_login')) {
                            $qp = $this->mod_sb->mengambil('pengguna', ['username'=>$this->session->userdata('uname')])->row();
                             if ($qp->no_tlp != null && $qp->KTP != null && $qp->Penghasilan != null) {
                                echo $this->lee->getRupiah2($gdp->harga); 
                             } else {
                                echo $this->lee->getRupiahWithSensor($gdp->harga); 
                             }                             
                          }else{
                            echo $this->lee->getRupiahWithSensor($gdp->harga); 
                          }
                          
                        ?>
                      </h3>
                    </button>
                    <!-- <div class="price-2"> -->
                        <!-- <div class="inner-2"> -->
                            <?php //echo $this->lee->getRupiah($gdp->harga); ?> 
                            <!-- <br> -->
                            <!-- <span>per nett</span> -->
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========== MAIN ========== -->
<main id="room_page">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="slider">
                    <div id="slider-larg" class="owl-carousel image-gallery">
                        <?php foreach ($getPhotoProduk as $gpp) : ?>
                        <!-- ITEM -->
                        <div class="item lightbox-image-icon">
                            <a href="<?= site_url('assets/uploads/photo_produk/').$gpp->gambar ?>" class="hover_effect h_lightbox h_yellow">
                                <img class="img-responsive" src="<?= site_url('assets/uploads/thumbnails_produk/large/').$gpp->gambar ?>" alt="Image" style="width: 100%; height: 500px">
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div id="thumbs" class="owl-carousel">
                        <?php foreach ($getPhotoProduk as $gpp) : ?>
                        <!-- ITEM -->
                        <div class="item"><img class="img-responsive" src="<?= site_url('assets/uploads/thumbnails_produk/small/').$gpp->gambar ?>" alt="Image" style="width: 170; height: 95px"></div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="main_title mt50">
                            <h2><?= $gdp->nama ?></h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="main_title mt50">
                            <h2 style="font-family: sans-serif;">
                              <?php
                                if ($this->session->userdata('is_login')) {
                                  $qp = $this->mod_sb->mengambil('pengguna', ['username'=>$this->session->userdata('uname')])->row();
                                   if ($qp->no_tlp != null && $qp->KTP != null && $qp->Penghasilan != null) {
                                      echo $this->lee->getRupiah2($gdp->harga); 
                                   } else {
                                      echo $this->lee->getRupiahWithSensor($gdp->harga); 
                                   }                             
                                }else{
                                  echo $this->lee->getRupiahWithSensor($gdp->harga); 
                                }
                                
                              ?>
                            </h2>
                        </div>
                    </div>
                </div>
                <!-- type property -->
                <button class="button btn_dark" data-toggle="tooltip" data-placement="bottom" title="Tipe Bangunan">
                    <?php if ($gdp->tipe_bangunan == 'rumah') {
                    ?>
                        <i class="fa fa-home"></i>Rumah
                    <?php
                    } else if($gdp->tipe_bangunan == 'vila'){
                    ?>
                        <i class="fa fa-building"></i> Villa
                    <?php
                    }else if($gdp->tipe_bangunan == 'apartemen'){
                    ?>
                        <i class="fa fa-building"></i> Apartement
                    <?php
                    }
                    ?>
                </button> 
                <button class="button btn_dark" data-toggle="tooltip" data-placement="bottom" title="Luas Tanah">Luas Tanah : <?= $gdp->luas_tanah ?> m²</button>
                <button class="button btn_dark"  data-toggle="tooltip" data-placement="bottom" title="Luas Bangunan">Luas Bangunan : <?= $gdp->luas_bangunan ?> m²</button>
                <br><br>
                Proyek : <h3 style="font-weight: 700" class="text-primary"><?= $gdp->nama_proyek ?></h3>
                <br>
                Pengembang : <h3 style="font-weight: 700"><a href="<?= site_url('profile_pengembang/').md5($gdp->id_pengembang) ?>" class="text-primary"><?= $gdp->nama_pengembang ?></a></h3>
                <hr>

                <!-- Deskripsi Property -->
                <h3 style="font-weight: 700; color: #444444"><i class="fa fa-newspaper-o"></i> Deskripsi</h3>
                <p><?= $this->typography->auto_typography($gdp->deskripsi); ?></p>
                <hr>
                <!-- fasilitas property -->
                <h3 style="font-weight: 700; color: #444444" class="c_title"><i class="fa fa-cube"></i> Fasilitas Property</h3>
                <br>
                <button class="button btn_dark"  data-toggle="tooltip" data-placement="bottom" title="Ruang Tamu"><i class="fa fa-television"></i>Ruang Tamu : <?= $gdp->ruang_tamu ?></button>
                <button class="button btn_dark"  data-toggle="tooltip" data-placement="bottom" title="Kamar Tidur"><i class="fa fa-bed"></i>Kamar Tidur : <?= $gdp->kamar_tidur ?></button>
                <button class="button btn_dark"  data-toggle="tooltip" data-placement="bottom" title="Kamar Mandi"><i class="fa fa-bathtub"></i>Kamar Mandi : <?= $gdp->kamar_mandi ?></button>
                <button class="button btn_dark"  data-toggle="tooltip" data-placement="bottom" title="Dapur"><i class="fa fa-cutlery"></i>Ruang Dapur : <?= $gdp->dapur ?></button>
                <br><br><hr>

                <!-- fasilitas umum -->
                <h3 style="font-weight: 700; color: #444444" class="c_title"><i class="fa fa-rss-square"></i> Fasilitas Umum</h3>
                <p><?= $this->typography->auto_typography($gdp->fasilitas_umum); ?></p>
                <hr>
                <!-- lokasi -->
                <h3 style="font-weight: 700; color: #444444"><i class="fa fa-map"></i> Lokasi</h3>
                <br>
                <button class="button "  data-toggle="tooltip" data-placement="bottom" title="Provinsi">Provinsi : <?= ($gdp->provinsi != null)? $gdp->provinsi : '-' ?></button>
                <button class="button "  data-toggle="tooltip" data-placement="bottom" title="Kabupaten">Kabupaten : <?= ($gdp->kabupaten != null)? $gdp->kabupaten : '-' ?></button>
                <button class="button "  data-toggle="tooltip" data-placement="bottom" title="Kecamatan">Kecamatan : <?= ($gdp->kecamatan != null)? $gdp->kecamatan : '-' ?></button>
                <p class="mt20">
                    <div id="peta" style="width: 100%; height: 300px"></div>
                </p><hr> <br>

                <!-- Pesan sekarang -->
                <!-- <div class="mt50">
                    <h2>
                        <button class="button btn_yellow btn-block bb"> <i class="fa fa-calendar"></i> PESAN SEKARANG! </button>
                    </h2>
                </div> -->

                <!-- properti lainnya -->
                <div id="rooms_grid">
                    <div class="main_title mt_wave mt_yellow a_left">
                        <h2 class="">Property lainnya</h2>
                        <a class="button btn_sm btn_yellow f_right" href="<?= site_url('property') ?>">Lebih Banyak</a>
                    </div>
                    <div class="row">
                        <?php foreach ($getDataProduk as $gdp) : ?>
                        <div class="col-md-4">
                            <div class="room_grid_item_2" style="
                              background: #f5f5f5;
                              margin-bottom: 50px;
                              border: 1px solid #eeeeee;
                              border-radius: 1px;">
                             <article class="room">
                               <figure>
                                <div class="price">
                                    <?php if ($gdp->tipe_bangunan == 'rumah') {
                                    ?>
                                        <i class="fa fa-home"></i> Rumah
                                    <?php
                                    } else if($gdp->tipe_bangunan == 'vila'){
                                    ?>
                                        <i class="fa fa-building"></i> Villa
                                    <?php
                                    }else if($gdp->tipe_bangunan == 'apartemen'){
                                    ?>
                                        <i class="fa fa-building"></i> Apartement
                                    <?php
                                    }
                                    ?>
                                </div>
                                <a class="hover_effect h_yellow h_link" href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>">
                                <img src="<?= site_url('assets/uploads/thumbnails_produk/medium/').$gdp->gambar_thumb ?>" class="img-responsive" alt="Image" style="width: 764px; height: 180px">
                                </a>
                                <div style="
                                    position: absolute;
                                    bottom: 0;
                                    text-align: center;
                                    z-index: 9;
                                " class="btn-group btn-group-justified" role="group" aria-label="...">
                                  <div class="btn-group" role="group">
                                    <button class="button btn_dark btn_sm btn-block " data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Luas Bangunan" data-content="Luas : <?= $gdp->luas_bangunan ?> m²" style="background-color: #444444; border-color: #444444; border: 0" >
                                      LuasBangunan :
                                      <?= $gdp->luas_bangunan ?> m²
                                    </button>
                                  </div>
                                    
                                  <div class="btn-group" role="group">
                                    <button class="button btn_dark btn_sm btn-block" data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Luas Tanah" data-content="Luas : <?= $gdp->luas_tanah ?> m²" style="background-color: #444444; border-color: #444444; border: 0">
                                      LuasTanah :
                                      <?= $gdp->luas_tanah ?> m²   
                                    </button>
                                  </div>
                                    
                                </div>
                             </figure>
                             </article>
                             <div class="room_info" style=" padding: 10px;">
                                <h4 class="mt5" style="font-weight: 700">
                                  <a href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>" class="text-info">
                                    <?php
                                    $sumchar = strlen($gdp->nama_produk );
                                    if ($sumchar <= 20) {
                                       echo $gdp->nama_produk;
                                       echo "<br>";
                                       echo "<br>";
                                     }else{
                                       echo substr($gdp->nama_produk, 0, 36).'...';
                                     }
                                    ?>  
                                  </a>
                                </h4>
                                <h4 class="text-warning" style="font-family: sans-serif; margin-bottom: -0.5rem">
                                  <?php
                                    if ($this->session->userdata('is_login')) {
                                      $qp = $this->mod_sb->mengambil('pengguna', ['username'=>$this->session->userdata('uname')])->row();
                                       if ($qp->no_tlp != null && $qp->KTP != null && $qp->Penghasilan != null) {
                                          echo $this->lee->getRupiah2($gdp->harga); 
                                       } else {
                                          echo $this->lee->getRupiahWithSensor($gdp->harga); 
                                       }                             
                                    }else{
                                      echo $this->lee->getRupiahWithSensor($gdp->harga); 
                                    }
                                    
                                  ?>
                                  <!-- diskon -->
                                  <!-- <label style="text-decoration: line-through;">Rp 1.500.000</label> -->
                                </h4>
                                <p>
                                  <a href="<?= site_url('profile_pengembang/').md5($gdp->id_pengembang) ?>" style="color: #444">
                                    <?php 
                                      $sumchar = strlen($gdp->nama);
                                      if ($sumchar <= 28) {
                                      ?>
                                        <?= $gdp->nama ?>
                                      <?php
                                      }else{
                                      ?>
                                        <?= substr($gdp->nama, 0, 24).'...' ?>
                                      <?php
                                      }
                                    ?>
                                  </a>
                                  <br>
                                  <?php 
                                  $sumchar = strlen($gdp->nama_proyek);
                                  if ($sumchar <= 28) {
                                  ?>
                                    <?= $gdp->nama_proyek ?>
                                  <?php
                                  }else{
                                  ?>
                                    <?= substr($gdp->nama_proyek, 0, 24).'...' ?>
                                  <?php
                                  }
                                ?></p>
                                <p>
                                <!-- <?= substr($this->typography->auto_typography($gdp->deskripsi), 0, 29).'...'; ?> -->
                                </p>
                                <div class="text-center">
                                  <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Ruang tamu" data-content="Jumlah : <?= $gdp->ruang_tamu ?>">
                                    <i class="fa fa-television"></i>
                                    Ruang Tamu :
                                    <?= $gdp->ruang_tamu ?>
                                  </button>
                                  <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Kamar Tidur" data-content="Jumlah : <?= $gdp->kamar_tidur ?>">
                                    <i class="fa fa-bed"></i> 
                                    Ruang Tidur :
                                    <?= $gdp->kamar_tidur ?>
                                  </button>
                                  <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Kamar Mandi" data-content="Jumlah : <?= $gdp->kamar_mandi ?>">
                                    <i class="fa fa-bathtub"></i> 
                                    Ruang Mandi :
                                    <?= $gdp->kamar_mandi ?>
                                  </button>
                                  <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Dapur" data-content="Jumlah : <?= $gdp->dapur ?>">
                                    <i class="fa fa-cutlery"></i>
                                    Ruang Dapur :
                                    <?= $gdp->dapur ?>
                                  </button>
                                </div>
                                <a href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button  btn_blue btn_full upper mt20">Lebih Detail</a>
                             </div>
                            </div>
                        </div>
                      <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar">
                    <aside class="widget">
                        <div class="vbf">
                            <h2 class="form_title" id="pesan">
                                <img src="<?= site_url('assets/logo/resis_logo_transparent_1.png') ?>" alt="" class="img img-responsive center-block" width="62px" height="62px">PESAN SEKARANG
                            </h2>
                            <form id="booking-form" class="inner">
                                <div class="form-group">
                                    <input class="form-control" name="booking-name" placeholder="Nama" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="booking-email" placeholder="Alamat Email" type="email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="booking-phone" placeholder="Nomor Telepon Genggam" type="text">
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="message" cols="70" rows="20" placeholder="Ketikkan Pesan Disini" class="form-control">Saya tertarik dengan properti ini, Mohon hubungi Saya segera, Terima kasih!</textarea>
                                </div><br>
                                <button class="button btn_lg btn_yellow btn_full" type="submit" onclick="alert('Sorry this features under the maintenance! please chat admin at bottom right for customer services')">PESAN PROPERTY</button>
                            <!--     <div class="a_center mt10">
                                    <a href="booking-form.html" class="a_b_f">Advanced Booking Form</a>
                                </div> -->
                            </form>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>BUTUH BANTUAN?</h4>
                        <div class="help">
                            Jika Anda mempunyai pertanyaan, jangan ragu untuk hubungi kami dengan cara klik pada button Chat dipojok kanan bawah, atau hubungi :
                            <div class="phone"><i class="fa  fa-phone"></i><a href="tel:+62857-4799-9476"> +62 857-4799-9476 </a></div>
                            <div class="email"><i class="fa  fa-envelope-o "></i><a href="mailto:cs@resis.id">cs@resis.id</a> || <a href="contact.html"> Kontak Kami</a></div>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>BERITA TERBARU</h4>
                        <div class="latest_posts">
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_yellow">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb1.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">10 Cara memilih property yang pas untuk Anda</a></h6>
                                    <span><i class="fa fa-calendar"></i>28/06/2021</span>
                                </div>
                            </article>
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_yellow">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Tip & Trik memilih rumah impian</a></h6>
                                    <span><i class="fa fa-calendar"></i>06/06/2021</span>
                                </div>
                            </article>
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_yellow">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Cek rumah tanpa ribet? virtual tour aja di Resis.id</a></h6>
                                    <span><i class="fa fa-calendar"></i>06/06/2021</span>
                                </div>
                            </article>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
//setingan marker peta produk
var url = '<?php echo site_url('Konsumen/ProdukDetail/getLokasi/'.$id_lokasi) ?>';
$.ajax({
  url : url,
  method : 'POST',
  dataType : 'JSON',
  success : function (response) {
    console.log(response);
    var mapOptions = {
        center: [response.data.lat, response.data.long],
        zoom: 13
    } 
    var maapin = new L.map('peta', mapOptions);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?accessToken={accessToken}', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'AIzaSyD4NfvktSJrrnmUz9hTTfEJcCmHC-PRBpc'
    }).addTo(maapin);
    L.marker([response.data.lat, response.data.long]).addTo(maapin)
        .bindPopup(response.data.nama+'<br>'+response.data.pengembang)
        .openPopup();
  }
});

</script>