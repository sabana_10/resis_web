 <!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay-2" style="background: url<?= site_url('assets/main/') ?>(images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <h1>Pengembang</h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url('main') ?>">Home</a></li>
                <li >Pengembang</li>
            </ol>
        </div>
    </div>
</div>
<!-- ========== MAIN SECTION ========== -->
<main id="about_us_page">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                        <img src="<?= site_url('assets/uploads/logo_pengembang/').$gdpg->logo ?>" class=" img-responsive" alt="Image" style="width: 200px; height: 200px;">
                        <!-- <img src="<?= site_url('assets/main/') ?>images/about.jpg" class="img1 img-responsive" alt="Image"  style="width: 200px; height: 200px"> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="countup_box">
                            <div class="inner">
                                <div class="countup number" data-count="<?= $getProyek ?>"></div>
                                <div class="text">Proyek</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="countup_box">
                            <div class="inner">
                                <div class="countup number" data-count="50"></div>
                                <div class="text">Properti</div>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="sidebar">
                    <form id="formCari" action="<?php echo site_url('property/cari_property') ?>" method="POST" class="widget_search" enctype="multipart/form-data">
                    <aside class="widget">
                      <div class="search">
                        <input type="search" id="cariProperty" name="cariProperty" placeholder="Mulai Cari Property...">
                        <button class="search_btn" id="searchsubmit" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </aside>
                    </form>
                    <aside class="widget">
                        <h4>Filter</h4>
                        <!-- form open -->
                        <form method="POST" action="<?= site_url('property/filter_property') ?>" id="formCariFilter" class="widget_search" enctype="multipart/form-data">
                          <!-- subsidi -->
                          <label style="color: #444; font-weight: 700">SUBSIDI</label><br>
                            <?php 
                              if (set_value('subsidi') != 1) {
                            ?>
                            <input type="hidden" name="subsidi" value="0">
                            <input type="checkbox" name="subsidi" value="1"> Subsidi<br>
                            <?php
                              }else{
                            ?>
                            <input type="hidden" name="subsidi" value="0">
                            <input type="checkbox" name="subsidi" value="1" checked="checked"> Subsidi<br>
                            <?php
                              }
                            ?>
                          <!-- lokasi -->
                          <label class="mt10" style="color: #444; font-weight: 700">LOKASI</label>
                          <div class="">
                            <div class="input-group">
                              Kabupaten
                              <select name="kabupaten" class="form-control" id="kabupaten" oninput="tested()">
                                  <!-- <option value="<?= set_value('kabupaten') ?>"><?= set_value('kabupaten') != null? set_value('kabupaten') : 'Pilih Kabupaten Disini' ?></option> -->
                                  <option value="">Pilih Kabupaten Disini</option>
                                  <?php
                                foreach ($getKabupaten as $gk) {
                                ?>
                                  <option value="<?= $gk->kabupaten ?>"><?= $gk->name ?></option>
                                <?php
                                }
                                  ?>
                              </select> 
                            </div>
                            <div class="form-group">
                                Kecamatan
                                <input type="hidden" name="kecamatan" value="0">
                                <input type="text" id="input_kecamatan" class="form-control" placeholder="Kecamatan" readonly="">
                                <select name="kecamatan" class="form-control kecamatan" id="kecamatan">
                                  <option value="">Pilih Kecamatan Disini</option>
                                </select>
                            </div>
                          </div>
                          <!-- harga -->
                          <label class="mt10" style="color: #444; font-weight: 700">HARGA</label>
                          <div class="row mb10">
                            <div class="col-md-12">
                              <div class="form-group">
                                Min Rp.
                                <input type="number" name="min_harga" class="form-control" id="harga" value="<?= set_value('min_harga') != null ? set_value('min_harga') : '' ?>" placeholder="Minimum Harga">
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                Max Rp.
                                <input type="number" name="max_harga" class="form-control" id="harga" value="<?= set_value('max_harga') != null ? set_value('max_harga') : '' ?>" placeholder="Maximum Harga">
                              </div>
                            </div>
                          </div>
                          <!-- luas tanah -->
                         <!--  <label style="color: #444; font-weight: 700">LUAS TANAH</label><br>
                          <?php foreach ($getDataLuasTanah as $lt) : ?>
                            <input type="hidden" name="luas_tanah" value="0">
                            <input type="checkbox" name="luas_tanah" value="<?= $lt->luas_tanah ?>"  > <?= $lt->luas_tanah ?> m² <br>
                          <?php endforeach; ?>
                          <label style="color: #444; font-weight: 700" class="mt10">LUAS BANGUNAN</label><br>
                            <?php foreach ($getDataLuasBangunan as $lb) : ?>
                            <input type="hidden" name="luas_bangunan" value="0">
                            <input type="checkbox" name="luas_bangunan" value="<?= $lb->luas_bangunan ?>" > <?= $lb->luas_bangunan ?> m² <br> -->
                          <!-- <?php endforeach; ?> -->
                          <!-- generate button -->
                          <button class="button btn_full btn_blue mt15" type="submit"><i class="fa fa-search"></i>Cari Produk</button>         
                        </form>
                        <!-- form close -->
                    </aside>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-9">
                <div class="main_title mt_yellow a_left">
                  <h2 class=""><?= $gdpg->nama ?> - sejak <?= $gdpg->tahun_berdiri ?></h2>
                </div>
                <h3 class="mb10">Tentang Pengembang</h3>
                <p><?= $this->typography->auto_typography($gdpg->deskripsi); ?></p><br>
                <!-- properti lainnya -->
                <div id="rooms_grid">
                  <div class="main_title mt_wave mt_yellow a_left">
                      <h2 class="">Property dijual</h2>
                      <!-- <a class="button btn_sm btn_yellow f_right" href="<?= site_url('property') ?>">Lebih Banyak</a> -->
                  </div>
                  <div class="row">
                      <?php foreach ($getDataProduk as $gdp) : ?>
                      <div class="col-md-4">
                          <div class="room_grid_item_2" style="
                            background: #f5f5f5;
                            margin-bottom: 50px;
                            border: 1px solid #eeeeee;
                            border-radius: 1px;">
                           <article class="room">
                             <figure>
                              <div class="price">
                                  <?php if ($gdp->tipe_bangunan == 'rumah') {
                                  ?>
                                      <i class="fa fa-home"></i> Rumah
                                  <?php
                                  } else if($gdp->tipe_bangunan == 'vila'){
                                  ?>
                                      <i class="fa fa-building"></i> Villa
                                  <?php
                                  }else if($gdp->tipe_bangunan == 'apartemen'){
                                  ?>
                                      <i class="fa fa-building"></i> Apartement
                                  <?php
                                  }
                                  ?>
                              </div>
                              <a class="hover_effect h_yellow h_link" href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>">
                              <img src="<?= site_url('assets/uploads/thumbnails_produk/medium/').$gdp->gambar_thumb ?>" class="img-responsive" alt="Image" style="width: 764px; height: 180px">
                              </a>
                              <div style="
                                  position: absolute;
                                  bottom: 0;
                                  text-align: center;
                                  z-index: 9;
                              " class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                  <button class="button btn_dark btn_sm btn-block " data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Luas Bangunan" data-content="Luas : <?= $gdp->luas_bangunan ?> m²" style="background-color: #444444; border-color: #444444; border: 0" >
                                    LuasBangunan :
                                    <?= $gdp->luas_bangunan ?> m²
                                  </button>
                                </div>
                                  
                                <div class="btn-group" role="group">
                                  <button class="button btn_dark btn_sm btn-block" data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Luas Tanah" data-content="Luas : <?= $gdp->luas_tanah ?> m²" style="background-color: #444444; border-color: #444444; border: 0">
                                    LuasTanah :
                                    <?= $gdp->luas_tanah ?> m²   
                                  </button>
                                </div>
                                  
                              </div>
                           </figure>
                           </article>
                           <div class="room_info" style=" padding: 10px;">
                              <h4 class="mt5" style="font-weight: 700">
                                <a href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>" class="text-info">
                                  <?php
                                  $sumchar = strlen($gdp->nama_produk );
                                  if ($sumchar <= 20) {
                                     echo $gdp->nama_produk;
                                     echo "<br>";
                                     echo "<br>";
                                   }else{
                                     echo substr($gdp->nama_produk, 0, 36).'...';
                                   }
                                  ?>  
                                </a>
                              </h4>
                              <h4 class="text-warning" style="font-family: sans-serif; margin-bottom: -0.5rem">
                                <?php
                                  if ($this->session->userdata('is_login')) {
                                    $qp = $this->mod_sb->mengambil('pengguna', ['username'=>$this->session->userdata('uname')])->row();
                                     if ($qp->no_tlp != null && $qp->KTP != null && $qp->Penghasilan != null) {
                                        echo $this->lee->getRupiah2($gdp->harga); 
                                     } else {
                                        echo $this->lee->getRupiahWithSensor($gdp->harga); 
                                     }                             
                                  }else{
                                    echo $this->lee->getRupiah2($gdp->harga); 
                                  }
                                  
                                ?>
                                <!-- diskon -->
                                <!-- <label style="text-decoration: line-through;">Rp 1.500.000</label> -->
                              </h4>
                              <p>
                                <a href="<?= site_url('profile_pengembang/').md5($gdp->id_pengembang) ?>" style="color: #444">
                                  <?php 
                                    $sumchar = strlen($gdp->nama);
                                    if ($sumchar <= 28) {
                                    ?>
                                      <?= $gdp->nama ?>
                                    <?php
                                    }else{
                                    ?>
                                      <?= substr($gdp->nama, 0, 24).'...' ?>
                                    <?php
                                    }
                                  ?>
                                </a>
                                <br>
                                <?php 
                                $sumchar = strlen($gdp->nama_proyek);
                                if ($sumchar <= 28) {
                                ?>
                                  <?= $gdp->nama_proyek ?>
                                <?php
                                }else{
                                ?>
                                  <?= substr($gdp->nama_proyek, 0, 24).'...' ?>
                                <?php
                                }
                              ?></p>
                              <p>
                              <!-- <?= substr($this->typography->auto_typography($gdp->deskripsi), 0, 29).'...'; ?> -->
                              </p>
                              <div class="text-center">
                                <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Ruang tamu" data-content="Jumlah : <?= $gdp->ruang_tamu ?>">
                                  <i class="fa fa-television"></i>
                                  Ruang Tamu :
                                  <?= $gdp->ruang_tamu ?>
                                </button>
                                <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Kamar Tidur" data-content="Jumlah : <?= $gdp->kamar_tidur ?>">
                                  <i class="fa fa-bed"></i> 
                                  Ruang Tidur :
                                  <?= $gdp->kamar_tidur ?>
                                </button>
                                <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover" data-original-title="Kamar Mandi" data-content="Jumlah : <?= $gdp->kamar_mandi ?>">
                                  <i class="fa fa-bathtub"></i> 
                                  Ruang Mandi :
                                  <?= $gdp->kamar_mandi ?>
                                </button>
                                <button class="button btn_xs" data-toggle="popover" data-placement="top" data-trigger="hover"  data-original-title="Dapur" data-content="Jumlah : <?= $gdp->dapur ?>">
                                  <i class="fa fa-cutlery"></i>
                                  Ruang Dapur :
                                  <?= $gdp->dapur ?>
                                </button>
                              </div>
                              <a href="<?php echo site_url('property/detail_property/').md5($gdp->id) ?>" class="button  btn_blue btn_full upper mt20">Lebih Detail</a>
                           </div>
                         </div>
                        </div>
                    <?php endforeach; ?>
                  </div>
              </div>
            </div>
        </div>
        <div class="row">
            
        </div>
    </div>

</main>

<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
$(document).ready(function(){
  $('#kecamatan').hide();
  $('#cariProperty').autocomplete({
    source : function(request, response) {
      $.ajax({
        url     : '<?php echo site_url('Konsumen/Pengembang/autoCompleteProperty/') ?>'+request.term,
        type    : 'GET',
        dataType: 'JSON',
        success : function (data) {
          // console.log(data);
          response(data);
        }
      });
    }, select: function(event, selectedData) {
      // console.log(selectedData.item.id);
      document.location.href='<?php echo site_url('property/cari_property/') ?>'+selectedData.item.id;
    }
  });
  // $('#formCari').on('submit', function(e) {
  //   e.preventDefault();
  //   var id =$('#cariProperty').val();
  //   url = '<?php echo site_url('property/cari_property/') ?>'+id;
  //   $.ajax({
  //     url : url,
  //     method : 'POST',
  //     data : new FormData(this),
  //     contentType : false,
  //     cache : false,
  //     processData : false,
  //     dataType : 'JSON',
  //     success : function (data) {
  //       document.location.href='<?php echo site_url('property/cari_property/') ?>'+id;
  //     },
  //     error : err=>{
  //     }
  //   });
  // });
})
</script>