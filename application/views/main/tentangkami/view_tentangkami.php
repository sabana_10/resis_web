<!-- ========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay-2" style="background: url(images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <h1>Tentang RESIS</h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Home</a></li>
                <li>Tentang RESIS</li>
            </ol>
        </div>
    </div>
</div>

<!-- ========== MAIN SECTION ========== -->
<main id="about_us_page">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="mb30">Real Estate Sales Information System (RESIS) - Berdiri Tahun 2021</h1>
                <p>Bisnis properti atau perumahan adalah salah satu sektor yang memiliki multiplier effect yang tinggi terhadap sektor bisnis lainnya. Sektor bisnis properti setidaknya mempengaruhi 147 bisnis turunan. Hal ini tentu saja berpengaruh terhadap kemajuan ekonomi nasional. Beberapa industri yang langsung berpengaruh dengan adanya bisnis perumahan diantaranya : material bahan bangunan, genteng, semen, paku, besi, kayu, dan lainnya. Selain itu pendapatan Perusahaan Listrik Negara (PLN), Perusahaan Daerah Air Minum (PDAM), Sektor Perbankan juga cukup bergantung pada perumahan atau properti.</p>

                <p>
                Real Estate Indonesia (REI) adalah salah satu asosiasi yang menaungi pengembang perumahan (developer) di seluruh Indonesia. Peran REI cukup besar dalam perkembangan bisnis perumahan serta properti dalam ruang lingkup daerah maupun nasional, hal ini terlihat dari ruang lingkup kegiatan REI yaitu : 
                </p>
                <ul class="list_menu">
                    <li>Mengusahakan/memperoleh tanah dari masyarakat, dan/atau dari pemerintah serta mematangkan tanah dan melaksanakan pembangunan di atas tanah tersebut.</li>
                    <li>Mengelola serta menyewakan tanah dengan atau tanpa bangunan berupa perumahan, pertokoan, perkantoran, pergudangan, kawasan industri, dan tempat-tempat rekreasi.</li>
                    <li>Usaha-usaha yang sah, yang masih berhubungan dengan real estate, seperti jasa appraisal, peragenan, property management, pialang, pengembangan promosi, penyuluhan real estate, dan industri real estate. </li>
                </ul>
                <p>
                Sekitar 750 pengembang perumahan (developer) di seluruh Indonesia tergabung dalam Asosiasi ini. REI memiliki komisariat yang terbagi di beberapa daerah, diantaranya adalah Banyumas Raya, di Banyumas Raya sendiri sekitar 64 pengembang perumahan (developer) tergabung dalam asosiasi ini. REI Banyumas Raya memberikan kontribusi yang cukup besar terhadap realisasi investasi serta pendapatan atau pajak daerah. Data dari Dinas Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Provinsi Jawa Tengah menyebutkan bahwa sektor perumahan adalah salah satu dari lima sektor yang mendominasi pada kegiatan Penanaman Modal Asing (PMA) di Jawa Tengah pada tahun 2019. Berdasarkan data yang pada Dokumen Laporan Keuangan Kabupaten Banyumas menyebutkan bahwa dari tahun ke tahun penerimaan Bea Perolehan Hak atas Tanah dan Bangunan (BPHTB) di Banyumas Raya mengalami peningkatan yang signifikan serta menjadi salah satu penerimaan  Pendapatan Asli Daerah (PAD) yang terbesar.  Hal ini tentu saja tidak lepas dari kontribusi pengembang perumahan (developer) yang tergabung pada Asosiasi REI Banyumas Raya.
                </p>
                <p>
                Sejak pandemi covid-19 diawal tahun 2020. Pergerakan pasar properti memiliki pola yang tidak stabil. Berdasarkan catatan Indonesia Property Watch (IPW), pasar properti sempat anjlok sampai 50,1 persen pada awal pandemi kuartal I/2020. Penurunan ini bukan dikarenakan pasar kehilangan daya beli, melainkan terganggunya mobilitas masyarakat yang ingin membeli properti secara langsung karena sedang masa pendemi covid-19. Terlebih lagi pemerintah melakukan pengetatan PPKM/PSBB, hal ini akan sangat berpengaruh terhadap pasar properti.
                </p>
                <p>
                Dalam rangka mendorong bisnis pengembang perumahan (developer) agar tetap bertahan dalam masa pandemi covid-19 ini serta mempermudah masyarakat dalam memilih properti yang baik tanpa harus keluar rumah, kami Resis team berinovasi untuk membuat sebuah platform Real Estate Sales Information System (RESIS) diharapkan dapat membantu dan mendorong bisnis perumahan yang dikelola anggota REI dengan mengedukasi masyarakat, membangun kepercayaan lebih dan memudahkan dalam memilih properti impian.
                </p>
                    
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <img src="<?= site_url('assets/logo/resis_logo_transparent.png') ?>" class=" img-responsive" alt="Image" style="width: 400px; height: 400px;">
                        <!-- <img src="<?= site_url('assets/main/') ?>images/about.jpg" class="img1 img-responsive" alt="Image"  style="width: 200px; height: 200px"> -->
                    </div>
                    <div class="col-md-1"></div>
               </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="countup_box">
                            <div class="inner">
                                <div class="countup number" data-count="150"></div>
                                <div class="text">Rooms</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="countup_box">
                            <div class="inner">
                                <div class="countup number" data-count="50"></div>
                                <div class="text">staffs</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="countup_box">
                            <div class="inner">
                                <div class="countup number" data-count="4"></div>
                                <div class="text">restaurant</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="countup_box">
                            <div class="inner">
                                <div class="countup number" data-count="3"></div>
                                <div class="text">pools</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row image-gallery">
            <div class="col-md-3 col-sm-6 mt20 mb20 br2">
                <a href="<?= site_url('assets/main/') ?>images/restaurant.jpg" class="hover_effect h_lightbox h_yellow">
                    <img src="<?= site_url('assets/main/') ?>images/restaurant.jpg" class="img-responsive full_width br2" alt="Image">
                </a>
            </div>
            <div class="col-md-3 col-sm-6 mt20 mb20 br2">
                <a href="<?= site_url('assets/main/') ?>images/spa.jpg" class="hover_effect h_lightbox h_yellow">
                    <img src="<?= site_url('assets/main/') ?>images/spa.jpg" class="img-responsive full_width br2" alt="Image">
                </a>
            </div>
            <div class="col-md-3 col-sm-6 mt20 mb20 br2">
                <a href="<?= site_url('assets/main/') ?>images/conference.jpg" class="hover_effect h_lightbox h_yellow">
                    <img src="<?= site_url('assets/main/') ?>images/conference.jpg" class="img-responsive full_width br2" alt="Image">
                </a>
            </div>
            <div class="col-md-3 col-sm-6 mt20 mb20 br2">
                <a href="<?= site_url('assets/main/') ?>images/swimming.jpg" class="hover_effect h_lightbox h_yellow">
                    <img src="<?= site_url('assets/main/') ?>images/swimming.jpg" class="img-responsive full_width br2" alt="Image">
                </a>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
        
        <p>per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum. </p> -->
    </div>

</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
</script>