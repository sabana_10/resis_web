<!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay-2">
    <div class="container">
        <div class="inner">
            <h1>KPR</h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url('') ?>">Home</a></li>
                <li>KPR</li>
            </ol>
        </div>
    </div>
</div>

<!-- =========== MAIN ========== -->
<main id="our_staff_page">
    <div class="container">
        <div class="row">
            <!-- ITEM -->
            <div class="col-md-3 col-sm-6">
              <div class="sidebar">
                <aside class="widget">
                    <div class="search">
                        <form method="get" class="widget_search">
                            <input type="search" placeholder="Mulai Cari...">
                            <button class="search_btn" id="searchsubmit" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                </aside>
                <aside class="widget">
                  <h4>Berita Terbaru</h4>
                  <div class="latest_posts">
                    <article class="latest_post">
                      <figure>
                          <a href="blog-post.html" class="hover_effect h_link h_yellow">
                              <img src="<?= site_url('assets/main/') ?>images/blog/thumb1.jpg" alt="Image">
                          </a>
                      </figure>
                      <div class="details">
                          <h6><a href="blog-post.html">10 Cara memilih property yang pas untuk Anda</a></h6>
                          <span><i class="fa fa-calendar"></i>28/06/2021</span>
                      </div>
                  </article>
                  <article class="latest_post">
                      <figure>
                          <a href="blog-post.html" class="hover_effect h_link h_yellow">
                              <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                          </a>
                      </figure>
                      <div class="details">
                          <h6><a href="blog-post.html">Tip & Trik memilih rumah impian</a></h6>
                          <span><i class="fa fa-calendar"></i>06/6/2021</span>
                      </div>
                  </article>
                  <article class="latest_post">
                      <figure>
                          <a href="blog-post.html" class="hover_effect h_link h_yellow">
                              <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                          </a>
                      </figure>
                      <div class="details">
                          <h6><a href="blog-post.html">Cek rumah tanpa ribet? virtual tour aja di Resis.id</a></h6>
                          <span><i class="fa fa-calendar"></i>06/6/2021</span>
                      </div>
                  </article>
                  </div>
              </aside>
              </div>
            </div>
            <!-- ITEM -->
            <div class="col-md-9 col-sm-6">
              <div class="main_title mt_yellow a_left">
                <h2 class="">Simulasi KPR</h2>
              </div>
              <div class="item" id="akun">
                <div>
                  <h2 style="font-weight: 700">Simulasi KPR</h2>
                  <p>Gunakan kalkulator KPR untuk mendapatkan kemudahan pinjaman kredit rumah, perbandingan suku bunga cicilan bank di Indonesia</p>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group" style="margin-bottom: 1rem">
                        <label for="">Harga Property (Rp.)</label>
                        <input name="hargaProperty" type="text" class="form-control" value="400.000.000" style="color: #444444" >
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group" style="margin-bottom: 1rem">
                        <label for="">Jangka Waktu (Tahun)</label>
                        <select name="jangkaWaktu" id="" class="form-control" style="color: #444444">
                          <option value="5">5 Tahun</option>
                          <option value="6">6 Tahun</option>
                          <option value="7">7 Tahun</option>
                          <option value="8">8 Tahun</option>
                          <option value="9">9 Tahun</option>
                          <option value="10">10 Tahun</option>
                          <option value="11">11 Tahun</option>
                          <option value="12">12 Tahun</option>
                          <option value="13">13 Tahun</option>
                          <option value="14">14 Tahun</option>
                          <option value="15" selected="selected">15 Tahun</option>
                          <option value="16">16 Tahun</option>
                          <option value="17">17 Tahun</option>
                          <option value="18">18 Tahun</option>
                          <option value="19">19 Tahun</option>
                          <option value="20">20 Tahun</option>
                          <option value="21">21 Tahun</option>
                          <option value="22">22 Tahun</option>
                          <option value="23">23 Tahun</option>
                          <option value="24">24 Tahun</option>
                          <option value="25">25 Tahun</option>
                          <option value="26">26 Tahun</option>
                          <option value="27">27 Tahun</option>
                          <option value="28">28 Tahun</option>
                          <option value="29">29 Tahun</option>
                          <option value="30">30 Tahun</option>
                        </select>
                    </div>        
                  </div>
                  <div class="col-md-4">
                    <div class="form-group" style="margin-bottom: 1rem">
                      <label for="">Estimasi Suku Bunga (% per Tahun)</label>
                        <select name="sukuBunga" id="" class="form-control" style="color: #444444">
                          <option value="6.00">6.00% (Tahun)</option>
                          <option value="6.68">6.68% (Tahun)</option>
                          <option value="7.70" selected="selected">7.70% (Tahun)</option>
                          <option value="7.75">7.75% (Tahun)</option>
                          <option value="8.00">8.00% (Tahun)</option>
                          <option value="9.25">9.25% (Tahun)</option>
                          <option value="9.75">9.75% (Tahun)</option>
                          <option value="9.99">9.99% (Tahun)</option>
                          <option value="10.29">10.29% (Tahun)</option>
                          <option value="10.99">10.99% (Tahun)</option>
                          <option value="11.29">11.29% (Tahun)</option>
                        </select> 
                    </div>
                  </div>
                  <div class="col-md-12">
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 1rem">
                        <label for="">Uang Muka / DP 20%</label>
                        <input name="dp" type="text" class="form-control" style="color: #444444" value="80.000.000" readonly="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 1rem">
                        <label for="">Jumlah Pinjaman</label>
                        <input name="pinjaman" type="text" class="form-control" style="color: #444444" value="320.000.000" readonly="">
                    </div>
                  </div>
                </div><br><br>
                <div class="row" style="margin-bottom: 3rem">
                  <div class="col-md-12">
                    <div class="form-group text-center" style="margin-bottom: 1rem">
                        <label for="">Angsuran per bulan</label>
                        <h3 id="angsuran" class="text-success" style="font-weight: 700; font-family: monospace;">Rp. 1.914.667</h3>
                    </div>
                  </div>
                </div>
                <!-- <hr> -->
                <!-- * Simulasi di atas hanya perhitungan dengan suku bunga fixed -->
            </div>
          </div>
        </div>
        
    </div>
</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
$(document).ready(function(){

  $('[name="hargaProperty"]').on('input', function (e) {
    var vDp = $('[name="dp"]').val(),
        vHp = $('[name="hargaProperty"]').val();
    $('[name="hargaProperty"]').val(konversiRupiah(vHp));
    $('[name="dp"]').val(hitungDp(vHp));
    $('[name="pinjaman"]').val(jumlahPinjaman(konversiRupiah(vHp), hitungDp(vHp)));

    $('#angsuran').text('Rp. ' + angsuran());

  });

  $('[name="jangkaWaktu"]').on('input', function (e) {
    var vHp = $('[name="hargaProperty"]').val();
    $('[name="pinjaman"]').val(jumlahPinjaman(konversiRupiah(vHp), hitungDp(vHp)));
    $('#angsuran').text('Rp. ' + angsuran());
  });

  $('[name="sukuBunga"]').on('input', function (e) {
    var vHp = $('[name="hargaProperty"]').val();
    $('[name="pinjaman"]').val(jumlahPinjaman(konversiRupiah(vHp), hitungDp(vHp)));

    $('#angsuran').text('Rp. ' + angsuran());

  });

  function angsuran() {
    var vSb = $('[name="sukuBunga"]').val(),
        vJw = $('[name="jangkaWaktu"]').val(),
        vP = $('[name="pinjaman"]').val(),
        p = parseInt(vP.replace(/[^,\d]/g, '')),
        split = vP.split('.');

    var bln = vJw * 12,
        prs = vSb / 100,
        prs2 = prs * 1000,
        flower = p * prs2 / 1000;

    var angsur = Number((p + flower) / bln).toFixed(0);
    var angsur2 = (p + flower) / bln;

    console.log(p);
    console.log(flower);
    console.log(angsur2);
    return konversiRupiah(angsur.toString());
  }

  function jumlahPinjaman(vHp, vDp) {
    var vH = parseInt(vHp.replace(/[^,\d]/g, '')),
        vD = parseInt(vDp.replace(/[^,\d]/g, '')),
        split1 = vHp.split('.'),
        split2 = vDp.split('.'),
        pinjaman = (vH - vD).toString();

    return konversiRupiah(pinjaman);
  }

  function hitungDp(angkaDp) {
    var vDp = parseInt(angkaDp.replace(/[^,\d]/g, '')),
        dp = (vDp / 100 * 20).toString(),
        split = angkaDp.split('.')

    return konversiRupiah(dp);
  }

  function konversiRupiah(angka) {
    var ns = angka.replace(/[^,\d]/g, '').toString(),
        split = ns.split(','),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return rupiah;
  }
});
</script>