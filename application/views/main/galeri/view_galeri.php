<!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay" style="background: url(images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <h1>Galeri List</h1>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li>Galeri List</li>
            </ol>
        </div>
    </div>
</div>

<!-- =========== MAIN ========== -->
<main class="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="grid gallery_items image-gallery">
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_swimming_pool">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery1.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery1.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Swimming Pool</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6  g_swimming_pool">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery2.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery2.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Swimming Pool</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery3.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery3.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Restaurant</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery4.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery4.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Restaurant</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_spa">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery5.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery5.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Room Service</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery6.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery6.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Restaurant</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_island">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery7.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery7.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Zakynthos</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_island">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery8.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery8.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Zakynthos</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_island">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery9.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery9.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Beach</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_island">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery10.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery10.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Boat</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_spa">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery11.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery11.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption class="item_details">
                            <h4>Spa</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                    <!-- ITEM -->
                    <figure class="g_item col-md-4 col-sm-6 g_swimming_pool">
                        <a href="<?= site_url('assets/main/') ?>images/gallery/gallery12.jpg" class="hover_effect h_lightbox h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/gallery/gallery12.jpg" class="img-responsive" alt="Image">
                        </a>
                        <figcaption>
                            <h4>Swimming Pool</h4>
                            <span>Lorem ipsum dolor sit amet</span>
                        </figcaption>
                    </figure>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar">
                    <aside class="widget">
                        <div class="search">
                            <form method="get" class="widget_search">
                                <input type="search" placeholder="Start Searching...">
                                <button class="search_btn" id="searchsubmit" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>KATEGORI</h4>
                        <ul class="categories">
                            <li><a href="#">Berita Properti <span class="num_posts">51</span></a></li>
                            <li class=""><a href="#">Blog <span class="num_posts">51</span></a></li>
                            <li><a href="#">Galeri <span class="num_posts">51</span></a></li>
                            <li><a href="#">Panduan dan Referensi <span class="num_posts">51</span></a></li>
                            <li><a href="#">Panduan Properti <span class="num_posts">51</span></a></li>
                        </ul>
                    </aside>
                    <aside class="widget">
                        <h4>LIST GALERI</h4>
                        <ul class="categories">
                            <li><a href="#">Rumah <span class="num_posts">51</span></a></li>
                            <li><a href="#">Vila <span class="num_posts">51</span></a></li>
                            <li><a href="#">Apartemen <span class="num_posts">51</span></a></li>
                            <li><a href="#">Kegiatan Hari Ini <span class="num_posts">51</span></a></li>
                            <li class=""><a href="#">Testimoni <span class="num_posts">51</span></a></li>
                        </ul>
                    </aside>
                    <aside class="widget">
                        <h4>BERITA TERBARU</h4>
                        <div class="latest_posts">
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb1.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Live your myth in Greece</a></h6>
                                    <span><i class="fa fa-calendar"></i>23/11/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Hotel Zante in pictures</a></h6>
                                    <span><i class="fa fa-calendar"></i>18/10/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb3.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Hotel Zante family party</a></h6>
                                    <span><i class="fa fa-calendar"></i>13/08/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb4.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Hotel Zante weddings</a></h6>
                                    <span><i class="fa fa-calendar"></i>13/08/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb5.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">10 things you should know</a></h6>
                                    <span><i class="fa fa-calendar"></i>13/08/2017</span>
                                </div>
                            </article>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>Tags</h4>
                        <div class="tagcloud clearfix">
                            <a href="#"><span class="tag">Hotel Zante</span><span class="num">12</span></a>
                            <a href="#"><span class="tag">HOLIDAYS</span><span class="num">24</span></a>
                            <a href="#"><span class="tag">PARTY</span><span class="num">8</span></a>
                            <a href="#"><span class="tag">GREECE</span><span class="num">4</span></a>
                            <a href="#"><span class="tag">PARTY</span><span class="num">56</span></a>

                            <a href="#"><span class="tag">ZAKYNTHOS</span><span class="num">12</span></a>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>ARCHIVE</h4>
                        <ul class="archive">
                            <li><a href="#">May 2017<span class="num_posts">21</span></a></li>
                            <li><a href="#">June 2017<span class="num_posts">24</span></a></li>
                            <li><a href="#">July 2017<span class="num_posts">38</span></a></li>
                            <li><a href="#">August 2017<span class="num_posts">11</span></a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>

</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
</script>