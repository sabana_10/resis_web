<!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay" style="background: url(images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <h1>Blog List</h1>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li>Blog List</li>
            </ol>
        </div>
    </div>
</div>

<!-- =========== MAIN ========== -->
<main class="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <!-- ITEM -->
                <article class="blog_list">
                    <figure>
                        <a href="blog-post.html" class="hover_effect h_link h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/blog/blog_post1.jpg" class="img-responsive" alt="Image">
                        </a>
                    </figure>
                    <div class="details">
                        <h2><a href="blog-post.html">Live your myth in Greece</a></h2>
                        <div class="info">
                            <span class="meta_part"><a href="#"><i class="fa fa-user"></i>John Doe</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-calendar"></i>February 15, 2017</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-comment-o"></i>68 Comments</a></span>
                            <span class="meta_part"><i class="fa fa-folder-open-o"></i><a href="#">News</a>, <a href="#">Events</a></span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit...</p>
                        <a class="button btn_blue " href="blog-post.html"><i class="fa fa-angle-double-right"></i> Read More </a>
                    </div>
                </article>
                <!-- ITEM -->
                <article class="blog_list">
                    <figure>
                        <a href="blog-post.html" class="hover_effect h_link h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/blog/blog_post2.jpg" class="img-responsive" alt="Image">
                        </a>
                    </figure>
                    <div class="details">
                        <h2><a href="blog-post.html">Hotel Zante in pictures</a></h2>
                        <div class="info">
                            <span class="meta_part"><a href="#"><i class="fa fa-user"></i>John Doe</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-calendar"></i>February 15, 2017</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-comment-o"></i>68 Comments</a></span>
                            <span class="meta_part"><i class="fa fa-folder-open-o"></i><a href="#">News</a>, <a href="#">Events</a></span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit...</p>
                        <a class="button btn_blue " href="blog-post.html"><i class="fa fa-angle-double-right"></i> Read More </a>
                    </div>
                </article>
                <!-- ITEM -->
                <article class="blog_list">
                    <figure>
                        <a href="blog-post.html" class="hover_effect h_link h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/blog/blog_post3.jpg" class="img-responsive" alt="Image">
                        </a>
                    </figure>
                    <div class="details">
                        <h2><a href="blog-post.html">Hotel Zante family party</a></h2>
                        <div class="info">
                            <span class="meta_part"><a href="#"><i class="fa fa-user"></i>John Doe</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-calendar"></i>February 15, 2017</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-comment-o"></i>68 Comments</a></span>
                            <span class="meta_part"><i class="fa fa-folder-open-o"></i><a href="#">News</a>, <a href="#">Events</a></span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit...</p>
                        <a class="button btn_blue " href="blog-post.html"><i class="fa fa-angle-double-right"></i> Read More </a>
                    </div>
                </article>
                <!-- ITEM -->
                <article class="blog_list">
                    <figure>
                        <a href="blog-post.html" class="hover_effect h_link h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/blog/blog_post4.jpg" class="img-responsive" alt="Image">
                        </a>
                    </figure>
                    <div class="details">
                        <h2><a href="blog-post.html">Hotel Zante Weddings</a></h2>
                        <div class="info">
                            <span class="meta_part"><a href="#"><i class="fa fa-user"></i>John Doe</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-calendar"></i>February 15, 2017</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-comment-o"></i>68 Comments</a></span>
                            <span class="meta_part"><i class="fa fa-folder-open-o"></i><a href="#">News</a>, <a href="#">Events</a></span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit...</p>
                        <a class="button btn_blue " href="blog-post.html"><i class="fa fa-angle-double-right"></i> Read More </a>
                    </div>
                </article>
                <!-- ITEM -->
                <article class="blog_list">
                    <figure>
                        <a href="blog-post.html" class="hover_effect h_link h_blue">
                            <img src="<?= site_url('assets/main/') ?>images/blog/blog_post5.jpg" class="img-responsive" alt="Image">
                        </a>
                    </figure>
                    <div class="details">
                        <h2><a href="blog-post.html">10 Things You Should Know</a></h2>
                        <div class="info">
                            <span class="meta_part"><a href="#"><i class="fa fa-user"></i>John Doe</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-calendar"></i>February 15, 2017</a></span>
                            <span class="meta_part"><a href="#"><i class="fa fa-comment-o"></i>68 Comments</a></span>
                            <span class="meta_part"><i class="fa fa-folder-open-o"></i><a href="#">News</a>, <a href="#">Events</a></span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit...</p>
                        <a class="button btn_blue " href="blog-post.html"><i class="fa fa-angle-double-right"></i> Read More </a>
                    </div>
                </article>
                <nav>
                    <ul class="pagination">
                        <li class="prev_pagination"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li class="next_pagination"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3">
                <div class="sidebar">
                    <aside class="widget">
                        <div class="search">
                            <form method="get" class="widget_search">
                                <input type="search" placeholder="Start Searching...">
                                <button class="search_btn" id="searchsubmit" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>KATEGORI</h4>
                        <ul class="categories">
                            <li><a href="#">Berita Properti <span class="num_posts">51</span></a></li>
                            <li class=""><a href="#">Blog <span class="num_posts">51</span></a></li>
                            <li><a href="#">Galeri <span class="num_posts">51</span></a></li>
                            <li><a href="#">Panduan dan Referensi <span class="num_posts">51</span></a></li>
                            <li><a href="#">Panduan Properti <span class="num_posts">51</span></a></li>
                        </ul>
                    </aside>
                    <aside class="widget">
                        <h4>BERITA TERBARU</h4>
                        <div class="latest_posts">
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb1.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Live your myth in Greece</a></h6>
                                    <span><i class="fa fa-calendar"></i>23/11/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Hotel Zante in pictures</a></h6>
                                    <span><i class="fa fa-calendar"></i>18/10/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb3.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Hotel Zante family party</a></h6>
                                    <span><i class="fa fa-calendar"></i>13/08/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb4.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">Hotel Zante weddings</a></h6>
                                    <span><i class="fa fa-calendar"></i>13/08/2017</span>
                                </div>
                            </article>
                            <!-- ITEM -->
                            <article class="latest_post">
                                <figure>
                                    <a href="blog-post.html" class="hover_effect h_link h_blue">
                                        <img src="<?= site_url('assets/main/') ?>images/blog/thumb5.jpg" alt="Image">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h6><a href="blog-post.html">10 things you should know</a></h6>
                                    <span><i class="fa fa-calendar"></i>13/08/2017</span>
                                </div>
                            </article>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>Tags</h4>
                        <div class="tagcloud clearfix">
                            <a href="#"><span class="tag">Hotel Zante</span><span class="num">12</span></a>
                            <a href="#"><span class="tag">HOLIDAYS</span><span class="num">24</span></a>
                            <a href="#"><span class="tag">PARTY</span><span class="num">8</span></a>
                            <a href="#"><span class="tag">GREECE</span><span class="num">4</span></a>
                            <a href="#"><span class="tag">PARTY</span><span class="num">56</span></a>

                            <a href="#"><span class="tag">ZAKYNTHOS</span><span class="num">12</span></a>
                        </div>
                    </aside>
                    <aside class="widget">
                        <h4>ARCHIVE</h4>
                        <ul class="archive">
                            <li><a href="#">May 2017<span class="num_posts">21</span></a></li>
                            <li><a href="#">June 2017<span class="num_posts">24</span></a></li>
                            <li><a href="#">July 2017<span class="num_posts">38</span></a></li>
                            <li><a href="#">August 2017<span class="num_posts">11</span></a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>

</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
</script>