<!-- =========== GOOGLE MAP ========== -->
<div id="map">
    <div id="" style="width: 100%;
    height: 400px;">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.4317729566887!2d109.25041001459591!3d-7.4173732946484465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655e933c7ebafb%3A0x405e82ed924c8e98!2sMarketing%20Gallery%20Banyumas%20Property%20Center!5e0!3m2!1sid!2sid!4v1623483212166!5m2!1sid!2sid" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <button class="button  openmap-btn" id="openStreetView">Toggle Street View</button>
    </div>
    <!-- <div id="map-canvas"></div> -->
</div>

<!-- ========== MAIN ========== -->
<main id="contact_page">
    <div class="container">
        <div class="row">
            
            <div class="col-md-8">
                <div class="main_title a_left">
                    <h2>CONTACT US</h2>
                </div>
                <form id="contact-form-page">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Your Name">
                        </div>
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label">Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Phone">
                        </div>
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Your Email">
                        </div>
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label">Subject</label>
                            <input type="text" class="form-control" name="subject" placeholder="Subject">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label">Message</label>
                            <textarea class="form-control" name="message" placeholder="Your Message..."></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="button  btn_blue mt40 upper pull-right">
                                <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Your Message
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="col-md-4">
                <div class="main_title a_left">
                    <h2>GET IN TOUCH</h2>
                </div>
                <ul class="contact-info upper">
                    <li>
                        <span>ADDRESS:</span> Navagio Zakynthos, Greece
                    </li>
                    <li>
                        <span>EMAIL:</span> example@site.com, contact@site.com
                    </li>
                    <li>
                        <span>WEB:</span> www.site.com
                    </li>
                    <li>
                        <span>PHONE:</span> +1 (330) <strong>993-443</strong> , +1 (330) <strong>995-445</strong>
                    </li>
                    <li>
                        <span>FAX:</span>
                        <strong>+1 123456780</strong>
                    </li>
                </ul>
                <div class="social_media">
                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                    <a class="googleplus" href="#"><i class="fa fa-google-plus"></i></a>
                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                    <a class="youtube" href="#"><i class="fa fa-youtube"></i></a>
                    <a class="instagram" href="#"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
</script>