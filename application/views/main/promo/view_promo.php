 <!-- =========== PAGE TITLE ========== -->
<div class="page_title gradient_overlay-2" style="background: url<?= site_url('assets/main/') ?>(images/page_title_bg.jpg);">
    <div class="container">
        <div class="inner">
            <h1>Promo</h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Home</a></li>
                <li >Promo</li>
            </ol>
        </div>
    </div>
</div>
<!-- ========== GALLERY ========== -->
<section id="gallery_style_2" class="white_bg">
    <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="sidebar">
              <aside class="widget">
                  <div class="search">
                      <form method="get" class="widget_search">
                          <input type="search" placeholder="Mulai Cari...">
                          <button class="search_btn" id="searchsubmit" type="submit">
                              <i class="fa fa-search"></i>
                          </button>
                      </form>
                  </div>
              </aside>
              <aside class="widget">
                  <h4>Kategori</h4>
                  <ul class="categories">
                      <li><a href="#">Rumah <span class="num_posts">51</span></a></li>
                      <li><a href="#">Apartement <span class="num_posts">51</span></a></li>
                      <li><a href="#">Villa <span class="num_posts">51</span></a></li>
                  </ul>
              </aside>
              <aside class="widget">
                  <h4>Berita Terbaru</h4>
                  <div class="latest_posts">
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb1.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Live your myth in Greece</a></h6>
                              <span><i class="fa fa-calendar"></i>23/11/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb2.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Hotel Zante in pictures</a></h6>
                              <span><i class="fa fa-calendar"></i>18/10/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb3.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Hotel Zante family party</a></h6>
                              <span><i class="fa fa-calendar"></i>13/08/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb4.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">Hotel Zante weddings</a></h6>
                              <span><i class="fa fa-calendar"></i>13/08/2017</span>
                          </div>
                      </article>
                      <!-- ITEM -->
                      <article class="latest_post">
                          <figure>
                              <a href="blog-post.html" class="hover_effect h_link h_blue">
                                  <img src="<?= site_url('assets/main/') ?>images/blog/thumb5.jpg" alt="Image">
                              </a>
                          </figure>
                          <div class="details">
                              <h6><a href="blog-post.html">10 things you should know</a></h6>
                              <span><i class="fa fa-calendar"></i>13/08/2017</span>
                          </div>
                      </article>
                  </div>
              </aside>
              <!-- <aside class="widget">
                  <h4>Tags</h4>
                  <div class="tagcloud clearfix">
                      <a href="#"><span class="tag">Hotel Zante</span><span class="num">12</span></a>
                      <a href="#"><span class="tag">HOLIDAYS</span><span class="num">24</span></a>
                      <a href="#"><span class="tag">PARTY</span><span class="num">8</span></a>
                      <a href="#"><span class="tag">GREECE</span><span class="num">4</span></a>
                      <a href="#"><span class="tag">PARTY</span><span class="num">56</span></a>

                      <a href="#"><span class="tag">ZAKYNTHOS</span><span class="num">12</span></a>
                  </div>
              </aside>
              <aside class="widget">
                  <h4>ARCHIVE</h4>
                  <ul class="archive">
                      <li><a href="#">May 2017<span class="num_posts">21</span></a></li>
                      <li><a href="#">June 2017<span class="num_posts">24</span></a></li>
                      <li><a href="#">July 2017<span class="num_posts">38</span></a></li>
                      <li><a href="#">August 2017<span class="num_posts">11</span></a></li>
                  </ul>
              </aside> -->
            </div>
          </div>
          <div class="col-md-9">
            <div class="main_title mt_wave mt_yellow a_left">
              <h2 class="">Promo Terkini</h2>
            </div>
            <div class="row">
                <!-- ITEM -->
                <div class="col-md-4">
                    <article class="room">
                        <figure>
                            <div class="price">€89 <span>/ night</span></div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/single-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h4><a href="room.html">Single Room</a></h4>
                                <span class="f_right"><a href="rooms-list.html" class="button btn_sm btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-4">
                    <article class="room">
                        <figure>
                            <div class="price">€129 <span>/ night</span></div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/double-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h4><a href="room.html">Double Room</a></h4>
                                <span class="f_right"><a href="room.html" class="button btn_sm btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <!-- ITEM -->
                <div class="col-md-4">
                    <article class="room">
                        <figure>
                            <div class="price"> € 189 <span>/ night</span></div>
                            <a class="hover_effect h_yellow h_link" href="room.html">
                                <img src="<?= site_url('assets/main/') ?>images/rooms/deluxe-room.jpg" class="img-responsive" alt="Image">
                            </a>
                            <figcaption>
                                <h4><a href="room.html">Delux Room</a></h4>
                                <span class="f_right"><a href="room.html" class="button btn_sm btn_blue">Lebih Detail</a></span>
                            </figcaption>
                        </figure>
                    </article>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
<script>
//menghilangkan transparensi header
document.getElementById("header").classList.remove('transparent');
</script>