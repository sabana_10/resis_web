<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Resis - Login</title>
    <!-- icons -->
    <link href="<?= site_url('assets/admin/') ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= site_url('assets/admin/') ?>assets/plugins/iconic/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
    <link href="<?= site_url('assets/admin/') ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="<?= site_url('assets/admin/') ?>assets/css/pages/extra_pages.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="<?= site_url('assets/logo/resis_logo_transparent_1.png') ?>" />

    <style>
        .container-login100-2 {
            width: 100%;
            min-height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            padding: 15px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            position: relative;
            z-index: 1;
        }

        .container-login100-2::before {
            content: "";
            display: block;
            position: absolute;
            z-index: -1;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background-color: rgba(10, 10, 10, 0.8);
            display: block;

        }
    </style>
</head>

<body>
    <div class="limiter">
        <div class="container-login100-2 ">
            <!-- <div class="container-login100 page-background"> -->
            <div class="wrap-login100">
                <form class="login100-form validate-form" id="formLogin">
                    <span class="login100-form-logo">
                        <!-- <i class="zmdi zmdi-flower"></i> -->
                        <img src="<?= site_url('assets/logo/resis_logo_transparent_1.png') ?>" alt="" class='img img-responsive img-thumbnail'>
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27">
                        Register
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Masukkan alamat email">
                        <input class="input100" type="email" name="username" placeholder="Email" autocomplete="OFF">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Masukkan password">
                        <input class="input100" type="password" name="pass" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div style="height: 40px;"></div>
                </form>
                <div class="container-login100-form-btn">
                    <!-- <button class="login100-form-btn btn-block">
							Login
						</button> -->
                    <!-- <a href="<?= site_url('admin/dashboard') ?>" class="login100-form-btn btn-block">
							Login
						</a> -->
                    <button class="login100-form-btn btn-block" onclick="ceklurd()">Register</button>
                </div>
                <div class="text-center" style="margin-top: 50px">
                    <span class="txt1">Already have an account? <a href="/resis/login" class="txt1" style="font-weight:bold">Login.</a></span>
                </div>
            </div>
        </div>
    </div>
    <!-- start js include path -->
    <script src="<?= site_url('assets/admin/') ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- bootstrap -->
    <script src="<?= site_url('assets/admin/') ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= site_url('assets/admin/') ?>assets/js/pages/extra_pages/login.js"></script>
    <!-- end js include path -->
</body>

</html>

<script>
    function ceklurd() {
        // alert($("#formLogin").serialize())

        $.ajax({
            url: 'register/save',
            type: 'POST',
            data: $("#formLogin").serialize(),
            success: function(res) {
                // alert(res)
                let json = $.parseJSON(res)

                if (json.status == 1) {
                    
                    location.href = '/resis/login'
                }else {
                    
                    alert(json.message)
                }
            }

        })
    }
</script>