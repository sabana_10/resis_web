<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"> -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Admin Resis" />
    <meta name="author" content="Resis" />
    <!-- ========== SEO ========== -->
    <title><?= $judul ?></title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta content="" name="author">
    
    <!-- ========== FAVICON ========== -->
    <link rel="apple-touch-icon-precomposed" href="<?= site_url('assets/main/') ?>images/favicon-apple.png" />
	<link rel="icon" href="<?= site_url('assets/logo/') ?>resis_logo_transparent_1.png">

    <!-- ========== STYLESHEETS ========== --> 
    <link href="<?= site_url('assets/main/') ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>revolution/css/layers.css" rel="stylesheet" type="text/css" />
    <link href="<?= site_url('assets/main/') ?>revolution/css/settings.css" rel="stylesheet" type="text/css" />
    <link href="<?= site_url('assets/main/') ?>revolution/css/navigation.css" rel="stylesheet" type="text/css" />
    <link href="<?= site_url('assets/main/') ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/famfamfam-flags.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/style.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/custom.css" rel="stylesheet" type="text/css">
    <link href="<?= site_url('assets/main/') ?>css/responsive.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <!-- slick JS -->
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/main/') ?>slick/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/main/') ?>slick/slick/slick-theme.css"/>
    <!-- sweetalert -->
    <script src="<?= site_url('assets/admin/sweetalert/dist/') ?>sweetalert2.all.min.js"></script>
    <!-- ========== ICON FONTS ========== -->
    <?php  if ($judul == 'Resis - Detail Property' || $judul == 'Resis') {   ?><script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jPushMenu.js"></script>
    <?php    }  ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/main.js"></script>
    <link href="<?= site_url('assets/main/') ?>fonts/font-awesome.min.css" rel="stylesheet">
    <link href="<?= site_url('assets/main/') ?>fonts/flaticon.css" rel="stylesheet">
    
    <!-- ========== GOOGLE FONTS ========== -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7cRaleway:400,500,600,700" rel="stylesheet">
    <!-- leaflet js -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- <style>
      * {
          box-sizing: border-box;
      }
      /*.row::after {
          content: "";
          clear: both;
          display: block;
      }*/
      /*[class*="col-"] {
          float: left;
          padding: 15px;
      }*/

      @media only screen and (min-width: 600px) {
          /* For tablets: */
          .img-hero{display: none;}
          .col-m-1 {width: 8.33%;}
          .col-m-2 {width: 16.66%;}
          .col-m-3 {width: 25%;}
          .col-m-4 {width: 33.33%;}
          .col-m-5 {width: 41.66%;}
          .col-m-6 {width: 50%;}
          .col-m-7 {width: 58.33%;}
          .col-m-8 {width: 66.66%;}
          .col-m-9 {width: 75%;}
          .col-m-10 {width: 83.33%;}
          .col-m-11 {width: 91.66%;}
          .col-m-12 {width: 100%;}
      }
      @media only screen and (min-width: 768px) {
          /* For desktop: */
          .img-hero{display: block;}
          .col-1 {width: 8.33%;}
          .col-2 {width: 16.66%;}
          .col-3 {width: 25%;}
          .col-4 {width: 33.33%;}
          .col-5 {width: 41.66%;}
          .col-6 {width: 50%;}
          .col-7 {width: 58.33%;}
          .col-8 {width: 66.66%;}
          .col-9 {width: 75%;}
          .col-10 {width: 83.33%;}
          .col-11 {width: 91.66%;}
          .col-12 {width: 100%;}
      }

      html {
        scroll-behavior: smooth;
      }
      html, body{
        max-width: 100%;
        overflow-x: hidden;
      }
    </style> -->
</head>

<body>
    <!-- <div id="logedin"><?= ($this->session->userdata('is_login') != null)? $this->session->userdata('is_login') : '' ?></div> -->
    <!-- ========== PRELOADER ========== -->
    <?= $loading ?>
    
    <div class="wrapper">
        
        <!-- ========== HEADER ========== -->
        <?= $header ?>
             
        <!-- ========= content ========== -->
        <?= $content ?>

        <!-- ========== FOOTER ========== -->
        <?= $footer ?>
    
    </div>
    
    <!-- ========== NOTIFICATION ========== -->
    <div id="notification"></div>

    <!-- ========== LOGIN MODAL  ========== -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
      <div class="modal-dialog" role="document" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div style="padding-left:5rem; padding-right: 5rem; padding-bottom: 1rem">
                <div class="main_title a_center mt30">
                    <h3 style="font-weight: 700; color: #444;">Masuk ke Resis.id</h3>
                </div>
                <!-- <div class="text-center"><?= $login_button ?></div> -->
                <a class="btn btn-danger btn-block" href="#" data-toggle="tooltip" data-original-title="Google"><i class="fa fa-google"></i> Continue with Google</a>
                <a class="btn btn-primary btn-block mt10" href="#" data-toggle="tooltip" data-original-title="Facebook" style="margin-top: 1rem"><i class="fa fa-facebook"></i> Continue with Facebook</a>
                <p class="text-center">----- atau -----</p>
                <form id="formLogin" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <input type="email" class="form-control" id="username" name="username" placeholder="Alamat Email">
                    </div>
                    <div class="form-group" style="margin-top: 1rem">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Kata Sandi">
                    </div>
                    <div class="row" style="margin-top: 1rem; margin-bottom: 2rem">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <a href="#" class="text-info" style="float: right;">Lupa kata sandi</a>
                        </div>
                    </div>
                    <button id="btnLogin" class="btn btn-block btn-primary">LOG IN</button>
                </form>
            </div>
          </div>
          <div class="modal-footer">
            <div style="padding-left:5rem; padding-right: 5rem;" class="text-center">
              <p>Belum punya akun? <label class="text-primary" onclick="regshow()">Daftar</label></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ========== REGISTER MODAL  ========== -->
    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width: 450px">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div style="padding-left:5rem; padding-right: 5rem; padding-bottom: 1rem">
                <div class="main_title a_center mt30">
                    <h3 style="font-weight: 700; color: #444;">Daftar Gratis di Resis.id</h3>
                </div>
                <a class="btn btn-danger btn-block" href="#" data-toggle="tooltip" data-original-title="Google"><i class="fa fa-google"></i> Continue with Google</a>
                <a class="btn btn-primary btn-block mt10" href="#" data-toggle="tooltip" data-original-title="Facebook" style="margin-top: 1rem"><i class="fa fa-facebook"></i> Continue with Facebook</a>
                <p class="text-center">----- atau -----</p>
                <form id="formRegister" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <input type="email" class="form-control" id="reg_username" name="reg_username" placeholder="Alamat Email">
                    </div>
                    <div class="form-group" style="margin-top: 1rem;">
                      <input type="password" class="form-control" id="reg_password" name="reg_password" placeholder="Kata Sandi">
                    </div>
                    <div class="form-group" style="margin-top: 1rem; margin-bottom: 2rem">
                      <input type="password" class="form-control" id="reg_password2" name="reg_password2" placeholder="Ulangi Kata Sandi">
                    </div>
                    <button id="btnRegister" class="btn btn-block btn-primary">REGISTER</button>

                    <div style="margin-top: 2rem; margin-bottom: 1rem; font-size: 12px" class="text-center">
                        <input type="checkbox" name="notif" checked="checked"> Ya, kirimkan saya informasi perumahan baru, data properti terkini dan penawaran dari partner
                        <p>Dengan mendaftar, anda setuju dengan syarat Resis.id <a href="#" class="text-primary">Ketentuan Penggunaan</a> dan <a href="#" class="text-primary">kebijakan privasi</a>.</p>
                    </div>
                </form>
            </div>
          </div>
          <div class="modal-footer">
            <div style="padding-left:5rem; padding-right: 5rem;" class="text-center">
              <p>Sudah punya akun? <label class="text-primary" data-toggle="modal" data-target="#loginModal" data-dismiss="modal">Login</label></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ========== REGISTER MODAL  ========== -->
    <div class="modal fade" id="addsModal" tabindex="-3" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document" style="width: 900px; height: 400px">
        <div class="">
          <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <a href="<?= site_url('profile') ?>" data-toggle="tooltip" data-placement="bottom" title="Klik Disini untuk melengkapi!">
              <img src="<?= site_url('assets/uploads/adds/lengkapi_user.jpeg') ?>" alt="" class="img-responsive text-center" style="width: 100%; height: 100%">
            </a>
          </div>
        </div>
      </div>
    </div>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    //tawk API
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/60b637b4de99a4282a1ac4fc/1f73sj6kh';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    $(window).on('load', function() {
      var ses = '<?php echo $this->session->userdata('is_login'); ?>';
      var verif = '<?php echo $this->session->userdata('is_verif'); ?>';
      if ($.cookie('pop') == null) { //jika cookie tidak ada
        if (ses != 0) { //jika session login tidak ada maka muncul pop up login
          if (verif == 0) { //jika akun terverifikasi maka muncul adds modal
            if ($.cookie('verif') == null) {
              $('#addsModal').modal('show');
              $.cookie('verif', 2);
            }
          }
        }else{ //jika tidak ada
          $('#loginModal').modal('show');
          $.cookie('pop', 2);
        }
      }else{
        if (ses != 0) { //jika session login tidak ada maka muncul pop up login
          if (verif == 0) { //jika akun terverifikasi maka muncul adds modal
            if ($.cookie('verif') == null) {
              $('#addsModal').modal('show');
              $.cookie('verif', 2);
            }
          }
        }
      }
      // (ses != null)? $('#loginModal').modal('show') : $('#loginModal').modal('hide');
    });
    // conversi rupiah
    function rupiahh() {
      const hrg = $('#harga').val();
        var number_string = hrg.replace(/[^,\d]/g, '').toString(),
        split       = number_string.split(','),
        sisa        = split[0].length % 3,
        rupiah        = split[0].substr(0, sisa),
        ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
        if (ribuan) {
          separator = sisa? '.' : '';
          rupiah += separator + ribuan.join('.');
        }

      $('#harga').val(rupiah);
    }
    function loginshow() {
        $('#loginModal').modal('show');
    }
    function regshow() {
        $('#registerModal').modal('show');
        $('#loginModal').modal('hide');
    }
    // function untuk ambil kabupaten
    function tested() {
      $('#kecamatan').empty();
      var e = document.getElementById("kabupaten");
      var id = e.value;
      url = '<?php echo site_url("Konsumen/Produk/getKecamatan/'+id+'") ?>';
      console.log(url);
      $.ajax({
          url : url,
          method : 'POST',
          data : {},
          contentType : false,
          cache : false,
          processData : false,
          dataType : 'JSON',
          success : function (data) {
            var result = Object.assign({}, data.data);
              $('#kecamatan').append('<option value="">Pilih Kecamatan Disini</option>');
            for (var i = 0; i < data.data.length; i++) {
              $('#input_kecamatan').remove();
              $('#kecamatan').show();
              $('#kecamatan').append('<option value="'+data.data[i].kecamatan+'">'+data.data[i].name+'</option>');
              $('#kecamatan').change();
            }
          },
          error : err=>{}
        });
    }
    $('#formLogin').on('submit', function(e) {
        e.preventDefault();
        $('#btnLogin').text('Authentikasi...');
        $('#btnLogin').attr('disabled', true);
        if ($('[name = "username"]').val().length != 0) {
            if ($('[name = "password"]').val().length != 0) {
                url = '<?php echo site_url('Konsumen/Auth/verifLogin') ?>';
                $.ajax({
                  url : url,
                  method : 'POST',
                  data : new FormData(this),
                  contentType : false,
                  cache : false,
                  processData : false,
                  dataType : 'JSON',
                  success : function (data) {
                    if (data.status) {
                        successNotif(data.message, data.page);
                    } else {
                        failNotif('login', data.message);
                    }
                  },
                  error : err=>{
                    failNotif('login', 'Authentikasi gagal!');
                  }
                });
            } else {
                failNotif('login', 'Password tidak boleh kosong!');
            }
        } else {
            failNotif('login', 'Username tidak boleh kosong!');
        }
    });
    // register
    $('#formRegister').on('submit', function(e) {
        e.preventDefault();
        $('#btnRegister').text('Authentikasi...');
        $('#btnRegister').attr('disabled', true);
        if ($('[name = "reg_username"]').val().length != 0) {
            if ($('[name = "reg_password"]').val().length != 0) {
                url = '<?php echo site_url('Konsumen/Auth/register') ?>';
                $.ajax({
                  url : url,
                  method : 'POST',
                  data : new FormData(this),
                  contentType : false,
                  cache : false,
                  processData : false,
                  dataType : 'JSON',
                  success : function (data) {
                    if (data.status) {
                        successNotif(data.message, data.page);
                    } else {
                        failNotif('register', data.message);
                    }
                  },
                  error : err=>{
                    failNotif('register', 'Authentikasi gagal!');
                  }
                });
            } else {
                failNotif('register', 'Password tidak boleh kosong!');
            }
        } else {
            failNotif('register', 'Username tidak boleh kosong!');
        }
    });
    $('#btnLogout').on('click', function() {
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
      })
      swalWithBootstrapButtons.fire({
        title: 'Apakah kamu yakin akan keluar?',
        //text: "Kamu akan kehilangan data ini setelah dihapus!",
        icon: false,
        showCancelButton: true,
        confirmButtonText: 'keluar!',
        cancelButtonText: 'Kembali',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            url : "<?php echo site_url('Konsumen/Auth/logout')?>",
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
              successNotif(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              failNotif('Gagal Logout!');
            }
          });
        }
      })
    });
    function successNotif(data, page) {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: data,
        showConfirmButton: false,
        timer: 2000,
      }).then(function() {
        document.location.href='<?php echo site_url('') ?>';
      });
    }
    function failNotif(method = null, titles) {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: titles,
        showConfirmButton: false,
        timer: 3000,
      }).then(function() {
        if (method == 'login') {
          $('#btnLogin').text('Login');
          $('#btnLogin').attr('disabled', false);
        }else{
          $('#btnRegister').text('Login');
          $('#btnRegister').attr('disabled', false);
        }
      });
    }
    </script>
    <!--End of Tawk.to Script-->

    <!-- ========== JAVASCRIPT ========== -->
    <script src="<?= site_url('assets/admin/') ?>assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4NfvktSJrrnmUz9hTTfEJcCmHC-PRBpc"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jquery.smoothState.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/moment.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/morphext.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/wow.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/owl.carousel.thumbs.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/countUp.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jquery.countdown.min.js"></script>
    <!-- Slick JS -->
    <script type="text/javascript" src="<?= site_url('assets/main/slick/') ?>slick/slick.min.js"></script>
    <!-- <script type="text/javascript" src="<?= site_url('assets/main/') ?>js/jquery-ui/jquery-ui.js"></script> -->
    
    <!-- ========== REVOLUTION SLIDER ========== -->
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets/main/') ?>revolution/js/extensions/revolution.extension.video.min.js"></script>
    
</body>
</html>