<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//main
$route['default_controller']                                    = 'Home';
$route['']                                                  = 'Home';
$route['main']                                            = 'Home';
// auth
$route['login']                                           = 'Konsumen/Auth';
$route['login/verif']                                      = 'Konsumen/Auth/veriflogin';
$route['register']                                          = 'Konsumen/Auth/register';
$route['register/save']                                      = 'Konsumen/Auth/createuser';
// property
$route['property']                                        = 'Konsumen/Produk';
$route['property/(:any)']                                = 'Konsumen/Produk';
$route['property/detail_property/(:any)']                 = 'Konsumen/ProdukDetail/index/$1';
// cari property
$route['cari_property']                                        = 'Konsumen/Produk/cariProduk';
$route['filter_property']                                = 'Konsumen/Produk/cariProdukFilter';
$route['property/cari/(:any)']                            = 'Konsumen/Produk/cariProduk2/$1';
// $route['property/cari_property_dev/(:any)']             = 'Konsumen/Pengembang/cariProduk2/$1';

$route['cari_kpr']                  					= 'Konsumen/Kpr';
$route['berita']                  						= 'Konsumen/Berita';
$route['promo']                   						= 'Konsumen/Promo';
$route['profile_pengembang/(:any)']       				= 'Konsumen/Pengembang/index/$1';

$route['berita']                                          = 'Konsumen/Berita';
$route['promo']                                           = 'Konsumen/Promo';
$route['profile_pengembang/(:any)']                       = 'Konsumen/Pengembang/index/$1';


$route['profile']                                  = 'Konsumen/ProfileUser';

$route['kontak']                                          = 'Konsumen/Kontak';
$route['about_us']                                        = 'Konsumen/TentangKami';
$route['blog']                                            = 'Konsumen/Blog';
$route['galeri']                                          = 'Konsumen/Galeri';
$route['panduan_produk']                                  = 'Konsumen/PanduanProduk';
$route['panduan_referensi']                               = 'Konsumen/PanduanReferensi';


//admin

$route['admin']       				 						= 'Admin/Home';
$route['admin/dashboard']        						= 'Admin/Home';
$route['admin/login']            						= 'Admin/Auth';
//data petugas
$route['admin/user/admin']       						= 'Admin/Admin';
$route['admin/user/petugas']     						= 'Admin/Petugas';
$route['admin/user/petugas/add']						= 'Admin/Petugas/tambahPetugas';
$route['admin/user/petugas/edit/(:any)'] 				= 'Admin/Petugas/editPetugas/$1';
//data pengembang
$route['admin/user/pengembang']     					= 'Admin/Pengembang';
$route['admin/user/pengembang/add'] 					= 'Admin/Pengembang/tambahPengembang';
$route['admin/user/pengembang/edit/(:any)'] 			= 'Admin/Pengembang/editPengembang/$1';
$route['admin/user/pengembang/editpw/(:any)'] 			= 'Admin/Pengembang/editPassword/$1';
//data proyek
$route['admin/data/project']     						= 'Admin/Project';
$route['admin/data/project/add'] 						= 'Admin/Project/tambahProject';
$route['admin/data/project/edit/(:any)'] 				= 'Admin/Project/editProject/$1';
//data produk
$route['admin/data/produk']     						= 'Admin/Produk';
$route['admin/data/produk/add'] 						= 'Admin/Produk/tambahProduk';
$route['admin/data/produk/edit/(:any)'] 				= 'Admin/Produk/editProduk/$1';
$route['admin/data/produk/detailproduk/(:any)'] 		= 'Admin/Produk/detailProduk/$1';
// data promo
$route['admin/promo'] 	    							= 'Admin/Promo';
$route['admin/promo/add'] 	    						= 'Admin/Promo/tambahPromo';
$route['admin/promo/edit/(:any)'] 	    				= 'Admin/Promo/editPromo/$1';

// data berita property

// pengaturan
$route['admin/pengaturan/profile/(:any)'] 				= 'Admin/Profile/index/$1';
$route['admin/pengaturan/faqs']        					= 'Admin/Pengaturan/faqs';


// data berita property
$route['admin/berita']                                   = 'Admin/Berita';
$route['admin/berita/add']                               = 'Admin/Berita/tambahBerita';

//errors
$route['404_override']                                     = 'Admin/Eror';
$route['admin/404_override']                              = 'Admin/Eror';
$route['translate_uri_dashes']                          = FALSE;
