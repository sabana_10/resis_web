<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lib_estetik
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function getRupiah($dataMentah)
    {
        return "Rp " . number_format($dataMentah, 2, ",", ".");
    }

    public function getRupiah2($dataMentah)
    {
        return "Rp " . number_format($dataMentah, 0, "", ".");
    }

    public function getRupiahWithSensor($dataMentah)
    {
    	$sensor = 2;
    	$afterIndex = 1;

        $hg = number_format($dataMentah, 0, "", ".");
    	// get sensor
    	if (strlen($dataMentah) <= 9 ) {
	    	$depan = substr($hg, 1, 2);
	    	$belakang = substr($hg, 3);
    	} else {
	    	$depan = substr($hg, 2, 2);
	    	$belakang = substr($hg, 4);
    	}
    	
    	// get harga belakang sensor
    	// pecah harga
    	$data = explode($depan, $hg);
    	// gather harga
    	$hargafinal = $data[0].'xx'.$belakang;

    	return "Rp ".$hargafinal;
    }

    public function pecahHarga($nominal)
	{
		return str_replace(".", "", $nominal);
	}

	public function getHarga($nominal)
	{
		return number_format($nominal, 0, "", ".");
	}	

    public function getTanggalIndonesia($dataMentah)
	{
	    // $dataMentah
		$tgl   = substr($dataMentah, -2);
		$bulan = substr($dataMentah, 5, 2);
		$tahun = substr($dataMentah, 0, 4);

		$bln = [
			"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];

		if ($bulan == 1) {
			$bulan = $bln[0];
		    $tglIndonesia = $tgl."-".$bulan."-".$tahun;
		} else if ($bulan == 2) {
			$bulan = $bln[1];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		} else if ($bulan == 3) {
			$bulan = $bln[2];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 4) {
			$bulan = $bln[3];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 5) {
			$bulan = $bln[4];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 6) {
			$bulan = $bln[5];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 7) {
			$bulan = $bln[6];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 8) {
			$bulan = $bln[7];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 9) {
			$bulan = $bln[8];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 10) {
			$bulan = $bln[9];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 11) {
			$bulan = $bln[10];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}
		 else if ($bulan == 12) {
			$bulan = $bln[11];
		    $tglIndonesia = $tgl." ".$bulan." ".$tahun;
		}

	    // $tglIndonesia = $tgl."-".$bulan."-".$tahun;

	    return $tglIndonesia;
	}
}

/* End of file Lib_estetik.php */
/* Location: ./application/libraries/Lib_estetik.php */
