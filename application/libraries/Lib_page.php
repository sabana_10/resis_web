<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lib_page
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function page($content = null, $data = null)
	{
		$datas = [
			'loading' => $this->ci->load->view('main/component/loading', $data, true),
			'header'  => $this->ci->load->view('main/component/header', $data, true),
			'content' => $this->ci->load->view('main/'.$content, $data, true),
			'footer'  => $this->ci->load->view('main/component/footer', $data, true),
		];

		return $this->ci->load->view('index', $datas);
	}

	public function page_admin($content = null, $data = null)
	{
		$datas = [
			'header'          => $this->ci->load->view('admin/component/header', $data, true),
			'sidebar'         => $this->ci->load->view('admin/component/sidebar', $data, true),
			'sidebar_setting' => $this->ci->load->view('admin/component/sidebar_setting', $data, true),
			'content'         => $this->ci->load->view('admin/'.$content, $data, true),
			'footer'          => $this->ci->load->view('admin/component/footer', $data, true),
		];

		return $this->ci->load->view('admin/index', $datas);
	}

}

/* End of file Lib_page.php */
/* Location: ./application/libraries/Lib_page.php */
