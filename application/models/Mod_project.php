<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_project extends CI_Model {

	public function mengambil()
	{
		$this->db->select('pr.id');
		$this->db->select('pb.nama AS `nama_pengembang`');
		$this->db->select('pr.nama');
		$this->db->select('pr.deskripsi');
		$this->db->select('pr.lokasi');
		$this->db->select('pr.fasilitas_umum');
		$this->db->select('pr.type');
		$this->db->select('pr.tahun_proyek_dimuali');
		$this->db->select('pr.estimasi_proyek_selesai');
		$this->db->select('pr.tanggal_input');
		$this->db->select('pr.tanggal_edit');
		$this->db->from('proyek AS pr');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');

		return $this->db->get();
	}

	public function mengambilById($id)
	{
		$this->db->select('pr.id');
		$this->db->select('pr.id_pengembang');
		$this->db->select('pb.nama AS `nama_pengembang`');
		$this->db->select('pr.nama');
		$this->db->select('pr.deskripsi');
		$this->db->select('pr.lokasi');
		$this->db->select('prov.id AS id_provinsi');
		$this->db->select('prov.name AS nama_provinsi');
		$this->db->select('reg.id AS id_kabupaten');
		$this->db->select('reg.name AS nama_kabupaten');
		$this->db->select('dis.id AS id_kecamatan');
		$this->db->select('dis.name AS nama_kecamatan');
		$this->db->select('vl.id AS id_desa');
		$this->db->select('vl.name AS nama_desa');
		$this->db->select('pr.fasilitas_umum');
		$this->db->select('pr.type');
		$this->db->select('pr.tahun_proyek_dimuali');
		$this->db->select('pr.estimasi_proyek_selesai');
		$this->db->select('pr.tanggal_input');
		$this->db->select('pr.tanggal_edit');
		$this->db->from('proyek AS pr');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');
		$this->db->join('provinces AS prov', 'prov.id = pr.provinsi', 'left');
		$this->db->join('regencies AS reg', 'reg.id = pr.kabupaten', 'left');
		$this->db->join('districts AS dis', 'dis.id = pr.kecamatan', 'left');
		$this->db->join('villages AS vl', 'vl.id = pr.desa', 'left');
		$this->db->where('md5(pr.id)', $id);

		return $this->db->get();
	}

	public function mengambilById2($id)
	{
		$this->db->select('pr.id');
		$this->db->select('pr.id_pengembang');
		$this->db->select('pb.nama AS `nama_pengembang`');
		$this->db->select('pr.nama');
		$this->db->select('pr.deskripsi');
		$this->db->select('pr.lokasi');
		$this->db->select('pr.fasilitas_umum');
		$this->db->select('pr.type');
		$this->db->select('pr.tahun_proyek_dimuali');
		$this->db->select('pr.estimasi_proyek_selesai');
		$this->db->select('pr.tanggal_input');
		$this->db->select('pr.tanggal_edit');
		$this->db->from('proyek AS pr');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');
		$this->db->where('pr.id_pengembang', $id);

		return $this->db->get();
	}

}

/* End of file Mod_project.php */
/* Location: ./application/models/Mod_project.php */