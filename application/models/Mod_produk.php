<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_produk extends CI_Model {

	public function mengambil($limit = null)
	{
		$this->db->select('pd.id');
		$this->db->select('pb.id AS id_pengembang');
		$this->db->select('pb.nama AS nama_pengembang');
		$this->db->select('pr.nama AS nama_proyek');
		$this->db->select('pd.nama');
		$this->db->select('pd.deskripsi');
		$this->db->select('pd.luas_tanah');
		$this->db->select('pd.luas_bangunan');
		$this->db->select('pd.kamar_tidur');
		$this->db->select('pd.kamar_mandi');
		$this->db->select('pd.ruang_tamu');
		$this->db->select('pd.dapur');
		$this->db->select('pd.harga');
		$this->db->from('produk AS pd');
		$this->db->join('proyek AS pr', 'pr.id = pd.id_proyek', 'left');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');
		$this->db->order_by('pd.id', 'desc');

		if ($limit != null) {
			$this->db->limit($limit);
		}

		return $this->db->get();
	}

	public function mengambilPagination($tipe = null, $id = null, $subsidi = null, $min_harga = null, $max_harga = null, $kabupaten = null, $kecamatan = null, $luas_tanah = null, $luas_bangunan = null)
	{
		$this->load->library('pagination');
		
		if ($tipe == 'cariFilter') {
			// $config['total_rows']      = $this->mpd->cariProduk($subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan, $luas_tanah, $luas_bangunan)->num_rows();
			$config['total_rows']      = $this->mpd->cariProduk($subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan, null, null)->num_rows();
		}else if($tipe == 'cari'){
			$config['total_rows']      = $this->mpd->cari($id)->num_rows();
		}else if($tipe == 'cariAuto'){
			$config['total_rows']      = $this->mod_sb->mengambil('vwhalamanutama', ['md5(id)'=>$id])->num_rows();
		}else{
			$config['total_rows']      = $this->mod_sb->mengambil('vwhalamanutama')->num_rows();
		}
			
		$config['base_url']        = site_url('property');
		$config['per_page']        = 18;
		$config['uri_segment']     = 2;
		$config['num_links']       = 3;
		
		$config['full_tag_open']   = '<ul class="pagination mt50 mb0">';
		$config['full_tag_close']  = '</ul>';
		
		$config['first_link']      = 'First';
		$config['first_tag_open']  = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link']       = 'Last';
		$config['last_tag_open']   = '<li>';
		$config['last_tag_close']  = '</li>';
		
		$config['next_link']       = '<i class="fa fa-angle-right"></i>';
		$config['next_tag_open']   = '<li class="next_pagination">';
		$config['next_tag_close']  = '</li>';
		
		$config['prev_link']       = '<i class="fa fa-angle-left"></i>';
		$config['prev_tag_open']   = '<li class="prev_pagination">';
		$config['prev_tag_close']  = '</li>';
		
		$config['cur_tag_open']    = '<li class="active"><a href="'.site_url('property').'">';
		$config['cur_tag_close']   = '</a></li>';
		
		$config['num_tag_open']    = '<li>';
		$config['num_tag_close']   = '</li>';
		
		$this->pagination->initialize($config);
		
		$page                      = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
		$data['limit']             = $config['per_page'];
		$data['total_rows']        = $config['total_rows'];
		$data['pagination']        = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas
		if ($tipe == 'cariFilter') {
			// $data['produk']        		= $this->mpd->cariProduk($subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan, $luas_tanah, $luas_bangunan, $config['per_page'], $page)->result();
			$data['produk']        		= $this->mpd->cariProduk($subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan, null, null, $config['per_page'], $page)->result();
		}else if($tipe == 'cari'){
			$data['produk']      		= $this->mpd->cari($id, $config['per_page'], $page)->result();
		}else if($tipe == 'cariAuto'){
			$data['produk']      		= $this->mod_sb->mengambil('vwhalamanutama', ['md5(id)'=>$id], $config['per_page'], $page)->result();
		}else{
			$data['produk']           	= $this->mod_sb->mengambil('vwhalamanutama', null, $config['per_page'], $page)->result();
		}
		
		// var_dump($page);
		// die;
		return $data;
	}

	public function cari($like, $limit = null, $offset = null)
	{
		$this->db->select('*');
		$this->db->from('vwhalamanutama');
		$this->db->like('nama_produk', $like, 'BOTH');
		$this->db->or_like('nama', $like, 'BOTH');
		$this->db->limit($limit, $offset);

		return $this->db->get();
	}

	public function cari2($like, $limit = null, $offset = null)
	{
		$this->db->select('*');
		$this->db->from('vwhalamanutama');
		$this->db->like('nama_produk', $like, 'BOTH');
		$this->db->limit($limit, $offset);

		return $this->db->get();
	}

	public function cariProduk($subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan = null, $luas_tanah, $luas_bangunan, $limit = null, $offset = null)
	{
		$this->db->select('vh.*');
		$this->db->from('vwhalamanutama AS vh');
		$this->db->where('vh.subsidi', $subsidi);
		if ($kabupaten                             != null) { $this->db->where('vh.kabupaten', $kabupaten);	}
		if ($kecamatan                             != null) { $this->db->where('vh.kecamatan', $kecamatan);	}
		if ($min_harga <= $max_harga AND $min_harga != null) { $this->db->where('vh.harga >= ', $min_harga); }
		if ($max_harga >= $min_harga AND $max_harga != null) { $this->db->where('vh.harga <= ', $max_harga); }
		// if ($luas_tanah                            != null) { $this->db->or_where('vh.luas_tanah', $luas_tanah);	}
		// if ($luas_bangunan                         != null) { $this->db->or_where('vh.luas_bangunan', $luas_bangunan);	}
		if ($limit                                 != null) { $this->db->limit($limit, $offset); }

		return $this->db->get();

	}

	public function autoCompleteProperty($text)
	{
		$this->db->select('vh.*');
		$this->db->from('vwhalamanutama AS vh');
		$this->db->like('vh.nama_produk', $text, 'BOTH');
		// $this->db->or_like('vh.nama_proyek', $text, 'BOTH');
		$this->db->or_like('vh.nama', $text, 'BOTH');
		$this->db->order_by('vh.nama_produk', 'asc');

		return $this->db->get();
	}

	public function autoCompleteProperty2($text, $where)
	{
		$this->db->select('vh.*');
		$this->db->from('vwhalamanutama AS vh');
		$this->db->where('vh.nama_produk', $where);
		$this->db->like('vh.nama_produk', $text, 'BOTH');
		$this->db->order_by('vh.nama_produk', 'asc');

		return $this->db->get();
	}

	public function mengambil2($id)
	{
		$this->db->select('pd.id');
		$this->db->select('pb.id AS id_pengembang');
		$this->db->select('pb.nama AS nama_pengembang');
		$this->db->select('pr.nama AS nama_proyek');
		$this->db->select('pr.deskripsi AS deskripsi_bangunan');
		$this->db->select('pr.fasilitas_umum');
		$this->db->select('pr.type AS tipe_bangunan');
		$this->db->select('pd.nama');
		$this->db->select('pd.deskripsi');
		$this->db->select('pd.luas_tanah');
		$this->db->select('pd.luas_bangunan');
		$this->db->select('pd.kamar_tidur');
		$this->db->select('pd.kamar_mandi');
		$this->db->select('pd.ruang_tamu');
		$this->db->select('pd.dapur');
		$this->db->select('pd.harga');
		$this->db->select('pr.lokasi');
		$this->db->select('prov.name AS provinsi');
		$this->db->select('reg.name AS kabupaten');
		$this->db->select('dis.name AS kecamatan');
		$this->db->from('produk AS pd');
		$this->db->join('proyek AS pr', 'pr.id = pd.id_proyek', 'left');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');
		$this->db->join('provinces AS prov', 'prov.id = pr.provinsi', 'left');
		$this->db->join('regencies AS reg', 'reg.id = pr.kabupaten', 'left');
		$this->db->join('districts AS dis', 'dis.id = pr.kecamatan', 'left');
		$this->db->where('md5(pd.id)', $id);

		return $this->db->get();
	}

	public function mengambil2_1($id)
	{
		$this->db->select('pd.id');
		$this->db->select('pb.id AS id_pengembang');
		$this->db->select('pb.nama AS nama_pengembang');
		$this->db->select('pr.nama AS nama_proyek');
		$this->db->select('pd.nama');
		$this->db->select('pd.deskripsi');
		$this->db->select('pd.luas_tanah');
		$this->db->select('pd.luas_bangunan');
		$this->db->select('pd.kamar_tidur');
		$this->db->select('pd.kamar_mandi');
		$this->db->select('pd.ruang_tamu');
		$this->db->select('pd.dapur');
		$this->db->select('pd.harga');
		$this->db->from('produk AS pd');
		$this->db->join('proyek AS pr', 'pr.id = pd.id_proyek', 'left');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');
		$this->db->where('pr.id_pengembang', $id);

		return $this->db->get();
	}

	public function mengambilProyekPengembang($id = null)
	{
	    $this->db->select('pr.id');
		$this->db->select('pr.nama AS nama_proyek');
		$this->db->select('pb.nama AS nama_pengembang');
		$this->db->from('proyek AS pr');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');

		if ($id != null) {
			$this->db->where('pr.id_pengembang', $id);
		}

		return $this->db->get();
	}

	public function mengambilProyekPengembang2($id)
	{
	    $this->db->select('pr.id');
		$this->db->select('pr.nama AS nama_proyek');
		$this->db->select('pb.nama AS nama_pengembang');
		$this->db->from('produk as pd');
		$this->db->join('proyek AS pr', 'pr.id = pd.id_proyek', 'left');
		$this->db->join('pengembang AS pb', 'pb.id = pr.id_pengembang', 'left');
		$this->db->where('pd.id', $id);

		return $this->db->get();
	}


}

/* End of file Mod_produk.php */
/* Location: ./application/models/Mod_produk.php */