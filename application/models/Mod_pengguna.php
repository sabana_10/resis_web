<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_pengguna extends CI_Model
{

	public function mengambil($username = null)
	{
		$this->db->select('p.id');
		$this->db->select('p.nama');
		$this->db->select('p.username');
		$this->db->select('p.password');
		$this->db->select('p.foto');
		$this->db->select('p.no_tlp');
		$this->db->select('p.KTP');
		$this->db->select('p.Penghasilan');
		$this->db->select('p.alamat');
		$this->db->select('pr.id AS id_provinsi');
		$this->db->select('rg.id AS id_kabupaten');
		$this->db->select('ds.id AS id_kecamatan');
		$this->db->select('pr.name AS provinsi');
		$this->db->select('rg.name AS kabupaten');
		$this->db->select('ds.name AS kecamatan');
		// $this->db->select('vl.name AS desa');
		$this->db->from('pengguna AS p');
		$this->db->join('provinces AS pr', 'pr.id = p.provinsi', 'left');
		$this->db->join('regencies AS rg', 'rg.id = p.kabupaten', 'left');
		$this->db->join('districts AS ds', 'ds.id = p.kecamatan', 'left');
		// $this->db->join('villages AS vl', 'vl.id = p.desa', 'left');
		$this->db->where('p.username', $username);

		return $this->db->get();
	}
}

/* End of file Mod_pengguna.php */
/* Location: ./application/models/Mod_pengguna.php */