<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data = [
			'judul'          => 'Resis',
			'getDataPromo'   => $this->mpd->mengambil(4)->result(),
			'getDataProduk'  => $this->mod_sb->mengambil('vwhalamanutama', null, 8)->result(),
			'getVirtualTour' => $this->mod_sb->mengambil('vwhalamanutama', null, 4)->result(),
		];

		// $this->googlelogin(); tes1

   //  	$login_button = '';
   //      if(!$this->session->userdata('access_token')){
   //          $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="'.base_url().'sign-with-google.png" /></a>';
   //          $data['login_button'] = $login_button;
			// $this->lp->page('home', $data);
   //      }else{
			$this->lp->page('home', $data);
        // }

	}

	public function googlelogin()
	{
		// require "./assets/main/google-api-php-client/vendor/autoload.php";
		// include_once "./vendor/autoload.php";
		// var_dump(APPPATH. "~/vendor/autoload.php");
		include_once "./vendor/autoload.php";
        $google_client = new Google_Client();
        $google_client->setClientId('626627450147-5pp8ee0af75qciv47mbfv4tcqhdppco3.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('26-ED7_PS6D5e_3gfm9mkuOe'); //Define your Client Secret Key
        $google_client->setRedirectUri('http://resis/'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        if(isset($_GET["code"]))
        {
        $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

            if(!isset($token["error"]))
            {
                $google_client->setAccessToken($token['access_token']);
                // set token ke session
                $this->session->set_userdata('access_token', $token['access_token']);

                $google_service = new Google_Service_Oauth2($google_client);
                $data = $google_service->userinfo->get();

                if($this->mod_sb->mengubah($this->table, $data['id'])){
	                //update data
	                $user_data = array(
						'username' => $data['email'],
						'password' => password_hash($data['password'], PASSWORD_DEFAULT),
						'nama'     => $data['given_name'],
						'foto'     => $data['picture'],
	                );

	                $this->mod_sb->mengubah($this->table, $data['id'], $user_data);
                }else{
	                //insert data
	                $user_data = array(
						'id'       => $data['id'],
						'username' => $data['email'],
						'password' => password_hash($data['password'], PASSWORD_DEFAULT),
						'nama'     => $data['given_name'],
						'foto'     => $data['picture'],
	                );

					$this->mod_sb->menambah($this->table, $data);
                }
                $this->session->set_userdata('user_data', $user_data);
            }
        }
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */