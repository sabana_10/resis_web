<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promo extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
	}

	public $table = 'promo';

	public function index()
	{
		$data = [
			'getDataPromo' => $this->mod_sb->mengambilOrderBy($this->table, null, 'id', 'desc')->result(),
		];

		$this->lp->page_admin('promo/view_promo', $data);
	}

	public function tambahPromo()
	{
		$this->lp->page_admin('promo/add_promo');
	}

	public function editPromo($id)
	{
		$q = $this->mod_sb->mengambil($this->table, ['md5(id)' => $id])->row();
		$data = [
			'getDataPromo'  => $q,
		];
		$this->lp->page_admin('promo/edit_promo', $data);
	}

	public function saveWithAjax()
	{
		$post = $this->input->post();
		// var_dump($post);die;
		$id = $post['id'];
		//config photo
		$config['upload_path']   = './assets/uploads/photo_promo/';
		$config['allowed_types'] = '*';
		$config['max_size']      = 0;
		$config['max_width']     = 0;
		$config['max_height']    = 0;
		$config['file_name']     = $post['judul'];
		$config['overwrite']     = true;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);


		//logik add and update
		if ($id != null or $id != '') {
			if (!$this->upload->do_upload('foto')) {
				$data = [
					'title'         => $post['judul'],
					'deskripsi'     => $post['deskripsi'],
					'tanggal_mulai' => $post['tanggal_mulai'],
					'tanggal_akhir' => $post['tanggal_akhir'],
					'is_active'     => 1,
					'id_petugas'    => $this->session->userdata('id'),

				];

				$q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
					'error'   => $this->upload->display_errors(),
				]);
			} else {
				$_data     = array('upload_data' => $this->upload->data());
				$foto      = $_data['upload_data']['file_name'];
				$thumbnail = $_data['upload_data']['file_name'];

				$this->addThumbnail($thumbnail);

				$data = [
					'title'         => $post['judul'],
					'deskripsi'     => $post['deskripsi'],
					'tanggal_mulai' => $post['tanggal_mulai'],
					'tanggal_akhir' => $post['tanggal_akhir'],
					'is_active'     => 1,
					'id_petugas'    => $this->session->userdata('id'),
					'foto'          => $foto,
					'thumbnail'     => $thumbnail,
				];

				$q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
				]);
			}
		} else {
			if (!$this->upload->do_upload('foto')) {
				$data = [
					'title'         => $post['judul'],
					'deskripsi'     => $post['deskripsi'],
					'tanggal_mulai' => $post['tanggal_mulai'],
					'tanggal_akhir' => $post['tanggal_akhir'],
					'is_active'     => 1,
					'id_petugas'    => $this->session->userdata('id'),

				];

				$q = $this->mod_sb->menambah($this->table, $data);
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil menambah data!',
					// 'error'   => $this->upload->display_errors()
				]);
			} else {
				$_data     = array('upload_data' => $this->upload->data());
				$foto      = $_data['upload_data']['file_name'];
				$thumbnail = $_data['upload_data']['file_name'];

				$this->addThumbnail($thumbnail);

				$data = [
					'title'         => $post['judul'],
					'deskripsi'     => $post['deskripsi'],
					'tanggal_mulai' => $post['tanggal_mulai'],
					'tanggal_akhir' => $post['tanggal_akhir'],
					'is_active'     => 1,
					'id_petugas'    => $this->session->userdata('id'),
					'foto'          => $foto,
					'thumbnail'     => $thumbnail,
				];

				$q = $this->mod_sb->menambah($this->table, $data);
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil menambah data!',
				]);
			}
		}
	}

	public function addThumbnail($file_name)
	{
		//config resize
		$config = [
			// thumbnail Large
			[
				'image_library'  => 'GD2',
				'source_image'   => './assets/uploads/photo_promo/' . $file_name,
				'maintain_ratio' => FALSE,
				'width'          => '865',
				'height'         => '500',
				'new_image'      => './assets/uploads/photo_promo/thumbnails/large/' . $file_name
			],
			// thumbnail medium
			[
				'image_library'  => 'GD2',
				'source_image'   => './assets/uploads/photo_promo/' . $file_name,
				'maintain_ratio' => FALSE,
				'width'          => '370',
				'height'         => '232',
				'new_image'      => './assets/uploads/photo_promo/thumbnails/medium/' . $file_name
			]
		];
		// library
		$this->load->library('image_lib', $config[0]);
		// pecah
		foreach ($config as $item) {
			// inisialisasi image
			$this->image_lib->initialize($item);
			// jika tidak di resize
			if (!$this->image_lib->resize()) {
				return false;
			}
			$this->image_lib->clear();
		}
	}

	public function deleteWithAjax($id)
	{
		//getDataPhoto
		$query = $this->mod_sb->mengambil($this->table, ['md5(id)' => $id])->row();
		// logik hapus photo
		if ($query != null) {
			$tes1 = unlink('assets/uploads/photo_promo/' . $query->foto);
			$tes2 = unlink('assets/uploads/photo_promo/thumbnails/large/' . $query->thumbnail);
			$tes3 = unlink('assets/uploads/photo_promo/thumbnails/medium/' . $query->thumbnail);
		}
		//delete data
		$q = $this->mod_sb->menghapus($this->table, ['md5(id)' => $id]);
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil menghapus data!',
		]);
	}
}

/* End of file Promo.php */
/* Location: ./application/controllers/Admin/Promo.php */