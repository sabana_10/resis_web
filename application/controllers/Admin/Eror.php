<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eror extends CI_Controller {

	public function index()
	{
		$this->lp->page_admin('errors/v_error_404');
	}

}

/* End of file Errors.php */
/* Location: ./application/controllers/Admin/Errors.php */