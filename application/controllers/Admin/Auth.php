<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	private $table = 'petugas';

	public function index()
	{
		// redirect('Auth/login','refresh');
		$this->load->view('admin/auth/login');
	}

	public function login()
	{
		$post  = $this->input->post();
		$un    = $post['username'];
		$pw    = $post['password'];
		
		$q_cek = $this->mod_sb->mengambil($this->table, ['username'=>$un])->row();
		$q_cek2 = $this->mod_sb->mengambil('pengembang', ['username'=>$un])->row();

		if ($q_cek) {
			if (password_verify($pw, $q_cek->password)) {
				//data for session
				$data = [
					'id'        => $q_cek->id,
					'username'  => $q_cek->username,
					'hak_akses' => $q_cek->hak_akases,
					'role'      => '',
				];

				//set session for hak akses
				$this->session->set_userdata($data);

				echo json_encode([
					'status'    => true,
					'message'   => 'Berhasil Login!',
					'dataLogin' => $data,
					'page'      => 'admin'
		    	]);

			} else {
				echo json_encode([
					'status'  => false,
					'message' => 'Password gagal! silahkan coba lagi.'
		    	]);
			}
		}else if($q_cek2){
			if (password_verify($pw, $q_cek2->password)) {
				//data for session
				$data = [
					'id'        => $q_cek2->id,
					'nama'      => $q_cek2->nama,
					'username'  => $q_cek2->username,
					'hak_akses' => 'pengembang',
					'role'      => '',
				];

				//set session for hak akses
				$this->session->set_userdata($data);

				echo json_encode([
					'status'    => true,
					'message'   => 'Berhasil Login!',
					'dataLogin' => $data,
					'page'      => 'pengembang'
		    	]);

			} else {
				echo json_encode([
					'status'  => false,
					'message' => 'Password gagal! silahkan coba lagi.'
		    	]);
			}
		}else {
			echo json_encode([
				'status'  => false,
				'message' => 'Akun ini tidak terdaftar!'
	    	]);
		}
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		echo json_encode([
			'status'    => true,
			'message'   => 'Berhasil Logout!',
    	]);
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Admin/Auth.php */