<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Berita extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in_admin();
        wates_petugas();
    }

    public $table = 'berita';

    public function index()
    {
        $data = [
            'getDataBerita' => $this->mod_sb->mengambil($this->table)->result(),

        ];

        $this->lp->page_admin('berita/view_berita', $data);
    }
    public function tambahBerita()
    {
        $this->lp->page_admin('berita/add_berita');
    }

    public function editBerita($id)
    {
        $data = [
            'getDataBerita' => $this->mod_sb->mengambilOrderBy($this->table, ['md5(id)' => $id])->row(),
        ];

        $this->lp->page_admin('berita/edit_berita', $data);
    }

    public function saveWithAjax()
    {
        $post = $this->input->post();
        // var_dump($post);die;
        $id = $post['id'];
        //config photo
        $config['upload_path']   = './assets/uploads/photo_berita/';
        $config['allowed_types'] = '*';
        $config['max_size']      = 0;
        $config['max_width']     = 0;
        $config['max_height']    = 0;
        $config['file_name']     = $post['judul'];
        $config['overwrite']     = true;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);


        //logik add and update
        if ($id != null or $id != '') {
            if (!$this->upload->do_upload('gambar')) {
                $data = [
                    'judul'         => $post['judul'],
                    'berita'     => $post['berita'],
                    'create_date' => $post['create_date'],
                    'edit_date' => $post['edit_date'],
                    'id_penggguna'    => $this->session->userdata('id'),

                ];

                $q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
                echo json_encode([
                    'status'  => true,
                    'message' => 'Berhasil mengubah data!',
                    'error'   => $this->upload->display_errors(),
                ]);
            } else {
                $_data     = array('upload_data' => $this->upload->data());
                $gambar      = $_data['upload_data']['file_name'];
                $gambar_thumb = $_data['upload_data']['file_name'];

                $this->addThumbnail($gambar_thumb);

                $data = [
                    'judul'         => $post['judul'],
                    'berita'     => $post['berita'],
                    'create_date' => $post['create_date'],
                    'edit_date' => $post['edit_date'],
                    'id_penggguna'    => $this->session->userdata('id'),
                    'gambar'          => $gambar,
                    'gambar_thumb'     => $gambar_thumb,
                ];

                $q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
                echo json_encode([
                    'status'  => true,
                    'message' => 'Berhasil mengubah data!',
                ]);
            }
        } else {
            if (!$this->upload->do_upload('gambar')) {
                $data = [
                    'judul'         => $post['judul'],
                    'berita'     => $post['berita'],
                    'create_date' => $post['create_date'],
                    // 'edit_date' => $post['edit_date'],
                    'id_penggguna'    => $this->session->userdata('id'),

                ];

                $q = $this->mod_sb->menambah($this->table, $data);
                echo json_encode([
                    'status'  => true,
                    'message' => 'Berhasil menambah data!',
                    // 'error'   => $this->upload->display_errors()
                ]);
            } else {
                $_data     = array('upload_data' => $this->upload->data());
                $gambar      = $_data['upload_data']['file_name'];
                $gambar_thumb = $_data['upload_data']['file_name'];

                $this->addThumbnail($gambar_thumb);

                $data = [
                    'judul'         => $post['judul'],
                    'berita'     => $post['berita'],
                    'create_date' => $post['create_date'],
                    // 'edit_date' => $post['edit_date'],
                    'id_penggguna'    => $this->session->userdata('id'),
                    'gambar'          => $gambar,
                    'gambar_thumb'     => $gambar_thumb,
                ];

                $q = $this->mod_sb->menambah($this->table, $data);
                echo json_encode([
                    'status'  => true,
                    'message' => 'Berhasil menambah data!',
                ]);
            }
        }
    }

    public function addThumbnail($file_name)
    {
        //config resize
        $config = [
            // thumbnail Large
            [
                'image_library'  => 'GD2',
                'source_image'   => './assets/uploads/photo_berita/' . $file_name,
                'maintain_ratio' => FALSE,
                'width'          => '865',
                'height'         => '500',
                'new_image'      => './assets/uploads/photo_berita/thumbnails/large/' . $file_name
            ],
            // thumbnail medium
            [
                'image_library'  => 'GD2',
                'source_image'   => './assets/uploads/photo_berita/' . $file_name,
                'maintain_ratio' => FALSE,
                'width'          => '370',
                'height'         => '232',
                'new_image'      => './assets/uploads/photo_berita/thumbnails/medium/' . $file_name
            ]
        ];
        // library
        $this->load->library('image_lib', $config[0]);
        // pecah
        foreach ($config as $item) {
            // inisialisasi image
            $this->image_lib->initialize($item);
            // jika tidak di resize
            if (!$this->image_lib->resize()) {
                return false;
            }
            $this->image_lib->clear();
        }
    }


    public function deleteWithAjax($id)
    {
        $q = $this->mod_sb->menghapus($this->table, ['md5(id)' => $id]);
        echo json_encode([
            'status'  => true,
            'message' => 'Berhasil menghapus data!',
        ]);
    }
}
