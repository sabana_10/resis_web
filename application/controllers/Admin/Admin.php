<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
		wates_su();
	}

	public $table = 'petugas';

	public function index()
	{
		$data = [
			'getDataPetugas' => $this->mod_sb->mengambil($this->table, ['hak_akases'=>'admin'])->result(),
		];

		$this->lp->page_admin('petugas/view_petugas', $data);
	}

	public function tambahPetugas()
	{
	    $this->lp->page_admin('petugas/add_petugas');
	}

	public function editPetugas($id)
	{
		$data = [
			'getDataPetugas' => $this->mod_sb->mengambil($this->table, ['md5(id)'=>$id])->row(),
		];

	    $this->lp->page_admin('petugas/edit_petugas', $data);
	}

	public function saveWithAjax()
	{
	    $post = $this->input->post();

	    $id = $post['id'];
	    if ($id != null && $id != '') {
	    	$data = [
				'username'   => $post['username'],
				'password'   => password_hash($post['password'], PASSWORD_DEFAULT),
				'hak_akases' => $post['hak_akses']
	    	];

	    	$q = $this->mod_sb->mengubah($this->table, ['id'=>$id], $data);
	    	echo json_encode([
				'status'  => true,
				'message' => 'Berhasil mengubah data!',
	    	]);
	    } else {
	    	$data = [
				'username'   => $post['username'],
				'password'   => password_hash($post['password'], PASSWORD_DEFAULT),
				'hak_akases' => $post['hak_akses']
	    	];

	    	$q = $this->mod_sb->menambah($this->table, $data);
	    	echo json_encode([
				'status'  => true,
				'message' => 'Berhasil menambah data!',
	    	]);
	    }
	    
	}

	public function deleteWithAjax($id)
	{
	    $q = $this->mod_sb->menghapus($this->table, ['md5(id)'=>$id]);
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil menghapus data!',
		]);
	    
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin/Admin.php */