<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
	}

	public $table = 'produk';

	public function index()
	{
		if ($this->session->userdata('hak_akses') == 'pengembang') {
			$query = $this->mpd->mengambil2_1($this->session->userdata('id'))->result();
		} else {
			$query = $this->mpd->mengambil()->result();
		}

		// var_dump($query);
		// exit;

		$data = [
			'getDataProduk' => $query,
		];

		$this->lp->page_admin('produk/view_produk', $data);
	}

	public function detailProduk($id)
	{
		$data = [
			'getDataProduk' => $this->mpd->mengambil2($id)->row(),
			'getPhotoProduk' => $this->mod_sb->mengambil('gambar_produk', ['md5(id_produk)' => $id])->result(),
		];
		$this->lp->page_admin('produk/view_dtl_produk', $data);
	}

	public function tambahProduk()
	{
		if ($this->session->userdata('hak_akses') == 'pengembang') {
			$query = $this->mpd->mengambilProyekPengembang($this->session->userdata('id'))->result();
		} else {
			$query = $this->mpd->mengambilProyekPengembang()->result();
		}

		$data = [
			'getDataProyek' => $query,
		];
		$this->lp->page_admin('produk/add_produk', $data);
	}

	public function editProduk($id)
	{
		$q = $this->mod_sb->mengambilOrderBy($this->table, ['md5(id)' => $id])->row();
		$data = [
			'getDataProduk'  => $q,
			'getDataProyek'  => $this->mpd->mengambilProyekPengembang2($q->id)->row(),
			'getPhotoProduk' => $this->mod_sb->mengambil('gambar_produk', ['md5(id_produk)' => $id])->result(),
		];
		$this->lp->page_admin('produk/edit_produk', $data);
	}

	public function getLokasi($id)
	{
		$q = $this->mp->mengambilById($id)->row();
		$latlong = explode(',', $q->lokasi);
		$lokasi = [
			'nama'       => $q->nama,
			'pengembang' => $q->nama_pengembang,
			'lat'        => $latlong[0],
			'lng'       => $latlong[1],
		];

		echo json_encode([
			'status' => true,
			'data'   => $lokasi
		]);
	}

	public function saveWithAjax()
	{
		$post = $this->input->post();

		$id = $post['id'];

		if ($id != null && $id != '') {

			$data = [
				'nama'          => $post['nama'],
				'deskripsi'     => $post['deskripsi'],
				'luas_tanah'    => $post['luas_tanah'],
				'luas_bangunan' => $post['luas_bangunan'],
				'kamar_tidur'   => $post['kamar_tidur'],
				'kamar_mandi'   => $post['kamar_mandi'],
				'ruang_tamu'    => $post['ruang_tamu'],
				'dapur'         => $post['dapur'],
				'subsidi'       => $post['subsidi'],
				'harga'         => $this->lee->pecahHarga($post['harga']),
			];

			$q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
			// var_dump($data);
			// die;
			echo json_encode([
				'status'  => true,
				'message' => 'Berhasil mengubah data!',
			]);
		} else {
			//hitung jumlah file
			$countfiles = count(($_FILES['gambar']['name']));

			if ($countfiles >= 1) {
				// add data produk
				$data = [
					'id_proyek'     => $post['id_proyek'],
					'nama'          => $post['nama'],
					'deskripsi'     => $post['deskripsi'],
					'luas_tanah'    => $post['luas_tanah'],
					'luas_bangunan' => $post['luas_bangunan'],
					'kamar_tidur'   => $post['kamar_tidur'],
					'kamar_mandi'   => $post['kamar_mandi'],
					'ruang_tamu'    => $post['ruang_tamu'],
					'dapur'         => $post['dapur'],
					'subsidi'       => $post['subsidi'],
					'harga'         => $this->lee->pecahHarga($post['harga']),
				];

				$q = $this->mod_sb->menambah($this->table, $data);
				//end add data produk

				$last_id = $this->db->insert_id();

				for ($i = 0; $i < $countfiles; $i++) {
					$_FILES['file']['name']     = $_FILES['gambar']['name'][$i];
					$_FILES['file']['type']     = $_FILES['gambar']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['gambar']['tmp_name'][$i];
					$_FILES['file']['error']    = $_FILES['gambar']['error'][$i];
					$_FILES['file']['size']     = $_FILES['gambar']['size'][$i];

					// konfigurasi
					$num = 1;
					$config['upload_path']   = './assets/uploads/photo_produk/';
					$config['allowed_types'] = '*';
					$config['max_size']      = 0;
					$config['max_width']     = 0;
					$config['max_height']    = 0;
					$config['file_name']     = $post['nama'] . date("ymds_") . $i;
					$config['overwrite']     = true;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					//upload ke server
					if ($this->upload->do_upload('file')) {
						$filedata                    = $this->upload->data();
						$uploadData[$i]['id_produk']        = $last_id;
						$uploadData[$i]['gambar']           = $filedata['file_name'];
						$uploadData[$i]['gambar_thumbnail'] = $filedata['file_name'];
						//thumbnail
						$this->addThumbnail($uploadData[$i]['gambar_thumbnail']);

						$dataphoto = [
							'id_produk' => $uploadData[$i]['id_produk'],
							'gambar'    => $uploadData[$i]['gambar'],
							'gambar_thumb' => $uploadData[$i]['gambar_thumbnail'],
						];
						// var_dump($dataphoto);
						// exit;
						$q = $this->mod_sb->menambah('gambar_produk', $dataphoto);
					}
				}
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil menambah data!',
				]);
			} else {
				// add data produk
				$data = [
					'id_proyek'     => $post['id_proyek'],
					'nama'          => $post['nama'],
					'deskripsi'     => $post['deskripsi'],
					'luas_tanah'    => $post['luas_tanah'],
					'luas_bangunan' => $post['luas_bangunan'],
					'kamar_tidur'   => $post['kamar_tidur'],
					'kamar_mandi'   => $post['kamar_mandi'],
					'ruang_tamu'    => $post['ruang_tamu'],
					'dapur'         => $post['dapur'],
					'subsidi'       => $post['subsidi'],
					'harga'         => $this->lee->pecahHarga($post['harga']),
				];


				$q = $this->mod_sb->menambah($this->table, $data);
				//end add data produk
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil menambah data!',
				]);
			}
			//end photo
		}
	}

	public function saveGambar()
	{
		//hitung jumlah file
		$countfiles = count(($_FILES['gambar']['name']));

		if ($countfiles >= 1) {
			$post = $this->input->post();
			$id = $post['id'];


			for ($i = 0; $i < $countfiles; $i++) {
				$_FILES['file']['name']     = $_FILES['gambar']['name'][$i];
				$_FILES['file']['type']     = $_FILES['gambar']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['gambar']['tmp_name'][$i];
				$_FILES['file']['error']    = $_FILES['gambar']['error'][$i];
				$_FILES['file']['size']     = $_FILES['gambar']['size'][$i];

				// konfigurasi
				$num = 1;
				$config['upload_path']   = './assets/uploads/photo_produk/';
				$config['allowed_types'] = '*';
				$config['max_size']      = 0;
				$config['max_width']     = 0;
				$config['max_height']    = 0;
				$config['file_name']     = $post['nama'] . date("ymds_") . $i;
				$config['overwrite']     = true;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				//upload ke server
				if ($this->upload->do_upload('file')) {
					$filedata                           = $this->upload->data();

					$uploadData[$i]['id_produk']        = $id;
					$uploadData[$i]['gambar']           = $filedata['file_name'];
					$uploadData[$i]['gambar_thumbnail'] = $filedata['file_name'];
					//thumbnail
					$this->addThumbnail($uploadData[$i]['gambar_thumbnail']);

					$dataphoto = [
						'id_produk'    => $uploadData[$i]['id_produk'],
						'gambar'       => $uploadData[$i]['gambar'],
						'gambar_thumb' => $uploadData[$i]['gambar_thumbnail'],
					];
					// var_dump($dataphoto);
					// die;
					$q = $this->mod_sb->menambah('gambar_produk', $dataphoto);
				}
			}
			echo json_encode([
				'status'  => true,
				'message' => 'Berhasil menambah data!',
			]);
		} else {
			echo json_encode([
				'status'  => true,
				'message' => 'Tidak ada gambar yang dipilih!',
			]);
		}
		//end photo
	}

	public function addThumbnail($file_name)
	{
		//config resize
		$config = [
			// thumbnail Large
			[
				'image_library'  => 'GD2',
				'source_image'   => './assets/uploads/photo_produk/' . $file_name,
				'maintain_ratio' => FALSE,
				'width'          => '865',
				'height'         => '500',
				'new_image'      => './assets/uploads/thumbnails_produk/large/' . $file_name
			],
			// thumbnail medium
			[
				'image_library'  => 'GD2',
				'source_image'   => './assets/uploads/photo_produk/' . $file_name,
				'maintain_ratio' => FALSE,
				'width'          => '270',
				'height'         => '171',
				'new_image'      => './assets/uploads/thumbnails_produk/medium/' . $file_name
			],
			// thumbnail small
			[
				'image_library'  => 'GD2',
				'source_image'   => './assets/uploads/photo_produk/' . $file_name,
				'maintain_ratio' => FALSE,
				'width'          => '270',
				'height'         => '171',
				'new_image'      => './assets/uploads/thumbnails_produk/small/' . $file_name
			],
		];
		// library
		$this->load->library('image_lib', $config[0]);
		// pecah
		foreach ($config as $item) {
			// inisialisasi image
			$this->image_lib->initialize($item);
			// jika tidak di resize
			if (!$this->image_lib->resize()) {
				return false;
			}
			$this->image_lib->clear();
		}
	}

	//delete
	public function deleteWithAjax($id)
	{
		//getDataPhoto
		$query = $this->mod_sb->mengambil('gambar_produk', ['md5(id_produk)' => $id])->result();
		//logik hapus photo
		if ($query != null) {
			foreach ($query as $qp) {
				if ($qp->gambar != 'default.png') {
					$q = $this->mod_sb->menghapus('gambar_produk', ['id_produk' => $qp->id_produk]);
					$tes = unlink('assets/uploads/photo_produk/' . $qp->gambar);
					$tes = unlink('assets/uploads/thumbnails_produk/large/' . $qp->gambar);
					$tes = unlink('assets/uploads/thumbnails_produk/medium/' . $qp->gambar);
					$tes = unlink('assets/uploads/thumbnails_produk/small/' . $qp->gambar);
				}
			}
		}

		$q = $this->mod_sb->menghapus($this->table, ['md5(id)' => $id]);
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil menghapus data!',
		]);
	}

	public function deleteWithAjax2($id)
	{
		//getDataPhoto
		$query = $this->mod_sb->mengambil('gambar_produk', ['md5(id)' => $id])->row();
		//logik hapus photo
		if ($query != null) {
			$tes = unlink('assets/uploads/photo_produk/' . $query->gambar);
			$tes2 = unlink('assets/uploads/thumbnails_produk/' . $query->gambar);
		}

		$q = $this->mod_sb->menghapus('gambar_produk', ['md5(id)' => $id]);
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil menghapus data!',
		]);
	}
	// end delete

}

/* End of file Produk.php */
/* Location: ./application/controllers/Admin/Produk.php */