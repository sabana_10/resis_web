<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengembang extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
		wates_petugas();
	}

	public $table = 'pengembang';

	public function index()
	{
		// $data = [
		// 	'password'      => password_hash('admin', PASSWORD_DEFAULT),
		// ];
		// $q = $this->mod_sb->mengubah($this->table, ['id'=>6], $data);
		// var_dump($this->mod_sb->mengambil($this->table, ['id'=>6])->row());
		// die;
		$data = [
			'getDataPengembang' => $this->mod_sb->mengambil($this->table)->result(),
		];

		$this->lp->page_admin('pengembang/view_pengembang', $data);
	}

	public function tambahPengembang()
	{
		$this->lp->page_admin('pengembang/add_pengembang');
	}

	public function editPengembang($id)
	{
		$data = [
			'getDataPengembang' => $this->mod_sb->mengambilOrderBy($this->table, ['md5(id)' => $id])->row(),
		];

		// $tes = $this->mod_sb->mengambilOrderBy($this->table, ['md5(id)'=>$id])->row();
		// if ($tes->id == '66') {
		//  var_dump($tes->username);
		//  exit;
		// }else{
		// 	var_dump('expression');
		// 	exit;
		// }

		$this->lp->page_admin('pengembang/edit_pengembang', $data);
	}

	public function saveWithAjax()
	{
		$post = $this->input->post();
		$id = $post['id'];
		//config logo
		$config['upload_path']   = './assets/uploads/logo_pengembang/';
		$config['allowed_types'] = '*';
		$config['max_size']      = 0;
		$config['max_width']     = 0;
		$config['max_height']    = 0;
		$config['file_name']     = $post['nama'] . date("ymd");
		$config['overwrite']     = true;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);


		//logik add and update
		if ($id != null or $id != '') {
			if (!$this->upload->do_upload('logo')) {
				$data = [
					'nama'          => $post['nama'],
					'deskripsi'     => $post['deskripsi'],
					'tahun_berdiri' => $post['tahun_berdiri'],
					'username'      => $post['username'],
				];

				$q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
					'error'   => $this->upload->display_errors(),
				]);
			} else {
				$_data = array('upload_data' => $this->upload->data());

				$data = [
					'nama'          => $post['nama'],
					'deskripsi'     => $post['deskripsi'],
					'tahun_berdiri' => $post['tahun_berdiri'],
					'username'      => $post['username'],
					'logo'          => $_data['upload_data']['file_name'],
				];

				$q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
				]);
			}
		} else {
			$username = $post['username'];
			$q_cek = $this->mod_sb->mengambil($this->table, ['username' => $username])->num_rows();
			if ($q_cek == 0) {
				// var_dump($q_cek);
				// exit;
				if (!$this->upload->do_upload('logo')) {
					$data = [
						'nama'          => $post['nama'],
						'deskripsi'     => $post['deskripsi'],
						'tahun_berdiri' => $post['tahun_berdiri'],
						'username'      => $post['username'],
						'password'      => password_hash($post['password'], PASSWORD_DEFAULT),
						'logo'          => 'default.png',

					];

					$q = $this->mod_sb->menambah($this->table, $data);
					echo json_encode([
						'status'  => true,
						'message' => 'Berhasil menambah data!',
						// 'error'   => $this->upload->display_errors()
					]);
				} else {
					$_data = array('upload_data' => $this->upload->data());
					$data = [
						'nama'          => $post['nama'],
						'deskripsi'     => $post['deskripsi'],
						'tahun_berdiri' => $post['tahun_berdiri'],
						'username'      => $post['username'],
						'password'      => password_hash($post['password'], PASSWORD_DEFAULT),
						'logo'          => $_data['upload_data']['file_name'],
					];

					$q = $this->mod_sb->menambah($this->table, $data);
					echo json_encode([
						'status'  => true,
						'message' => 'Berhasil menambah data!',
					]);
				}
			} else {
				echo json_encode([
					'status'  => false,
					'errorintruth' => 'Username sudah digunakan, silahkan pilih yang baru!',
				]);
			}
		}
	}

	public function cek_usr($username)
	{
		return $this->mod_sb->mengambil($this->table, ['username' => $username])->row();
	}

	public function deleteWithAjax($id)
	{
		//getDataPhoto
		$query = $this->mod_sb->mengambil($this->table, ['md5(id)' => $id])->row();
		//logik hapus photo
		if ($query != null) {
			if ($query->logo != 'default.png') {
				$tes = unlink('assets/uploads/logo_pengembang/' . $query->logo);
			}
		}
		//delete data
		$q = $this->mod_sb->menghapus($this->table, ['md5(id)' => $id]);
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil menghapus data!',
		]);
	}

	public function editPassword($id)
	{
		$data = [
			'getDataPengembang' => $this->mod_sb->mengambil($this->table, ['md5(id)' => $id])->row(),
		];

		$this->lp->page_admin('pengembang/edit_pw_pengembang', $data);
	}

	public function savePwWithAjax()
	{
		$post   = $this->input->post();

		$id     = $post['id'];
		$newpw  = $post['passwordbaru'];
		$newpw2 = $post['ulangipasswordbaru'];

		if ($newpw != $newpw2) {
			echo json_encode([
				'status' => 'bedapwnya',
				'bedapw' => 'Password baru tidak sinkron! mohon cek kembali dibagian input ulangi password baru.',
			]);
		} else {
			$data = [
				'password'      => password_hash($newpw, PASSWORD_DEFAULT),
			];
			$q = $this->mod_sb->mengubah($this->table, ['id' => $id], $data);
			echo json_encode([
				'status'  => true,
				'message' => 'Berhasil mengubah password!',
			]);
		}
	}
}

/* End of file Pengembang.php */
/* Location: ./application/controllers/Admin/Pengembang.php */