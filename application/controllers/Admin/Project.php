<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
	}

	public $table = 'proyek';

	public function index()
	{
		if ($this->session->userdata('hak_akses') == 'pengembang') {
			$query = $this->mp->mengambilById2($this->session->userdata('id'))->result();
		} else {
			$query = $this->mp->mengambil()->result();
		}

		$data = [
			'getDataProject' => $query,
		];

		$this->lp->page_admin('project/view_project', $data);
	}

	public function tambahProject()
	{
		$data = [
			'getProvinsi' => $this->mod_sb->mengambilOrderBy('provinces', null, 'name', 'asc')->result(),
			'getDataPengembang' => $this->mod_sb->mengambil('pengembang')->result(),
		];

	    $this->lp->page_admin('project/add_project', $data);
	}

	public function editProject($id)
	{
	    $data = [
			'getProvinsi'    => $this->mod_sb->mengambilOrderBy('provinces', null, 'name', 'asc')->result(),
			'getKabupaten'   => $this->mod_sb->mengambilOrderBy('regencies', null, 'name', 'asc')->result(),
			'getKecamatan'   => $this->mod_sb->mengambilOrderBy('districts', null, 'name', 'asc')->result(),
			'getDesa'        => $this->mod_sb->mengambilOrderBy('villages', null, 'name', 'asc')->result(),
			'getDataProject' => $this->mp->mengambilById($id)->row(),
		    // 'getDataPengembang' => $this->mod_sb->mengambil('pengembang')->result(),
	    ];


	    $this->lp->page_admin('project/edit_project', $data);
	}

	public function saveWithAjax()
	{
	    $post = $this->input->post();

	    $id = $post['id'];

	    if ($id != null && $id != '') {
	    	if ($post['provinsi'] != 'Pilih Provinsi' || $post['provinsi'] != '') {
	    		if ($post['input_kabupaten'] != null && $post['input_kecamatan'] != null) {
	    			$data = [
						// 'id_pengembang'           => $post['id_pengembang'],
						'nama'                    => $post['nama'],
						'deskripsi'               => $post['deskripsi'],
						'lokasi'                  => $post['lokasi'],
						'fasilitas_umum'          => $post['fasilitas_umum'],
						'type'                    => $post['type'],
						'tahun_proyek_dimuali'    => $post['tahun_proyek_dimuali'],
						'estimasi_proyek_selesai' => $post['estimasi_proyek_selesai'],
						'tanggal_edit'            => date('Y-m-d'),
			    	];
	    		}else{
	    			$data = [
						// 'id_pengembang'           => $post['id_pengembang'],
						'nama'                    => $post['nama'],
						'deskripsi'               => $post['deskripsi'],
						'provinsi'                => $post['provinsi'],
						'kabupaten'               => $post['kabupaten'],
						'kecamatan'               => $post['kecamatan'],
						// 'desa'                    => $post['desa'],
						'lokasi'                  => $post['lokasi'],
						'fasilitas_umum'          => $post['fasilitas_umum'],
						'type'                    => $post['type'],
						'tahun_proyek_dimuali'    => $post['tahun_proyek_dimuali'],
						'estimasi_proyek_selesai' => $post['estimasi_proyek_selesai'],
						'tanggal_edit'            => date('Y-m-d'),
			    	];
	    		}
	    	}else{
		    	$data = [
					// 'id_pengembang'           => $post['id_pengembang'],
					'nama'                    => $post['nama'],
					'deskripsi'               => $post['deskripsi'],
					'lokasi'                  => $post['lokasi'],
					'fasilitas_umum'          => $post['fasilitas_umum'],
					'type'                    => $post['type'],
					'tahun_proyek_dimuali'    => $post['tahun_proyek_dimuali'],
					'estimasi_proyek_selesai' => $post['estimasi_proyek_selesai'],
					'tanggal_edit'            => date('Y-m-d'),
		    	];
	    	}

	    	$q = $this->mod_sb->mengubah($this->table, ['id'=>$id], $data);
	    	echo json_encode([
				'status'  => true,
				'message' => 'Berhasil mengubah data!',
	    	]);
	    } else {
	    	$data = [
				'id_pengembang'           => $post['id_pengembang'],
				'nama'                    => $post['nama'],
				'deskripsi'               => $post['deskripsi'],
				'provinsi'                => $post['provinsi'],
				'kabupaten'               => $post['kabupaten'],
				'kecamatan'               => $post['kecamatan'],
				// 'desa'                    => $post['desa'],
				'lokasi'                  => $post['lokasi'],
				'fasilitas_umum'          => $post['fasilitas_umum'],
				'type'                    => $post['type'],
				'tahun_proyek_dimuali'    => $post['tahun_proyek_dimuali'],
				'estimasi_proyek_selesai' => $post['estimasi_proyek_selesai'],
				'tanggal_input'           => date('Y-m-d'),
	    	];
	    	// var_dump($data['desa']);
	    	// die;
	    	
	    	$q = $this->mod_sb->menambah($this->table, $data);
	    	echo json_encode([
				'status'  => true,
				'message' => 'Berhasil menambah data!',
	    	]);
	    }
	}
	    
	public function deleteWithAjax($id)
	{
	    $q = $this->mod_sb->menghapus($this->table, ['md5(id)'=>$id]);
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil menghapus data!',
		]);
	    
	}

	public function getKabupaten($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('regencies', ['province_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	public function getKecamatan($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('districts', ['regency_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	public function getDesa($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('villages', ['district_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

}

/* End of file Project.php */
/* Location: ./application/controllers/Admin/Project.php */