<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
	}

	public function index()
	{
		if ($this->session->userdata('hak_akses') == 'super_admin') {
			$getptg = $this->mod_sb->mengambil('petugas')->num_rows();
		} else {
			$getptg = $this->mod_sb->mengambil('petugas', ['hak_akases'=>'petugas'])->num_rows();
		}
		

		$data = [
			'getPengembang' => $this->mod_sb->mengambil('pengembang')->num_rows(),
			'getProyek'     => $this->mod_sb->mengambil('proyek')->num_rows(),
			'getProduk'     => $this->mod_sb->mengambil('produk')->num_rows(),
			'getPetugas'     => $getptg,
		];

		$this->lp->page_admin('home', $data);
		// $this->load->view('admin/index');
	}

	// dashboard pengmebang
	public function dashboardPengembang()
	{
		$data = [
			'getProyek'     => $this->mod_sb->mengambil('proyek', ['id_pengembang'=>$this->session->userdata('id')])->num_rows(),
			'getProduk'     => $this->mod_sb->mengambil('produk')->num_rows(),
		];

		$this->lp->page_admin('dashboardPengembang', $data);
		// $this->load->view('admin/index');
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Admin/Home.php */