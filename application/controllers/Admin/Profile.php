<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in_admin();
	}

	public $table = 'petugas';
	

	public function index($id)
	{
		if ($this->session->userdata('hak_akses') == 'pengembang') {
			$getQuery = $this->mod_sb->mengambil('pengembang', ['md5(id)'=>$id])->row();
		}else{
			$getQuery = $this->mod_sb->mengambil($this->table, ['md5(id)'=>$id])->row();
		}

		$data = [
			'gdp' => $getQuery,
		];

		$this->lp->page_admin('profil/view_profile', $data);
	}

	public function savePwWithAjax()
	{
		$post   = $this->input->post();
		
		$id     = $post['id'];
		$newpw  = $post['passwordbaru'];
		$newpw2 = $post['ulangipasswordbaru'];
		$lastpw = $post['passwordlama'];

	    if ($newpw != $newpw2) {
	    	echo json_encode([
				'status' => 'bedapwnya',
				'bedapw' => 'Password baru tidak sinkron! mohon cek kembali dibagian input ulangi password baru.',
			]);
	    } else {
	    	$q_cekpw = $this->mod_sb->mengambil($this->table, ['id'=>$id] )->row();
	    	if (!password_verify($lastpw, $q_cekpw->password)) {
	    		echo json_encode([
					'status'  => 'pwlamasalah',
					'pwsalah' => 'Password lama salah! mohon ulangi kembali.',
				]);
	    	} else {
	    		$data = [
	    			'password'      => password_hash($newpw, PASSWORD_DEFAULT),
	    		];
	    		$q = $this->mod_sb->mengubah($this->table, ['id'=>$id], $data);
	    		echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah password!',
				]);
	    	}
    	}
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/Admin/Profile.php */