<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function index()
	{

		$data = [
			'judul'               => 'Resis - Property',
			'getKabupaten'        => $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result(),
			// 'getDataProduk'       => $this-0>mod_sb->mengambil('vwhalamanutama', null, 21)->result(),
			'getDataProduk'       => $this->mpd->mengambilPagination(),
			'getDataLuasTanah'    => $this->mod_sb->mengambil('vwtopluastanah', null, 5)->result(),
			'getDataLuasBangunan' => $this->mod_sb->mengambil('vwluasbangunan', null, 5)->result(),
		];
		$this->lp->page('produk/view_produk', $data);
	}

	public function cariProduk()
	{
		$post          = $this->input->post();
		$data = [
			'judul'               => 'Resis - Property',
			'getKabupaten'        => $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result(),
			// 'getDataProduk'       => $this->mod_sb->mengambil('vwhalamanutama', null, 21)->result(),
			// 'getDataProduk'       => $this->mpd->mengambilPagination('cari', $post['cariProperty']),
			'getDataProduk'       => $this->mpd->mengambilPagination('cari', $post['cariProperty']),
			'getDataLuasTanah'    => $this->mod_sb->mengambil('vwtopluastanah', null, 5)->result(),
			'getDataLuasBangunan' => $this->mod_sb->mengambil('vwluasbangunan', null, 5)->result(),
		];
		$this->lp->page('produk/view_produk', $data);
	}

	public function cariProduk2($id)
	{
		$post          = $this->input->post();

		$data = [
			'judul'               => 'Resis - Property',
			'getKabupaten'        => $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result(),
			// 'getDataProduk'       => $this->mod_sb->mengambil('vwhalamanutama', null, 21)->result(),
			'getDataProduk'       => $this->mpd->mengambilPagination('cariAuto', $id),
			'getDataLuasTanah'    => $this->mod_sb->mengambil('vwtopluastanah', null, 5)->result(),
			'getDataLuasBangunan' => $this->mod_sb->mengambil('vwluasbangunan', null, 5)->result(),
		];
		$this->lp->page('produk/view_produk', $data);
	}

	public function cariProdukFilter()
	{
		$post          = $this->input->post();
		$subsidi       = $post['subsidi']; 
		$min_harga     = $post['min_harga'];
		$max_harga     = $post['max_harga']; 
		$kabupaten     = $post['kabupaten']; 
		$kecamatan     = $post['kecamatan']; 
		// $luas_tanah    = $post['luas_tanah']; 
		// $luas_bangunan = $post['luas_bangunan'];

		$data = [
			'judul'               => 'Resis - Property',
			'getKabupaten'        => $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result(),
			//'getDataProduk'       => $this->mod_sb->mengambil('vwhalamanutama', null, 21)->result(),
			// 'getDataProduk'       => $this->mpd->mengambilPagination('cariFilter', null, $subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan, $luas_tanah, $luas_bangunan),
			'getDataProduk'       => $this->mpd->mengambilPagination('cariFilter', null, $subsidi, $min_harga, $max_harga, $kabupaten, $kecamatan),
			'getDataLuasTanah'    => $this->mod_sb->mengambil('vwtopluastanah', null, 5)->result(),
			'getDataLuasBangunan' => $this->mod_sb->mengambil('vwluasbangunan', null, 5)->result(),
		];
		$this->lp->page('produk/view_produk', $data);
	}

	public function autoCompleteProperty($text)
	{
		header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE,OPTIONS');
		header('Access-Control-Allow-Headers:Content-Type');
	    if ($text) {
	    	$result = $this->mpd->autoCompleteProperty($text)->result();
	    	foreach ($result as $row) {
	    		// $array_result['label'] = $row->nama_produk." - ".$row->nama;
	    		// $array_result['value'] = $row->nama;
	    		$array_result[] = array(
					'label' => $row->nama_produk." - ".$row->nama,
					'nama'  => $row->nama,
					'id'    => md5($row->id),
	    		);
	    	}
	    	echo json_encode($array_result);
	    }    
	}


	public function getKabupaten($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	public function getKecamatan($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('vetampilkecamatanproyek', ['regency_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	// public function getDesa($id)
	// {
	// 	$q = $this->mod_sb->mengambilOrderBy('villages', ['district_id'=>$id], 'name', 'asc')->result();
	// 	echo json_encode([
	// 		'status'  => true,
	// 		'message' => 'Berhasil mengambil data!',
	// 		'data'    => $q,
	// 	]);
	// }
}

/* End of file Proyek.php */
/* Location: ./application/controllers/Konsumen/Proyek.php */