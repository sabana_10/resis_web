<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TentangKami extends CI_Controller {

	public function index()
	{
		$data = [
			'judul'          => 'Resis - Tentang Kami',
		];
		$this->lp->page('tentangkami/view_tentangkami', $data);
	}

}

/* End of file TentangKami.php */
/* Location: ./application/controllers/Konsumen/TentangKami.php */