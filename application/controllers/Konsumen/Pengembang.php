<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembang extends CI_Controller {

	public $table = 'pengembang';

	public function index($id)
	{


		$data = [
			'judul'         => 'Resis - Profile Pengembang',
			'gdpg' => $this->mod_sb->mengambil($this->table, ['md5(id)'=>$id])->row(),
			'getDataProduk'  => $this->mod_sb->mengambil('vwhalamanutama', ['md5(id_pengembang)'=>$id], 3)->result(),
			'getProyek'     => $this->mod_sb->mengambil('proyek', ['md5(id_pengembang)'=>$id])->num_rows(),
			'getProduk'     => $this->mod_sb->mengambil('produk')->num_rows(),
			'getKabupaten'        => $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result(),
		];

		$this->lp->page('pengembang/view_pengembang', $data);
	}

	public function cariProduk2($id)
	{
		$post          = $this->input->post();

		$data = [
			'judul'               => 'Resis - Property',
			'getKabupaten'        => $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result(),
			'getDataProduk'       => $this->mpd->cari($id)->result(),
			'getDataLuasTanah'    => $this->mod_sb->mengambil('vwtopluastanah', null, 5)->result(),
			'getDataLuasBangunan' => $this->mod_sb->mengambil('vwluasbangunan', null, 5)->result(),
		];
		$this->lp->page('pengembang/view_pengembang', $data);
	}

	public function autoCompleteProperty($text, $where)
	{
		header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE,OPTIONS');
		header('Access-Control-Allow-Headers:Content-Type');
	    if ($text) {
	    	$result = $this->mpd->autoCompleteProperty($text, $where)->result();
	    	foreach ($result as $row) {
	    		// $array_result['label'] = $row->nama_produk." - ".$row->nama;
	    		// $array_result['value'] = $row->nama;
	    		$array_result[] = array(
					'label' => $row->nama_produk,
					'nama'  => $row->nama_produk,
					'id'    => md5($row->id),
	    		);
	    	}
	    	echo json_encode($array_result);
	    }    
	}

	public function getKabupaten($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('vwtampilkabupatenproduk', null, 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	public function getKecamatan($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('vetampilkecamatanproyek', ['regency_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

}

/* End of file Pengembang.php */
/* Location: ./application/controllers/Konsumen/Pengembang.php */