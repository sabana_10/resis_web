<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function index()
	{
		$data = [
			'judul'          => 'Resis - Berita',
		];

		$this->lp->page('berita/view_berita', $data);	
	}

}

/* End of file Berita.php */
/* Location: ./application/controllers/Konsumen/Berita.php */