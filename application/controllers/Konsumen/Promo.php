<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	public function index()
	{
		$data = [
			'judul'          => 'Resis - Promo',
		];

		$this->lp->page('promo/view_promo', $data);		
	}

}

/* End of file Promo.php */
/* Location: ./application/controllers/Konsumen/Promo.php */