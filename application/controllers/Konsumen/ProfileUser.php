<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileUser extends CI_Controller {

	public function index()
	{
		// var_dump($this->session->userdata('username'));die;
		$data = [
			'judul'       => 'Resis - Profile User',
			'getProvinsi' => $this->mod_sb->mengambilOrderBy('provinces', null, 'name', 'asc')->result(),
			'getDataUser' => $this->mu->mengambil($this->session->userdata('uname'))->row(),
		];
		$this->lp->page('profile_user/view_profile_user', $data);
	}

	public function saveAkun()
	{
		$post = $this->input->post();

	    $username = $post['email'];

	    // config
		$config['upload_path']   = './assets/uploads/photo_pengguna/';
		$config['allowed_types'] = '*';
		$config['max_size']      = 10000;
		$config['max_width']     = 0;
		$config['max_height']    = 0;
		$config['file_name']     = $post['id'];
		$config['overwrite']     = true;
	    $this->load->library('upload', $config);
	    $this->upload->initialize($config);
    	
	    if ($username != null && $username != '') {
	    	if (!$this->upload->do_upload('foto')) {
				$data = [
					'nama'        => $post['nama'],
					'no_tlp'      => $post['no_tlp'],
					'KTP'         => $post['KTP'],
					'Penghasilan' => str_replace(".", "", $post['Penghasilan'],)
		    	];

		    	$q = $this->mod_sb->mengubah('pengguna', ['username'=>$username], $data);
		    	// var_dump($data);
		    	// die;
		    	echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
					'error'   => $this->upload->display_errors(),
		    	]);

	    	}else{
	    		$_data = array('upload_data' => $this->upload->data());

	    		$data = [
					'nama'        => $post['nama'],
					'no_tlp'      => $post['no_tlp'],
					'KTP'         => $post['KTP'],
					'Penghasilan' => str_replace(".", "", $post['Penghasilan']),
					'foto'        => $_data['upload_data']['file_name'],
		    	];

		    	$q = $this->mod_sb->mengubah('pengguna', ['username'=>$username], $data);
		    	// var_dump($data);
		    	// die;
		    	echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
		    	]);
	    	}
	    }else{
	    	echo json_encode([
				'status'  => false,
				'message' => 'Gagal mengubah data!',
	    	]);
	    }
	}

	public function saveAlamat()
	{
		$post = $this->input->post();

	    $username = $post['email'];
	    // var_dump($post['provinsi']);die;
	    if ($username != null && $username != '') {
	    	if ($post['provinsi'] != 'Pilih Provinsi' && $post['provinsi'] != '') {
	    		if ($post['input_kabupaten'] != null && $post['input_kecamatan'] != null) {
			    	$data = [
						'alamat'    => $post['alamat'],
			    	];
	    		}else{
	    			$data = [
						'provinsi'  => $post['provinsi'],
						'kabupaten' => $post['kabupaten'],
						'kecamatan' => $post['kecamatan'],
						'alamat'    => $post['alamat'],
			    	];
	    		}    		
		    	$q = $this->mod_sb->mengubah('pengguna', ['username'=>$username], $data);
		    	// var_dump($data);
		    	// die;
		    	echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
		    	]);
	    	} else {
	    		$data = [
					'alamat'    => $post['alamat'],
		    	];

		    	$q = $this->mod_sb->mengubah('pengguna', ['username'=>$username], $data);
		    	// var_dump($data);
		    	// die;
		    	echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mengubah data!',
		    	]);
	    	}
	    	
	    }else{
	    	echo json_encode([
				'status'  => false,
				'message' => 'Gagal mengubah data!',
	    	]);
	    }
	}

	public function getKabupaten($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('regencies', ['province_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	public function getKecamatan($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('districts', ['regency_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

	public function getDesa($id)
	{
		$q = $this->mod_sb->mengambilOrderBy('villages', ['district_id'=>$id], 'name', 'asc')->result();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil mengambil data!',
			'data'    => $q,
		]);
	}

}

/* End of file ProfileUser.php */
/* Location: ./application/controllers/Konsumen/ProfileUser.php */