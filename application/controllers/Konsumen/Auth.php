<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{	
	public $table = 'pengguna';

	public function verifLogin()
	{
		$post		= $this->input->post();
		$username	= $post['username'];
		$pass		= $post['password'];

		// include_once APPPATH . "../vendor/autoload.php";
	 //    $google_client = new Google_Client();
	 //    $google_client->setClientId('626627450147-5pp8ee0af75qciv47mbfv4tcqhdppco3.apps.googleusercontent.com'); //masukkan ClientID anda 
	 //    $google_client->setClientSecret('AIzaSyD4NfvktSJrrnmUz9hTTfEJcCmHC-PRBpc'); //masukkan Client Secret Key anda
	 //    $google_client->setRedirectUri('http://localhost/resis_id'); //Masukkan Redirect Uri anda
	 //    $google_client->addScope('email');
	 //    $google_client->addScope('profile');

		// if(isset($_GET["code"]))
		//   {
		//    $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
		//    if(!isset($token["error"]))
		//    {
		//     $google_client->setAccessToken($token['access_token']);
		//     $this->session->set_userdata('access_token', $token['access_token']);
		// 	$google_service   = new Google_Service_Oauth2($google_client);
		// 	$data             = $google_service->userinfo->get();
		// 	$current_datetime = date('Y-m-d H:i:s');
		//     $user_data = array(
		// 		'nama'      => $data['given_name'],
		// 		// 'last_name'       => $data['family_name'],
		// 		'uname'   => $data['email'],
		// 		'foto' => $data['picture'],
		// 		'updated_at'      => $current_datetime
		//      );
		//     $this->session->set_userdata('user_data', $data);
		//    }									
		// }

		$q_cek = $this->mod_sb->mengambil($this->table, ['username'=>$username])->row();
		if ($q_cek) {
			if (password_verify($pass, $q_cek->password)) {
				if ($q_cek->no_tlp != null && $q_cek->KTP != null && $q_cek->Penghasilan != null) {
					$verif = 1;
				} else {
					$verif = 0;
				}
				
				$data = [
					'id'       => $q_cek->id,
					'uname'    => $q_cek->username,
					'is_login' => 1,
					'role'     => 'pengguna',
					'hak_akses' => 'pengguna',
					'is_verif' => $verif,
				];
				
				$this->session->set_userdata( $data );

				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil login!'
		    	]);
			} else {
				echo json_encode([
					'status'  => false,
					'message' => 'Password salah!'
		    	]);
			}
			
		} else {
			echo json_encode([
				'status'  => false,
				'message' => 'Akun ini tidak terdaftar!'
	    	]);
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		echo json_encode([
			'status'  => true,
			'message' => 'Berhasil keluar!'
    	]);
	}

	public function register()
	{
		$post		= $this->input->post();
		$username	= $post['reg_username'];
		$pass		= $post['reg_password'];
		$pass2		= $post['reg_password2'];

		$cek = $this->mod_sb->mengambil('pengguna', ['username'=>$username])->result();

		if (!$cek) {
			if ($pass2 == $pass) {
				// set data for uploading to db
				$data = [
					'username' => $username,
					'password' => password_hash($pass, PASSWORD_DEFAULT),
				];
				// add to db
				$q = $this->mod_sb->menambah($this->table, $data);

				// get data for session
				$qp = $this->mod_sb->mengambil('pengguna', ['username'=>$username])->row();
				
				if ($qp->no_tlp != null && $qp->KTP != null && $qp->Penghasilan != null) {
					$verif = 1;
				} else {
					$verif = 0;
				}
				// data for session
				$sessioning = [
					'id'       => $qp->id,
					'uname'    => $qp->username,
					'is_login' => 1,
					'role'     => 'pengguna',
					'hak_akses' => 'pengguna',
					'is_verif' => $verif,
				];
				
				// set session
				$this->session->set_userdata( $sessioning );
				// output
				echo json_encode([
					'status'  => true,
					'message' => 'Berhasil mendaftar!',
		    	]);
			}else{
				echo json_encode([
					'status'  => false,
					'message' => 'Password tidak sinkron! mohon cek kembali dibagian input ulangi password.',
		    	]);
			}
		} else {
			echo json_encode([
				'status'  => false,
				'message' => 'email sudah dipakai!',
	    	]);
		}
		

	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Admin/Auth.php */