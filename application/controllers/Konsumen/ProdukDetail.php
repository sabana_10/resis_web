<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdukDetail extends CI_Controller {

	public function index($id)
	{
		$gdp = $this->mpd->mengambil2($id)->row();

		$data = [
			'judul'          => 'Resis - Detail Property',
			'id_lokasi'      => $id,
			'gdp'            => $gdp,
			'getPhotoProduk' => $this->mod_sb->mengambil('gambar_produk', ['md5(id_produk)'=>$id])->result(),
			'getDataProduk'  => $this->mod_sb->mengambil('vwhalamanutama', null, 3)->result(),
		];

		// var_dump($this->mod_sb->mengambil('proyek', ['nama'=>$gdp->nama_proyek]));
		// exit;

		$this->lp->page('produk/v_detail_produk', $data);
	}

	public function getLokasi($id)
	{
	    $q = $this->mpd->mengambil2($id)->row();
	    // var_dump($q);
	    // die;
    	$latlong = explode(',', $q->lokasi);
	    $lokasi = [
			'nama'       => $q->nama,
			'pengembang' => $q->nama_pengembang,
			'lat'        => $latlong[0],
			'long'       => $latlong[1],
	    ];

	    echo json_encode([
			'status' => true,
			'data'   => $lokasi
    	]);
	}

}

/* End of file ProdukDetail.php */
/* Location: ./application/controllers/Konsumen/ProdukDetail.php */