<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthGoogle extends CI_Controller {

	public function index()
	{
		include_once APPPATH . "../vendor/autoload.php";
		  $google_client = new Google_Client();
		  $google_client->setClientId('626627450147-5pp8ee0af75qciv47mbfv4tcqhdppco3.apps.googleusercontent.com'); //masukkan ClientID anda 
		  $google_client->setClientSecret('AIzaSyD4NfvktSJrrnmUz9hTTfEJcCmHC-PRBpc'); //masukkan Client Secret Key anda
		  $google_client->setRedirectUri('http://localhost/resis_id'); //Masukkan Redirect Uri anda
		  $google_client->addScope('email');
		  $google_client->addScope('profile');

		  if(isset($_GET["code"]))
		  {
		   $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
		   if(!isset($token["error"]))
		   {
		    $google_client->setAccessToken($token['access_token']);
		    $this->session->set_userdata('access_token', $token['access_token']);
			$google_service   = new Google_Service_Oauth2($google_client);
			$data             = $google_service->userinfo->get();
			$current_datetime = date('Y-m-d H:i:s');
		    $user_data = array(
				'nama'      => $data['given_name'],
				// 'last_name'       => $data['family_name'],
				'uname'   => $data['email'],
				'foto' => $data['picture'],
				'updated_at'      => $current_datetime
		     );
		    $this->session->set_userdata('user_data', $data);
		   }									
		  }
		  $login_button = '';
		  if(!$this->session->userdata('access_token'))
		  {
		  	
			$login_button         = '<a href="'.$google_client->createAuthUrl().'"><img src="https://1.bp.blogspot.com/-gvncBD5VwqU/YEnYxS5Ht7I/AAAAAAAAAXU/fsSRah1rL9s3MXM1xv8V471cVOsQRJQlQCLcBGAsYHQ/s320/google_logo.png" /></a>';
			$data['login_button'] = $login_button;
		   $this->load->view('google_login', $data);
		  }
		  else
		  {
		  	// uncomentar kode dibawah untuk melihat data session email
		  	// echo json_encode($this->session->userdata('access_token')); 
		  	// echo json_encode($this->session->userdata('user_data'));
		   echo "Login success";
		  }
	}

	public function logout()
	 {
	  $this->session->unset_userdata('access_token');

	  $this->session->unset_userdata('user_data');
	  echo "Logout berhasil";
	 }

}

/* End of file AuthGoogle.php */
/* Location: ./application/controllers/Konsumen/AuthGoogle.php */